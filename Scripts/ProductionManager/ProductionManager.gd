extends Node2D

const ConstructionYardScene : PackedScene = preload("res://Scenes/Buildings/ContructionYard/ContructionYard.tscn")
const OreRefineryScene : PackedScene = preload("res://Scenes/Buildings/OreRefinery/OreRefinery.tscn")
const PowerPlantScene : PackedScene = preload("res://Scenes/Buildings/PowerPlant/PowerPlant.tscn")
const BarraksScene : PackedScene = preload("res://Scenes/Buildings/Barraks/Barraks.tscn")
const FactoryScene : PackedScene = preload("res://Scenes/Buildings/Factory/Factory.tscn")

const USSRConstructionYardScene : PackedScene = preload("res://Scenes/Buildings/USSR/USSRContructionYard/USSRContructionYard.tscn")
const USSROreRefineryScene : PackedScene = preload("res://Scenes/Buildings/USSR/USSROreRefinery/USSROreRefinery.tscn")
const USSRPowerPlantScene : PackedScene = preload("res://Scenes/Buildings/USSR/USSRPowerPlant/USSRPowerPlant.tscn")
const USSRBarraksScene : PackedScene = preload("res://Scenes/Buildings/USSR/USSRBarraks/USSRBarraks.tscn")
const USSRFactoryScene : PackedScene = preload("res://Scenes/Buildings/USSR/USSRFactory/USSRFactory.tscn")

const SoldierScene : PackedScene = preload("res://Scenes/Units/Soldier/Soldier.tscn")
const MCVScene : PackedScene = preload("res://Scenes/Units/MCV/MCV.tscn")
const HarvesterScene : PackedScene = preload("res://Scenes/Units/Harvester/Harvester.tscn")
const TankScene : PackedScene = preload("res://Scenes/Units/Tank/Tank.tscn")

const USSRSoldierScene : PackedScene = preload("res://Scenes/Units/UssrSoldier/UssrSoldier.tscn")
const USSRMCVScene : PackedScene = preload("res://Scenes/Units/USSRMCV/USSRMCV.tscn")
const USSRHarvesterScene : PackedScene = preload("res://Scenes/Units/USSRHarvester/USSRHarvester.tscn")
const USSRTankScene : PackedScene = preload("res://Scenes/Units/USSRTank/USSRTank.tscn")

export (int) var maxInfantryUnits : int = 10
export (int) var maxTechnicUnits : int = 4
export (int) var maxFreeHarvesters : int = 4

var harverstersQuotaExceed : bool = false
var harvestersIncoming : int = 0
var infantryUnitsCount : int = 0
var technicUnitsCount : int = 0
var harvestersQuota : int = 0

signal playerStats_changed
signal productionButtons_changed

signal placeUnit
signal placeBuilding

onready var buildingsTimer : Timer = $BuildingsTimer
onready var unitsTimer : Timer = $UnitsTimer

var readyBuilding = null
var readyUnit = null

var dependencies : Dictionary = {
	"OreRefinery": ["ConstructionYard"],
	"PowerPlant": ["ConstructionYard"],
	"Barraks": ["ConstructionYard", "PowerPlant"],
	"Factory": ["ConstructionYard", "PowerPlant"],
	"Soldier": ["Barraks"],
	"MCV": ["Factory", "PowerPlant"],
	"Harvester": ["Factory", "OreRefinery"],
	"Tank": ["Factory"],
	"USSROreRefinery": ["USSRConstructionYard"],
	"USSRPowerPlant": ["USSRConstructionYard"],
	"USSRBarraks": ["USSRConstructionYard", "USSRPowerPlant"],
	"USSRFactory": ["USSRConstructionYard", "USSRPowerPlant"],
	"USSRSoldier": ["USSRBarraks"],
	"USSRMCV": ["USSRFactory", "USSRPowerPlant"],
	"USSRHarvester": ["USSRFactory", "USSROreRefinery"],
	"USSRTank": ["USSRFactory"]
}

export (Dictionary) var playerStats : Dictionary = {
	"money": 5000,
	"energy": 0
} setget set_playerStats, get_playerStats

func set_playerStats(value : Dictionary):
	playerStats = value
	emit_signal("playerStats_changed", playerStats)
	
func get_playerStats() -> Dictionary:
	return playerStats
	
export (Dictionary) var productionButtons : Dictionary = {} setget set_productionButtons, get_productionButtons

func set_productionButtons(value : Dictionary):
	if value != productionButtons:
		productionButtons = value
		emit_signal("productionButtons_changed", productionButtons)
	
func get_productionButtons() -> Dictionary:
	return productionButtons

func _ready():
	buildingsTimer.connect("timeout", self, "_on_buildingProductionComplete")
	unitsTimer.connect("timeout", self, "_on_unitProductionComplete")
	updateProductionButtons()

func _process(delta):
	playerStats.energy = get_tree().get_nodes_in_group("PowerPlant").size() * 100
	set_playerStats(playerStats)
	
func add_money(money : int):
	playerStats.money += money
	set_playerStats(playerStats)
	
	#if not buildingsTimer.is_stopped() or not unitsTimer.is_stopped():
	#	updateProductionButtons()
	
func is_dependency_satisfied(prodName : String) -> bool:
	if dependencies.has(prodName):
		var dep = dependencies[prodName]
		
		var allBuildingsExist : bool = true
		for dependency in dep:
			if get_tree().get_nodes_in_group(dependency).empty():
				allBuildingsExist = false
				
		return allBuildingsExist
		
	return true
	
func updateProductionButtons():
	var buttons = {"buildings": [], "units": []}
	
	if is_dependency_satisfied("ConstructionYard"):
		buttons.buildings.push_back({
				"name" : "ConstructionYard_button",
				"normal_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_normal.png",
				"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
				"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
				"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
				"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
				"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
				"progress": get_building_progress("ConstructionYard"),
				"production": is_building_in_production("ConstructionYard"),
				"disabled": is_buildingProductionButton_disabled("ConstructionYard")
			})
	if is_dependency_satisfied("PowerPlant"):
		buttons.buildings.push_back({
				"name" : "PowerPlant_button",
				"normal_icon": "res://Assets/Buttons/Buildings/PowerPlant/PowerPlant_normal.png",
				"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
				"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
				"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
				"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
				"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
				"progress": get_building_progress("PowerPlant"),
				"production": is_building_in_production("PowerPlant"),
				"disabled": is_buildingProductionButton_disabled("PowerPlant")
			})
		
	if is_dependency_satisfied("OreRefinery"):
		buttons.buildings.push_back({
				"name" : "OreRefinery_button",
				"normal_icon": "res://Assets/Buttons/Buildings/OreRefinery_normal.png",
				"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
				"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
				"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
				"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
				"fully_ready_icon": "res://Assets/Buttons/Buildings/OreRefinery_final.png",
				"progress": get_building_progress("OreRefinery"),
				"production": is_building_in_production("OreRefinery"),
				"disabled": is_buildingProductionButton_disabled("OreRefinery")
			})
	
	if is_dependency_satisfied("Barraks"):
		buttons.buildings.push_back({
				"name" : "Barraks_button",
				"normal_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_normal.png",
				"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
				"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
				"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
				"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
				"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
				"progress": get_building_progress("Barraks"),
				"production": is_building_in_production("Barraks"),
				"disabled": is_buildingProductionButton_disabled("Barraks")
			})
			
	if is_dependency_satisfied("Factory"):
		buttons.buildings.push_back({
				"name" : "Factory_button",
				"normal_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_normal.png",
				"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
				"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
				"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
				"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
				"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
				"progress": get_building_progress("Factory"),
				"production": is_building_in_production("Factory"),
				"disabled": is_buildingProductionButton_disabled("Factory")
			})
			
	if is_dependency_satisfied("Soldier"):
		buttons.units.push_back({
					"name" : "Soldier_button",
					"normal_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_normal.png",
					"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
					"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
					"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
					"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
					"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
					"progress": get_unit_progress("Soldier"),
					"production": is_unit_in_production("Soldier"),
					"disabled": is_unitProductionButton_disabled("Soldier")
				})
		
	if is_dependency_satisfied("MCV"):
		buttons.units.push_back({
					"name" : "MCV_button",
					"normal_icon": "res://Assets/Sprites/mcv_right.png",
					"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
					"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
					"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
					"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
					"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
					"progress": get_unit_progress("MCV"),
					"production": is_unit_in_production("MCV"),
					"disabled": is_unitProductionButton_disabled("MCV")
				})
				
	if is_dependency_satisfied("Harvester"):
		buttons.units.push_back({
					"name" : "Harvester_button",
					"normal_icon": "res://Assets/Sprites/harvester_right.png",
					"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
					"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
					"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
					"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
					"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
					"progress": get_unit_progress("Harvester"),
					"production": is_unit_in_production("Harvester"),
					"disabled": is_unitProductionButton_disabled("Harvester")
				})
				
	if is_dependency_satisfied("Tank"):
		buttons.units.push_back({
					"name" : "Tank_button",
					"normal_icon": "res://Assets/Sprites/tank_right.png",
					"disabled_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_disabled.png",
					"started_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_1to4.png",
					"third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_2to4.png",
					"two_third_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_3to4.png",
					"fully_ready_icon": "res://Assets/Buttons/Buildings/ConstructionYard/ConstructionYard_construction_full.png",
					"progress": get_unit_progress("Tank"),
					"production": is_unit_in_production("Tank"),
					"disabled": is_unitProductionButton_disabled("Tank")
				})
				
	set_productionButtons(buttons)
	
func button_accept_action(buttonName : String) -> void:
	
	match buttonName:
		"ConstructionYard_button":
			if readyBuilding:
				startBuildingPlacing()
			else:
				buildingProduction_start("ConstructionYard")
		"PowerPlant_button":
			if readyBuilding:
				startBuildingPlacing()
			else:
				buildingProduction_start("PowerPlant")
		"OreRefinery_button":
			if readyBuilding:
				startBuildingPlacing()
			else:
				buildingProduction_start("OreRefinery")
		"Barraks_button":
			if readyBuilding:
				startBuildingPlacing()
			else:
				buildingProduction_start("Barraks")
		"Factory_button":
			if readyBuilding:
				startBuildingPlacing()
			else:
				buildingProduction_start("Factory")
		"Soldier_button":
			if readyUnit:
				startUnitPlacing()
			else:
				unitProduction_start("Soldier")
		"MCV_button":
			if readyUnit:
				startUnitPlacing()
			else:
				unitProduction_start("MCV")
		"Harvester_button":
			if readyUnit:
				startUnitPlacing()
			else:
				unitProduction_start("Harvester")
		"Tank_button":
			if readyUnit:
				startUnitPlacing()
			else:
				unitProduction_start("Tank")
		_:
			return
			
func button_decline_action(buttonName : String) -> void:
	match buttonName:
			"ConstructionYard_button":
				if readyBuilding and readyBuilding.buildingName == "ConstructionYard":
					buildingProduction_cancel()
			"PowerPlant_button":
				if readyBuilding and readyBuilding.buildingName == "PowerPlant":
					buildingProduction_cancel()
			"OreRefinery_button":
				if readyBuilding and readyBuilding.buildingName == "OreRefinery":
					buildingProduction_cancel()
			"Barraks_button":
				if readyBuilding and readyBuilding.buildingName == "Barraks":
					buildingProduction_cancel()
			"Factory_button":
				if readyBuilding and readyBuilding.buildingName == "Factory":
					buildingProduction_cancel()
			"Soldier_button":
				if readyUnit and readyUnit.unitName == "Soldier":
					unitProduction_cancel()
			"MCV_button":
				if readyUnit and readyUnit.unitName == "MCV":
					unitProduction_cancel()
			"Harvester_button":
				if readyUnit and readyUnit.unitName == "Harvester":
					unitProduction_cancel()
			"Tank_button":
				if readyUnit and readyUnit.unitName == "Tank":
					unitProduction_cancel()
			_:
				return

func is_buildingProductionButton_disabled(buildingName : String) -> bool:
	return readyBuilding and not (readyBuilding.buildingName == buildingName and buildingsTimer.is_stopped())

func get_building_progress(buildingName : String) -> float:
	if readyBuilding and readyBuilding.buildingName == buildingName:
		if not buildingsTimer.is_stopped():
			return 1.0 - buildingsTimer.time_left / readyBuilding.productionTime
		else:
			return 1.0
		
	return 0.0

func is_building_in_production(buildingName : String) -> bool:
	return readyBuilding and readyBuilding.buildingName == buildingName
	
func buildingProduction_start(name : String):
	readyBuilding = create_building(name)

	playerStats.money -= readyBuilding.cost
	set_playerStats(playerStats)
	
	buildingsTimer.wait_time = readyBuilding.productionTime
	buildingsTimer.start()
	updateProductionButtons()
	
func buildingProduction_cancel():
	if readyBuilding:
		playerStats.money += readyBuilding.cost
		set_playerStats(playerStats)
		readyBuilding.queue_free()
		
	readyBuilding = null
	buildingsTimer.stop()
	updateProductionButtons()
	
func buildingPlaced():
	if readyBuilding.buildingName == "OreRefinery":
		var harvester = create_unit("Harvester")
		harvester.global_position = readyBuilding.get_delivery_position()
		readyBuilding.get_parent().add_child(harvester)
		harvester.state = Unit.States.FIND_ORE
	if readyBuilding.buildingName == "USSROreRefinery":
		var harvester = create_unit("USSRHarvester")
		harvester.global_position = readyBuilding.get_delivery_position()
		readyBuilding.get_parent().add_child(harvester)
		harvester.state = Unit.States.FIND_ORE
	readyBuilding = null
	updateProductionButtons()

func _on_buildingProductionComplete():
	updateProductionButtons()

func create_building(name : String) -> Node:
	match name:
		"ConstructionYard":
			return ConstructionYardScene.instance()
		"PowerPlant":
			return PowerPlantScene.instance()
		"OreRefinery":
			return OreRefineryScene.instance()
		"Barraks":
			return BarraksScene.instance()
		"Factory":
			return FactoryScene.instance()
		"USSRConstructionYard":
			return USSRConstructionYardScene.instance()
		"USSRPowerPlant":
			return USSRPowerPlantScene.instance()
		"USSROreRefinery":
			return USSROreRefineryScene.instance()
		"USSRBarraks":
			return USSRBarraksScene.instance()
		"USSRFactory":
			return USSRFactoryScene.instance()
		_:
			return null
			
func startBuildingPlacing() -> void:
	emit_signal("placeBuilding", readyBuilding)
	
func cancelBuildingPlacing() -> void:
	updateProductionButtons()
	
func is_building_in_progress() -> bool:
	return readyBuilding and not buildingsTimer.is_stopped()
	
func is_building_finished() -> bool:
	return readyBuilding and buildingsTimer.is_stopped()
	
func is_unitProductionButton_disabled(unitName : String) -> bool:
	return readyUnit and not (readyUnit.unitName == unitName and unitsTimer.is_stopped())

func get_unit_progress(unitName : String) -> float:
	if readyUnit and readyUnit.unitName == unitName:
		if not unitsTimer.is_stopped():
			return 1.0 - unitsTimer.time_left / readyUnit.productionTime
		else:
			return 1.0
		
	return 0.0

func is_unit_in_production(unitName : String) -> bool:
	return readyUnit and readyUnit.unitName == unitName
	
func unitProduction_start(name : String):
	readyUnit = create_unit(name)
	
	playerStats.money -= readyUnit.cost
	set_playerStats(playerStats)
	
	unitsTimer.wait_time = readyUnit.productionTime
	unitsTimer.start()
	updateProductionButtons()
	
func unitProduction_cancel():
	if readyUnit:
		playerStats.money += readyUnit.cost
		set_playerStats(playerStats)
		readyUnit.queue_free()
		
	readyUnit = null
	unitsTimer.stop()
	updateProductionButtons()
	
func unitPlaced():
	readyUnit = null
	updateProductionButtons()

func _on_unitProductionComplete():
	updateProductionButtons()

func create_unit(name : String) -> Node:
	match name:
		"Soldier":
			return SoldierScene.instance()
		"MCV":
			return MCVScene.instance()
		"Harvester":
			return HarvesterScene.instance()
		"Tank":
			return TankScene.instance()
		"UssrSoldier":
			return USSRSoldierScene.instance()
		"USSRMCV":
			return USSRMCVScene.instance()
		"USSRHarvester":
			return USSRHarvesterScene.instance()
		"UssrTank":
			return USSRTankScene.instance()
		_:
			return null
			
func startUnitPlacing() -> void:
	emit_signal("placeUnit", readyUnit)
	
func cancelUnitPlacing() -> void:
	updateProductionButtons()
	
func is_unit_in_progress() -> bool:
	return readyUnit and not unitsTimer.is_stopped()
	
func is_unit_finished() -> bool:
	return readyUnit and unitsTimer.is_stopped()

