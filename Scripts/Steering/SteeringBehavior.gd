extends Reference
class_name SteeringBehavior

const MinInt : int = -2147483648
const MaxInt : int = 2147483647
const MinFloat : float = 1.17549435e-38
const MaxFloat : float = 1.70141183e+38

const Epsilon : float = 1e-12

const Pi : float = PI
const HalfPi : float = PI / 2.0

const summing_method_weighted_average : int = 0
const summing_method_prioritized : int = 1
const summing_method_dithered : int = 2

const behavior_type_none : int              = 0x00000
const behavior_type_seek : int              = 0x00002
const behavior_type_arrive : int            = 0x00008
const behavior_type_wander : int            = 0x00010
const behavior_type_separation : int        = 0x00040
const behavior_type_alignment : int         = 0x00080
const behavior_type_cohesion : int          = 0x00100
const behavior_type_wall_avoidance : int    = 0x00200
const behavior_type_obstacles_avoidance : int    = 0x00400
const behavior_type_flee : int    			= 0x00800

# a pointer to the owner of this instance
var agent = null

# the steering force created by the combined effect of all
# the selected behaviors
var steeringForce : Vector2 = Vector2()
 
# these can be used to keep track of friends, pursuers, or prey
var targetAgent1 = null
var targetAgent2 = null

var evadeTargetAgent = null

# the current target
var target : Vector2 = Vector2()


# a vertex buffer to contain the feelers rqd for wall avoidance
const feelersCount : int = 3;
var feelers : PoolVector2Array = PoolVector2Array()

# the length of the 'feeler/s' used in wall detection
var wallDetectionFeelerLength : float


# the current position on the wander circle the agent is
# attempting to steer towards
var wanderTarget : Vector2 = Vector2()

# explained above
var wanderJitter : float
var wanderRadius : float
var wanderDistance : float


# multipliers. These can be adjusted to effect strength of the  
# appropriate behavior.
var weightSeparation : float
var weightAlignment : float
var weightCohesion : float
var weightWander : float
var weightWallAvoidance : float
var weightObstaclesAvoidance : float
var weightSeek : float
var weightFlee : float
var weightArrive : float


# how far the agent can 'see'
var viewDistance : float

# binary flags to indicate whether or not a behavior should be active
var flags : int = 0

  
# Arrive makes use of these to determine how quickly a Raven_Bot
# should decelerate to its target
const deceleration_slow : int = 3
const deceleration_normal : int = 2
const deceleration_fast : int = 1

# default
var deceleration : int

# is cell space partitioning to be used or not?
var cellSpaceOn : bool = false
 
# what type of method is used to sum any active behavior
onready var summingMethod : int = summing_method_prioritized

func _init(bot_agent, params : Resource):
	agent = bot_agent
	
	wallDetectionFeelerLength = params.WallDetectionFeelerLength
	wanderJitter = params.WanderJitterPerSec
	wanderRadius = params.WanderRad
	wanderDistance = params.WanderDist
	weightSeparation = params.SeparationWeight
	weightAlignment = params.AlignmentWeight
	weightCohesion = params.CohesionWeight
	weightWander = params.WanderWeight
	weightWallAvoidance = params.WallAvoidanceWeight
	weightObstaclesAvoidance = params.ObstaclesAvoidanceWeight
	weightSeek = params.SeekWeight
	weightFlee = params.FleeWeight
	weightArrive = params.ArriveWeight
	viewDistance = params.ViewDistance
	deceleration = deceleration_normal
	
	feelers.resize(feelersCount)
	
	# stuff for the wander behavior
	var theta : float = randf() * 2 * PI
	
	# create a vector to a target position on the wander circle
	wanderTarget = Vector2(wanderRadius * cos(theta), wanderRadius * sin(theta))


# this function tests if a specific bit of m_iFlags is set
func On(bt : int) -> bool:
	return (flags & bt) == bt

#--------------------- AccumulateForce ----------------------------------
#
#  This function calculates how much of its max steering force the 
#  vehicle has left to apply and then applies that amount of the
#  force to add.
#------------------------------------------------------------------------
func AccumulateForce(ForceToAdd : Vector2) -> bool:
	# calculate how much steering force the vehicle has used so far
	var MagnitudeSoFar : float = steeringForce.length()
	
	# calculate how much steering force remains to be used by this vehicle
	var MagnitudeRemaining : float = agent.maxForce - MagnitudeSoFar
	
	# return false if there is no more force left to use
	if MagnitudeRemaining <= 0.0:
		return false
	
	# calculate the magnitude of the force we want to add
	var MagnitudeToAdd : float = ForceToAdd.length()
	
	# if the magnitude of the sum of ForceToAdd and the running total
	# does not exceed the maximum force available to this vehicle, just
	# add together. Otherwise add as much of the ForceToAdd vector is
	# possible without going over the max.
	if MagnitudeToAdd < MagnitudeRemaining:
		steeringForce += ForceToAdd
	else:
		MagnitudeToAdd = MagnitudeRemaining
		
		# add it to the steering force
		steeringForce += (ForceToAdd.normalized() * MagnitudeToAdd)
	
	return true

# creates the antenna utilized by the wall avoidance behavior
func CreateFeelers() -> void:
	# feeler pointing straight in front
	feelers[0] = agent.global_position + wallDetectionFeelerLength * agent.heading #* agent.Speed()
	
	# feeler to left
	feelers[1] = agent.global_position + wallDetectionFeelerLength * agent.heading.rotated(HalfPi * 3.5);
	
	# feeler to right
	feelers[2] = agent.global_position + wallDetectionFeelerLength * agent.heading.rotated(HalfPi * 0.5);

# BEGIN BEHAVIOR DECLARATIONS

#------------------------------- Seek -----------------------------------
#
#  Given a target, this behavior returns a steering force which will
#  direct the agent towards the target
#------------------------------------------------------------------------
func Seek(targ : Vector2) -> Vector2: 
	var DesiredVelocity : Vector2 = (targ - agent.global_position).normalized() * agent.maxSpeed
	
	return (DesiredVelocity - agent.velocity)

#------------------------------- Flee -----------------------------------
#
#  Given a target, this behavior returns a steering force which will
#  direct the agent against the target
#------------------------------------------------------------------------
func Flee(targ : Vector2) -> Vector2: 
	var DesiredVelocity : Vector2 = (agent.global_position - targ).normalized() * agent.maxSpeed
	
	return (DesiredVelocity - agent.velocity)

#--------------------------- Arrive -------------------------------------
#
#  This behavior is similar to seek but it attempts to arrive at the
#  target with a zero velocity
#------------------------------------------------------------------------
func Arrive(targ : Vector2, decel : int) -> Vector2:
	var ToTarget : Vector2 = targ - agent.global_position
	
	# calculate the distance to the target
	var dist : float = ToTarget.length()
	
	if dist > 0:
		# because Deceleration is enumerated as an int, this value is required
		# to provide fine tweaking of the deceleration..
		var DecelerationTweaker : float = 0.3
		
		# calculate the speed required to reach the target given the desired
		# deceleration
		var speed : float =  dist / (decel * DecelerationTweaker)    
		
		# make sure the velocity does not exceed the max
		speed = min(speed, agent.maxSpeed)
		
		# from here proceed just like Seek except we don't need to normalize 
		# the ToTarget vector because we have already gone to the trouble
		# of calculating its length: dist. 
		var DesiredVelocity : Vector2 =  ToTarget * speed / dist;
		
		return (DesiredVelocity - agent.velocity)
	
	return Vector2()

#--------------------------- Wander -------------------------------------
#
#  This behavior makes the agent wander about randomly
#------------------------------------------------------------------------
func Wander() -> Vector2:
	# first, add a small random vector to the target's position
	wanderTarget += Vector2((randf()*2 - 1) * wanderJitter, (randf()*2 - 1) * wanderJitter)
	
	# reproject this new vector back on to a unit circle
	wanderTarget = wanderTarget.normalized()
	
	# increase the length of the vector to the same as the radius
	# of the wander circle
	wanderTarget *= wanderRadius;
	
	# move the target into a position WanderDist in front of the agent
	var targ : Vector2 = wanderTarget + Vector2(wanderDistance, 0);
	
	# project the target into world space
	var Target : Vector2 = agent.global_transform.xform(targ)

	# and steer towards it
	return Target - agent.global_position

# this returns a steering force which will keep the agent away from any
# walls it may encounter
func WallAvoidance() -> Vector2:
	# the feelers are contained in a PoolVector2Array(), feelers
	CreateFeelers()
	
	var DistToThisIP : float    = 0.0;
	var DistToClosestIP : float = MaxFloat;
	
	# this will hold an index into the vector of walls
	var ClosestObstacle = null
	
	var SteeringForce : Vector2
	var ClosestPoint : Vector2  # holds the closest intersection point
	var ClosestNormal : Vector2  # holds the closest intersection normal
	
	var space_state : Physics2DDirectSpaceState = agent.get_world_2d().direct_space_state
	# examine each feeler in turn
	for feeler in feelers:
		var intersection : = space_state.intersect_ray(agent.global_position, feeler, [agent])

		if not intersection.empty() and intersection.collider.is_in_group("Obstacles"):
			DistToThisIP = (agent.global_position - intersection.position).length()
			# is this the closest found so far? If so keep a record
			if DistToThisIP < DistToClosestIP:
				DistToClosestIP = DistToThisIP
				ClosestObstacle = intersection.collider
				ClosestPoint = intersection.position
				ClosestNormal = intersection.normal
		
		# if an intersection point has been detected, calculate a force  
		# that will direct the agent away
		if ClosestObstacle:
			# calculate by what distance the projected position of the agent
			# will overshoot the wall
			var OverShoot : Vector2 = feeler - ClosestPoint
			
			# create a force in the direction of the wall normal, with a 
			# magnitude of the overshoot
			SteeringForce = ClosestNormal * OverShoot.length()
	
		# next feeler
	
	return SteeringForce


func ObstaclesAvoidance(obstacles : Array) -> Vector2:
	# If we aren't moving much, we don't need to try avoid
	if not agent or agent.velocity.length_squared() <= agent.boundingRadius():
		return Vector2()
	
	# If we aren't going to collide, we don't need to avoid
	if obstacles.empty():
		return Vector2()

	# find closest obstacle
	var distToObstacle : float = INF
	var closestObstacle : Node = null
	for obstacleId in obstacles:
		var obstacle = instance_from_id(obstacleId)
		if not obstacle:
			continue
		
		var dist : float = agent.global_position.distance_to(obstacle.global_position)
		if dist < distToObstacle:
			distToObstacle = dist
			closestObstacle = obstacle
	
	if not closestObstacle:
		return Vector2()

	# Add our velocity and the other Agents velocity
	# If this makes the total length longer than the individual length of one of them, then we are going in the same direction
	var ourVelocityLengthSquared : float = agent.velocity.length_squared()
	var obstacleVelocity : Vector2 = closestObstacle.velocity if "velocity" in closestObstacle else Vector2()
	var combinedVelocity : Vector2 = agent.velocity + obstacleVelocity
	var combinedVelocityLengthSquared : float = combinedVelocity.length_squared()

	# We are going in the same direction and they aren't avoiding
	if combinedVelocityLengthSquared > ourVelocityLengthSquared and (not "avoidanceDirection" in closestObstacle or closestObstacle.avoidanceDirection == null):
		return Vector2()
		
	# We need to Steer to go around it, we assume the other shape is also a circle

	var vectorInOtherDirection : Vector2 = closestObstacle.global_position - agent.global_position

	# Are we more left or right of them
	var isLeft : bool = false
	if "avoidanceDirection" in closestObstacle and closestObstacle.avoidanceDirection != null:
		# If they are avoiding, avoid with the same direction as them, so we go the opposite way
		isLeft = closestObstacle.avoidanceDirection;
	else:
		# http://stackoverflow.com/questions/13221873/determining-if-one-2d-vector-is-to-the-right-or-left-of-another
		var dot : float = agent.velocity.x * -vectorInOtherDirection.y + agent.velocity.y * vectorInOtherDirection.x
		isLeft = dot > 0.0
	
	agent.avoidanceDirection = isLeft

	# Calculate a right angle of the vector between us
	# http://www.gamedev.net/topic/551175-rotate-vector-90-degrees-to-the-right/#entry4546571
	var resultVector : Vector2 = Vector2(-vectorInOtherDirection.y, vectorInOtherDirection.x) if isLeft else Vector2(vectorInOtherDirection.y, -vectorInOtherDirection.x)
	resultVector = resultVector.normalized()

	# Move it out based on our radius + theirs
	resultVector *= agent.boundingRadius() + (closestObstacle.boundingRadius() if closestObstacle.has_method("boundingRadius") else 0.0)

	# Steer torwards it, increasing force based on how close we are
	return steerTowards(agent, resultVector) / distToObstacle
		

func steerTowards(agent, desiredDirection) -> Vector2:
	# Multiply our direction by speed for our desired speed
	var desiredVelocity : Vector2 = desiredDirection * agent.maxSpeed

	# The velocity change we want
	var velocityChange : Vector2 = desiredVelocity - agent.velocity
	# Convert to a force
	return velocityChange * (agent.maxForce / agent.maxSpeed)
#---------------------------- Separation --------------------------------
#
# this calculates a force repelling from the other neighbors
#------------------------------------------------------------------------  
func Separation(flocking_neighbors : Array) -> Vector2:
	if not agent:
		return Vector2()

	# iterate through all the neighbors and calculate the vector from the
	var SteeringForce : Vector2
	var neighborsCounted : int = 0

	for neighbor in flocking_neighbors:
		# make sure this agent isn't included in the calculations and that
		# the agent being examined is close enough. ***also make sure it doesn't
		# include the evade target ***
		if neighbor == agent or neighbor == evadeTargetAgent:
			continue

		if not neighbor:
			continue
			
		var ToAgent : Vector2 = agent.global_position - neighbor.global_position

		SteeringForce += ToAgent / agent.boundingRadius()

		neighborsCounted += 1

	if neighborsCounted > 0:
		SteeringForce /= neighborsCounted

	return SteeringForce

#---------------------------- Alignment --------------------------------
#
# this calculates a force to set one heading with neighbors
#------------------------------------------------------------------------
func Alignment(neighbors : Array) -> Vector2:
	if not agent:
		return Vector2()

	# iterate through all the neighbors and calculate the average heading vector
	var averageHeading : Vector2
	var neighborsCounted : int = 0

	for neighbor in neighbors:
#		if neighborId == agent.get_instance_id() or neighborId == evadeTargetAgent:
#			continue

#		var neighbor = instance_from_id(neighborId)
		if not neighbor:
			continue
			
		if neighbor == agent or neighbor == evadeTargetAgent:
			continue
		
		var neighborVelocity : Vector2 = neighbor.velocity

		if neighborVelocity.length() > 0:
			averageHeading += neighborVelocity.normalized()
			neighborsCounted += 1

	var force : Vector2
	if neighborsCounted > 0:
		averageHeading /= neighborsCounted

		# Steer towards that heading
		var desired : Vector2 = averageHeading * agent.maxSpeed
		force = desired - agent.velocity
		force *= (agent.maxForce / agent.maxSpeed)

	return force

#---------------------------- Separation --------------------------------
#
# this calculates a force repelling from the other neighbors
#------------------------------------------------------------------------
func Cohesion(neighbors : Array) -> Vector2:
	if not agent:
		return Vector2()

	# Start with just our position
	var centerOfMass : Vector2 = agent.global_position
	var neighborsCounted : int = 1

	for neighbor in neighbors:
#		if neighborId == agent.get_instance_id() or neighborId == evadeTargetAgent:
#			continue
		
#		var neighbor = instance_from_id(neighborId)
		if not neighbor:
			continue
			
		if neighbor == agent or neighbor == evadeTargetAgent:
			continue
			
		centerOfMass += neighbor.global_position

		neighborsCounted += 1

	var SteeringForce : Vector2

	if neighborsCounted > 1:
		centerOfMass /= neighborsCounted

		SteeringForce = Seek(centerOfMass)

	return SteeringForce


# END BEHAVIOR DECLARATIONS

#---------------------- CalculatePrioritized ----------------------------
#
#  this method calls each active steering behavior in order of priority
#  and acumulates their forces until the max steering force magnitude
#  is reached, at which time the function returns the steering force 
#  accumulated to that  point
#------------------------------------------------------------------------
func CalculatePrioritized() -> Vector2:    
	var force : Vector2 = Vector2()
	
	if On(behavior_type_wall_avoidance):
		force = WallAvoidance() * weightWallAvoidance
		
		if not AccumulateForce(force):
			return steeringForce
			
	if On(behavior_type_obstacles_avoidance):
		force = ObstaclesAvoidance(agent.get_obstacles()) * weightObstaclesAvoidance
		
		if not AccumulateForce(force):
			return steeringForce

	# these next three can be combined for flocking behavior (wander is
	# also a good behavior to add into this mix)
	
	if On(behavior_type_separation):
		force = Separation(agent.get_flocking_neighbors()) * weightSeparation
	
		if not AccumulateForce(force):
			return steeringForce
			
	if On(behavior_type_alignment):
		force = Alignment(agent.get_neighbors()) * weightAlignment
	
		if not AccumulateForce(force):
			return steeringForce
			
	if On(behavior_type_cohesion):
		force = Cohesion(agent.get_neighbors()) * weightCohesion
	
		if not AccumulateForce(force):
			return steeringForce
	
	if On(behavior_type_seek):
		force = Seek(target) * weightSeek
		
		if not AccumulateForce(force):
			return steeringForce
	
	if On(behavior_type_flee):
		force = Flee(target) * weightFlee
		
		if not AccumulateForce(force):
			return steeringForce
	
	if On(behavior_type_arrive):
		force = Arrive(target, deceleration) * weightArrive
		
		if not AccumulateForce(force):
			return steeringForce;
	
	if On(behavior_type_wander):
		force = Wander() * weightWander
		
		if not AccumulateForce(force):
			return steeringForce

	return steeringForce

#----------------------- Calculate --------------------------------------
#
#  calculates the accumulated steering force according to the method set
#  in summingMethod
#------------------------------------------------------------------------
func Calculate() -> Vector2:
	# reset the steering force
	steeringForce = Vector2()
	
#	# tag neighbors if any of the following 3 group behaviors are switched on
#	if On(separation):
#		m_pWorld->TagRaven_BotsWithinViewRange(m_pRaven_Bot, viewDistance);
	
	steeringForce = CalculatePrioritized()
	
	return steeringForce

# calculates the component of the steering force that is parallel
# with the bot heading
func ForwardComponent() -> float:
	return agent.heading.dot(steeringForce);

# calculates the component of the steering force that is perpendicuar
# with the bot heading
func SideComponent() -> float:
	return agent.side.dot(steeringForce);


func SetTarget(t : Vector2) -> void:
	target = t
	
func Target() -> Vector2:
	return target

func SetTargetAgent1(Agent) -> void:
	targetAgent1 = Agent
	
func SetTargetAgent2(Agent) -> void:
	targetAgent2 = Agent

func SetSummingMethod(sm : int) -> void:
	summingMethod = sm


func SeekOn() -> void:
	flags |= behavior_type_seek

func FleeOn() -> void:
	flags |= behavior_type_flee
	
func ArriveOn() -> void:
	flags |= behavior_type_arrive
	
func WanderOn() -> void:
	flags |= behavior_type_wander
	
func SeparationOn() -> void:
	flags |= behavior_type_separation
	
func AlignmentOn() -> void:
	flags |= behavior_type_alignment
	
func CohesionOn() -> void:
	flags |= behavior_type_cohesion
	
func WallAvoidanceOn() -> void:
	flags |= behavior_type_wall_avoidance

func ObstaclesAvoidanceOn() -> void:
	flags |= behavior_type_obstacles_avoidance

func SeekOff() -> void:
	if On(behavior_type_seek):
		flags ^= behavior_type_seek
		
func FleeOff() -> void:
	if On(behavior_type_flee):
		flags ^= behavior_type_flee
		
func ArriveOff() -> void:
	if On(behavior_type_arrive):
		flags ^= behavior_type_arrive
		
func WanderOff() -> void:
	if On(behavior_type_wander):
		flags ^= behavior_type_wander
		
func SeparationOff() -> void:
	if On(behavior_type_separation):
		flags ^= behavior_type_separation
		
func AlignmentOff() -> void:
	if On(behavior_type_alignment):
		flags ^= behavior_type_alignment
		
func CohesionOff() -> void:
	if On(behavior_type_cohesion):
		flags ^= behavior_type_cohesion
		
func WallAvoidanceOff() -> void:
	if On(behavior_type_wall_avoidance):
		flags ^= behavior_type_wall_avoidance
		
func ObstalesAvoidanceOff() -> void:
	if On(behavior_type_obstacles_avoidance):
		flags ^= behavior_type_obstacles_avoidance

func SeekIsOn() -> bool:
	return On(behavior_type_seek)

func FleeIsOn() -> bool:
	return On(behavior_type_flee)
	
func ArriveIsOn() -> bool:
	return On(behavior_type_arrive)
	
func WanderIsOn() -> bool:
	return On(behavior_type_wander)
	
func SeparationIsOn() -> bool:
	return On(behavior_type_separation)
	
func CohesionIsOn() -> bool:
	return On(behavior_type_cohesion)

func AlignmentIsOn() -> bool:
	return On(behavior_type_alignment)

func WallAvoidanceIsOn() -> bool:
	return On(behavior_type_wall_avoidance)
	
func ObstaclesAvoidanceIsOn() -> bool:
	return On(behavior_type_obstacles_avoidance)

func draw(canvas : CanvasItem):
	if On(behavior_type_wall_avoidance):
		for feeler in feelers:
			var local_feeler = agent.global_transform.xform_inv(feeler)
			canvas.draw_line(Vector2(), local_feeler, Color(1.0, 0.0, 0.0))
