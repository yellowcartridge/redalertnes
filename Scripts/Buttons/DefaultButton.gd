extends TextureButton

signal was_pressed
signal was_right_pressed

export (float) var progress : float = 1.0

func _pressed():
	emit_signal("was_pressed")
	
func connect_me(obj, button_name):
	connect("was_pressed", obj, "button_pressed", [button_name])
	connect("was_right_pressed", obj, "button_pressed_right", [button_name])
	name = button_name


func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.pressed:
			emit_signal("was_right_pressed")
