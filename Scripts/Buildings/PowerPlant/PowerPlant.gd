extends Building
class_name PowerPlant

func _init():
	buildingName = "USSRPowerPlant"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": 0, "y": 0, "name": "PowerPlant_top_left"},
			{"x": 1, "y": 0, "name": "PowerPlant_top_right"},
			{"x": 0, "y": 1, "name": "PowerPlant_bottom_left"},
			{"x": 1, "y": 1, "name": "PowerPlant_bottom_right"}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": 0, "y": 0, "name": "USSRPowerPlant_top_left"},
			{"x": 1, "y": 0, "name": "USSRPowerPlant_top_right"},
			{"x": 0, "y": 1, "name": "USSRPowerPlant_bottom_left"},
			{"x": 1, "y": 1, "name": "USSRPowerPlant_bottom_right"}
		]
	}
	
	dependencyBuildings = [
		"USSRConstructionYard"
	]
