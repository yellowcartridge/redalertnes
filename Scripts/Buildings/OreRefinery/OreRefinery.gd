extends Building
class_name OreRefinery

onready var deliveryPosition : Position2D = $DeliveryPosition

func _init():
	buildingName = "OreRefinery"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "OreRefinery_top_left"},
			{"x": 0, "y": -1, "name": "OreRefinery_top_center"},
			{"x": 1, "y": -1, "name": "OreRefinery_top_right"},
			{"x": -1, "y": 0, "name": "OreRefinery_center_left"},
			{"x": 0, "y": 0, "name": "OreRefinery_center_center"},
			{"x": 1, "y": 0, "name": "OreRefinery_center_right"},
			{"x": -1, "y": 1, "name": "BlueOreRefinery_bottom_left"},
			{"x": 0, "y": 1, "name": "BlueOreRefinery_bottom_center"},
			{"x": 1, "y": 1, "name": "OreRefinery_bottom_right"}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "USSROreRefinery_top_left"},
			{"x": 0, "y": -1, "name": "USSROreRefinery_top_center"},
			{"x": 1, "y": -1, "name": "USSROreRefinery_top_right"},
			{"x": -1, "y": 0, "name": "USSROreRefinery_center_left"},
			{"x": 0, "y": 0, "name": "USSROreRefinery_center_center"},
			{"x": 1, "y": 0, "name": "USSROreRefinery_center_right"},
			{"x": -1, "y": 1, "name": "BlueOreRefinery_bottom_left"},
			{"x": 0, "y": 1, "name": "BlueOreRefinery_bottom_center"},
			{"x": 1, "y": 1, "name": "USSROreRefinery_bottom_right"}
		]
	}
	
	dependencyBuildings = [
	]
	
	cost = 1000
	productionTime = 60
	health = 2000
	max_health = 2000

func get_delivery_position() -> Vector2:
	return deliveryPosition.global_position
