extends Building
class_name Barraks

func _init():
	buildingName = "Barraks"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "Barraks_top_left"},
			{"x": 0, "y": -1, "name": "Barraks_top_center"},
			{"x": 1, "y": -1, "name": "Barraks_top_right"},
			{"x": -1, "y": 0, "name": "Barraks_center_left"},
			{"x": 0, "y": 0, "name": "Barraks_center_center"},
			{"x": 1, "y": 0, "name": "Barraks_center_right"},
			{"x": -1, "y": 1, "name": "Barraks_bottom_left"},
			{"x": 0, "y": 1, "name": "Barraks_bottom_center"},
			{"x": 1, "y": 1, "name": "Barraks_bottom_right"}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "USSRBarraks_top_left"},
			{"x": 0, "y": -1, "name": "USSRBarraks_top_center"},
			{"x": 1, "y": -1, "name": "USSRBarraks_top_right"},
			{"x": -1, "y": 0, "name": "USSRBarraks_center_left"},
			{"x": 0, "y": 0, "name": "USSRBarraks_center_center"},
			{"x": 1, "y": 0, "name": "USSRBarraks_center_right"},
			{"x": -1, "y": 1, "name": "USSRBarraks_bottom_left"},
			{"x": 0, "y": 1, "name": "USSRBarraks_bottom_center"},
			{"x": 1, "y": 1, "name": "USSRBarraks_bottom_right"}
		]
	}
	
	dependencyBuildings = [
		"ConstructionYard",
		"PowerPlant"
	]

