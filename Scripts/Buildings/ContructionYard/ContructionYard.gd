extends Building
class_name ConstructionYard

func _init():
	buildingName = "ConstructionYard"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "construction_yard_left_top"},
			{"x": 0, "y": -1, "name": "construction_yard_center_top"},
			{"x": 1, "y": -1, "name": "construction_yard_right_top"},
			{"x": -1, "y": 0, "name": "construction_yard_left_center"},
			{"x": 0, "y": 0, "name": "construction_yard_center_center"},
			{"x": 1, "y": 0, "name": "construction_yard_right_center"},
			{"x": -1, "y": 1, "name": "construction_yard_left_bottom"},
			{"x": 0, "y": 1, "name": "construction_yard_center_bottom"},
			{"x": 1, "y": 1, "name": "construction_yard_right_bottom"}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "USSRConstructionYard_top_left"},
			{"x": 0, "y": -1, "name": "USSRConstructionYard_top_center"},
			{"x": 1, "y": -1, "name": "USSRConstructionYard_top_right"},
			{"x": -1, "y": 0, "name": "USSRConstructionYard_center_left"},
			{"x": 0, "y": 0, "name": "USSRConstructionYard_center_center"},
			{"x": 1, "y": 0, "name": "USSRConstructionYard_center_right"},
			{"x": -1, "y": 1, "name": "USSRConstructionYard_bottom_left"},
			{"x": 0, "y": 1, "name": "USSRConstructionYard_bottom_center"},
			{"x": 1, "y": 1, "name": "USSRConstructionYard_bottom_right"}
		]
	}
