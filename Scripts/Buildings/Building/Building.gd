extends Node2D
class_name Building

export (String) var waterTileName: String = "water_tile"

export (Dictionary) var red_tiles : Dictionary = {}
export (Dictionary) var blue_tiles : Dictionary = {}

export (bool) var can_collect_ore : bool = false

export (Global.PlayerColor) var color : int = Global.PlayerColor.RED

export (String) var buildingName : String = ""

export (int) var productionTime : int = 3
export (int) var cost : int = 100
export (int) var energyConsume : int = 10

export (Array) var dependencyBuildings : Array = []
export (Array) var production : Array = []

export (float) var max_health : float = 100.0
export (float) var health : float = 100.0

export (int) var ownerId : int = 0

export (Global.Orientation) var currentOrientation : int = Global.Orientation.RIGHT

var isDead : bool = false

signal building_mouse_enter
signal building_mouse_exit
signal building_destroyed

var attackedBy

func _ready():
	pass
	#connect("building_mouse_enter", get_parent(), "building_mouse_enter")
	#connect("building_mouse_exit", get_parent(), "building_mouse_exit")
	#connect("building_destroyed", get_parent(), "building_destroyed")

func _on_Building_mouse_entered():
	emit_signal("building_mouse_enter", get_instance_id())


func _on_Building_mouse_exited():
	emit_signal("building_mouse_exit", get_instance_id())

func is_enemy() -> bool:
	return is_in_group("Enemies")

func add_damage(amount : float, attacker):
	attackedBy = attacker
	
	health -= amount
	
	if health <= 0.0:
		isDead = true
		emit_signal("building_destroyed", get_instance_id(), ownerId, global_position, get_tiles())
		queue_free()

func get_tiles() -> Array:
	if color == Global.PlayerColor.BLUE:
		if blue_tiles.has(currentOrientation):
			return blue_tiles[currentOrientation]
	else:
		if red_tiles.has(currentOrientation):
			return red_tiles[currentOrientation]
			
	return []

func calculate_orientation(tilemap : TileMap, tileToPlace : Vector2) -> void:
	pass
