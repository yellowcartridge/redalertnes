extends Building
class_name USSRFactory

func _init():
	buildingName = "USSRFactory"
	
	blue_tiles = [
		{"x": -1, "y": -1, "name": "Factory_top_left"},
		{"x": 0, "y": -1, "name": "Factory_top_center"},
		{"x": 1, "y": -1, "name": "Factory_top_right"},
		{"x": -1, "y": 0, "name": "Factory_center_left"},
		{"x": 0, "y": 0, "name": "Factory_center_center"},
		{"x": 1, "y": 0, "name": "Factory_center_right"},
		{"x": -1, "y": 1, "name": "Factory_bottom_left"},
		{"x": 0, "y": 1, "name": "Factory_bottom_center"},
		{"x": 1, "y": 1, "name": "Factory_bottom_right"}
	]
	
	red_tiles = [
		{"x": -1, "y": -1, "name": "USSRFactory_top_left"},
		{"x": 0, "y": -1, "name": "USSRFactory_top_center"},
		{"x": 1, "y": -1, "name": "USSRFactory_top_right"},
		{"x": -1, "y": 0, "name": "USSRFactory_center_left"},
		{"x": 0, "y": 0, "name": "USSRFactory_center_center"},
		{"x": 1, "y": 0, "name": "USSRFactory_center_right"},
		{"x": -1, "y": 1, "name": "USSRFactory_bottom_left"},
		{"x": 0, "y": 1, "name": "USSRFactory_bottom_center"},
		{"x": 1, "y": 1, "name": "USSRFactory_bottom_right"}
	]
	
	dependencyBuildings = [
		"USSRConstructionYard",
		"USSRPowerPlant"
	]
