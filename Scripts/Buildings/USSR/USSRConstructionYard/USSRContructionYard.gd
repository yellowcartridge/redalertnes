extends Building
class_name USSRConstructionYard

func _init():
	buildingName = "USSRConstructionYard"
	
	blue_tiles = [
		{"x": -1, "y": -1, "name": "ConstructionYard_top_left"},
		{"x": 0, "y": -1, "name": "ConstructionYard_top_center"},
		{"x": 1, "y": -1, "name": "ConstructionYard_top_right"},
		{"x": -1, "y": 0, "name": "ConstructionYard_center_left"},
		{"x": 0, "y": 0, "name": "ConstructionYard_center_center"},
		{"x": 1, "y": 0, "name": "ConstructionYard_center_right"},
		{"x": -1, "y": 1, "name": "ConstructionYard_bottom_left"},
		{"x": 0, "y": 1, "name": "ConstructionYard_bottom_center"},
		{"x": 1, "y": 1, "name": "ConstructionYard_bottom_right"}
	]
	
	red_tiles = [
		{"x": -1, "y": -1, "name": "USSRConstructionYard_top_left"},
		{"x": 0, "y": -1, "name": "USSRConstructionYard_top_center"},
		{"x": 1, "y": -1, "name": "USSRConstructionYard_top_right"},
		{"x": -1, "y": 0, "name": "USSRConstructionYard_center_left"},
		{"x": 0, "y": 0, "name": "USSRConstructionYard_center_center"},
		{"x": 1, "y": 0, "name": "USSRConstructionYard_center_right"},
		{"x": -1, "y": 1, "name": "USSRConstructionYard_bottom_left"},
		{"x": 0, "y": 1, "name": "USSRConstructionYard_bottom_center"},
		{"x": 1, "y": 1, "name": "USSRConstructionYard_bottom_right"}
	]
	
	productionTime = 30
	cost = 100
	health = 5000
	max_health = 5000
