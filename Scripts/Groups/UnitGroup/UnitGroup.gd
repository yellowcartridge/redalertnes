extends Node
class_name UnitGroup

export (float) var maxGroupSpeed : float = 0.0
export (Vector2) var groupCentroid : Vector2 = Vector2()
export (Vector2) var groupGoal : Vector2 = Vector2() setget set_goal, get_goal

var units : Array = []
var groupLeader : int = -1

func get_size() -> int:
	return units.size()
	
func set_goal(goal : Vector2) -> void:
	groupGoal = goal
	
func get_goal() -> Vector2:
	return groupGoal

func add_unit(unitId : int) -> void:
	if not unitId in units:
		units.push_back(unitId)
	else:
		print("Unit ", unitId, " is already in group")
		
func remove_unit(unitId : int) -> void:
	if unitId in units:
		units.erase(unitId)
	else:
		print("Unit ", unitId, " is not in this group")
		
func is_unit_in_group(unitId : int) -> bool:
	return units.has(unitId)
