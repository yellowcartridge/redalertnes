extends KinematicBody2D
class_name Unit

enum States {
	IDLE,
	MOVE,
	MOVE_AWAY,
	RETURN_TO_POS,
	DEFEND,
	ATTACK,
	HARVEST,
	DELIVER,
	FIND_ORE,
	FIND_REFINERY,
	ESCAPE,
	PROCESS_CELL_MOVEMENT
}

export (Global.PlayerColor) var color : int = Global.PlayerColor.RED
export (bool) var rotatable : bool = true

onready var state : int = States.IDLE
onready var plannedState : int = States.IDLE

const steeringParams : Resource = preload("res://Scripts/Steering/DefaultParams.tres")
onready var steering : SteeringBehavior = SteeringBehavior.new(self, steeringParams)
onready var sprite : AnimatedSprite = get_node_or_null("AnimatedSprite")
onready var visibilityNotifier : VisibilityNotifier2D = get_node_or_null("VisibilityNotifier")
onready var moveTimer : Timer = get_node_or_null("MoveTimer")
onready var attackTimer : Timer = get_node_or_null("AttackTimer")
onready var obstacleAvoidanceArea : Area2D = get_node_or_null("ObstaclesAvoidanceArea")
onready var neighborsArea : Area2D = get_node_or_null("NeighborsArea")
onready var sensoryArea : Area2D = get_node_or_null("SensoryArea")

export (String) var unitName : String = ""
export (int) var productionTime : int = 3
export (int) var cost : int = 100
export (int) var energyConsume : int = 0

export (Array) var dependencyBuildings : Array = []

export (float) var mass : float = 1.0
export (float) var MaxSpeed = 32.0
  
# the maximum speed this entity may travel at.
export (float) var maxSpeed = MaxSpeed

# the maximum force this entity can produce to power itself 
# (think rockets and thrust)
export (float) var maxForce = 50.0
  
# the maximum rate (radians per second)this vehicle can rotate         
export (float) var maxTurnRate = 2.0

export (float) var speed : float = 32.0
export (bool) var selected : bool = false setget set_selected

export (bool) var expandable : bool = false
export (String) var expansionName : String = ""
export (bool) var can_harvest : bool = false
export (bool) var has_ore : bool = false
export (int) var gain : int = 500
export (int) var harvestTime : int = 5
export (NodePath) var harvestTimerPath : NodePath
onready var harvestTimer : Node = get_node_or_null(harvestTimerPath)
onready var ore_tile_position : Vector2 = Vector2()

export (ProductionRecord.MovableMaterial) var movableMaterial : int = ProductionRecord.MovableMaterial.GROUND
export (ProductionRecord.ProductionType) var type : int = ProductionRecord.ProductionType.INFANTRY

export (bool) var can_attack : bool = false
export (float) var damage : float = 10.0
export (float) var shoot_cooldown : float = 2.0
export (float) var max_distance_for_attack : float = 50.0

export (float) var attack_response_time : float = 0.3

export (float) var max_health : float = 100.0
export (float) var health : float = 100.0

export (int) var ownerId : int = 0
export (int) var groupId : int = 0

var tilemap : TileMap = null

var isDead : bool = false
var isExpanded : bool = false

var move_p = false
var moveTarget = Vector2()
var path = PoolVector2Array()
var initial_position = Vector2()

var away_p = false
var originPoint = Vector2()

var attack_p = false
var isAttackSatisfied : bool = false
var attackTargetId : int = -1
var shooting_enabled : bool = true
var shaking_enabled : bool = false
var attackedBy = null

var escape_p = false
var escapeTarget : Vector2 = Vector2()

var avoidanceDirection = null

var tilemapPosition : Vector2 = Vector2()

var mouseOnUnit : bool = false

var velocity : Vector2 = Vector2()

var navigation : Navigation2D = null
var movableMaterials : Array = []

var stuck : bool = false

onready var collisionShape : CollisionShape2D = get_node_or_null("CollisionShape2D")
onready var sensoryShape : CollisionShape2D = get_node_or_null("SensoryArea/CollisionShape2D")

onready var noise : OpenSimplexNoise = OpenSimplexNoise.new()

onready var heading : Vector2 = Vector2(1, 0)
onready var side : Vector2 = heading.rotated(90.0)

onready var neighbors: Array = []
onready var flocking_neighbors: Array = []
onready var obstacles: Array = []

onready var brain : GoalComposite = GoalThink.new(self)

signal deselect_others
signal was_selected
signal was_deselected
signal try_to_expand
signal unit_destroyed

signal enemy_clicked

signal unit_mouse_enter
signal unit_mouse_exit

signal request_closest_ore_tile_position
signal request_closest_ore_refinery_position
signal request_closest_ore_tile_position_cb
signal request_closest_ore_refinery_position_cb
signal ore_tile_collected

signal gain_for_player

signal become_visible
signal become_unvisible

signal update_fog_of_war


func mouse_clicked():
	emit_signal("deselect_others", self)
	if not selected:
		set_selected(true)
		
	else:
		emit_signal("try_to_expand", self)

func set_selected(value):
	if selected != value:
		selected = value

		if selected:
			emit_signal("was_selected", self)
			#sprite.play("selected")
			
#			if mouseOnUnit and expandable and expansion:
#				emit_signal("expand_unit_ready", self)
		else:
			emit_signal("was_deselected", self)
#			if color == TeamColor.BLUE:
#				sprite.play("blue")
#			else:
#				sprite.play("red")


func clicked_by_hostile():
	emit_signal("enemy_clicked", self)

func _ready():
	if obstacleAvoidanceArea:
		obstacleAvoidanceArea.connect("body_entered", self, "_on_ObstaclesAvoidanceArea_body_entered")
		obstacleAvoidanceArea.connect("body_exited", self, "_on_ObstaclesAvoidance_body_exited")
	if neighborsArea:
		neighborsArea.connect("body_entered", self, "_on_NeighborsArea_body_entered")
		neighborsArea.connect("body_exited", self, "_on_NeighborsArea_body_exited")
	if sensoryArea:
		sensoryArea.connect("body_entered", self, "_on_SensoryArea_body_entered")
		sensoryArea.connect("body_exited", self, "_on_SensoryArea_body_exited")
	
	if harvestTimer:
		harvestTimer.connect("timeout", self, "_on_HarvestTimer_timeout")
	if moveTimer:
		moveTimer.connect("timeout", self, "_on_MoveTimer_timeout")
	if attackTimer:
		attackTimer.connect("timeout", self, "checkAttackTargetStatus")
	
	randomize()
	noise.seed = randi()
	noise.period = 4
	noise.octaves = 2
	
	if color == Global.PlayerColor.BLUE:
		sprite.play("blue")
	else:
		sprite.play("red")
	
	if tilemap:
		tilemapPosition = tilemap.world_to_map(global_position)
#		tilemap.take_cell_tilemap(get_instance_id(), tilemapPosition)
		emit_signal("update_fog_of_war", ownerId, tilemapPosition, sensoryRadius())

#	steering.SeparationOn()
#	steering.ObstaclesAvoidanceOn()

var noise_y = 0.0
export (Vector2) var max_shake_offset : Vector2 = Vector2(4, 4)

func shake():
	var shake_amount = 0.5
	var SHAKE_EXPONENT = 1.8
	var amount := pow(shake_amount, SHAKE_EXPONENT)

	noise_y += 1.0
	#rotation = max_rotation * amount * noise.get_noise_2d(noise.seed, noise_y)
	var offset : = Vector2(
		max_shake_offset.x * amount * noise.get_noise_2d(noise.seed * 2, noise_y),
		max_shake_offset.y * amount * noise.get_noise_2d(noise.seed * 3, noise_y)
	)
	
	global_position += offset
	

func _physics_process(delta):
	if shaking_enabled:
		shake()
		
	if can_harvest and brain.isIdle():
		brain.AddGoal_Mining()
		
	brain.Process()
	UpdateMovement(delta)
	update()
	return
		
	match state:
		States.IDLE:
			if can_harvest:
				if has_ore:
					state = States.FIND_REFINERY
				else:
					state = States.FIND_ORE
					
			if not flocking_neighbors.empty():
				move_unit_away()
			
		States.MOVE:
			if move_p:
				#path = smoothPath(navigation.get_simple_path(position, moveTarget, false))
				path = GetPathToPosition(moveTarget)
				if path.empty():
					return
				initial_position = position
				move_p = false
				attack_p = false

#				steering.AlignmentOn()
#				steering.CohesionOn()
				
				if tilemap:
					if position.distance_squared_to(path[0]) < 64 and tilemap and tilemap.world_to_map(position) == tilemap.world_to_map(path[0]):
						path.remove(0)
						initial_position = position
					
						if moveTimer:
							moveTimer.stop()
					#tilemapPosition = tilemap.world_to_map(initial_position)
					if path.size() > 0:
						move_to_next_path_target()
				
			elif path.size() > 0:
				move_towards(initial_position, path[0], delta)
			elif path.empty():
				print("On position")
				state = plannedState
				if moveTimer:
					moveTimer.stop()
		
		States.MOVE_AWAY:
			if away_p:
				steering.FleeOn()
				away_p = false
				
			UpdateMovement(delta)
			
			if flocking_neighbors.empty():
				return_unit_back()
			
		States.RETURN_TO_POS:
			steering.FleeOff()
			move(originPoint, plannedState)
			
		States.PROCESS_CELL_MOVEMENT:
			if tilemap:
				tilemap.connect("cell_released", self, "awaiting_cell_released")
		States.ATTACK:
			attack_action()
		States.FIND_ORE:
			harvest()
		States.FIND_REFINERY:
			deliver()
		States.HARVEST:
			start_harvest()
			
		States.DELIVER:
			start_delivery()
				
		States.ESCAPE:
			if escape_p:
				escape_p = false
				
				if position.distance_squared_to(escapeTarget) > 1:
					steering.SetTarget(escapeTarget)
				else:
					steering.SetTarget(position + Vector2(rand_range(1.0, 10.0), rand_range(1.0, 10.0)))
					
				steering.FleeOn()
			else:
				UpdateMovement(delta)
	
	
func awaiting_cell_released(entityId : int) -> void:
	if not tilemap:
		return
		
	tilemap.disconnect("cell_released", self, "awaiting_cell_released")

#func _draw():
#	brain.draw(self)
#
#	if path.empty():
#		return
#
#	var path_pos = global_transform.xform_inv(path[0])
#	draw_line(Vector2(), path_pos, Color.red)

func start_harvest() -> void:
	if has_ore:
		state = States.FIND_REFINERY
	else:
		state = States.HARVEST
		if $HarvestTimer.is_stopped():
			$HarvestTimer.start(harvestTime)
					
func _on_HarvestTimer_timeout():
	if state == States.HARVEST:
		has_ore = true
		state = States.FIND_REFINERY
	elif state == States.DELIVER:
		has_ore = false
		state = States.FIND_ORE

func start_delivery() -> void:
	if not has_ore:
		state = States.FIND_ORE
	else:
		state = States.DELIVER
		if $HarvestTimer.is_stopped():
			$HarvestTimer.start(harvestTime)

func collect_ore(ore_tile_pos : Vector2) -> void:
	emit_signal("ore_tile_collected", ore_tile_pos)

func collect_money() -> void:
	emit_signal("gain_for_player", ownerId, gain)

func _on_MoveTimer_timeout():
	stuck = true
	if state == States.MOVE:
		move_p = true

func move_towards(pos, point, delta):
	UpdateMovement(delta)
	
	if position.distance_squared_to(point) < 64 and tilemap and tilemap.world_to_map(position) == tilemap.world_to_map(point):
#		tilemap.release_cell_tilemap(get_instance_id(), tilemapPosition)
		path.remove(0)
		initial_position = position
#		tilemapPosition = tilemap.world_to_map(initial_position)
		
		if moveTimer:
			moveTimer.stop()
		
		move_to_next_path_target()

func move_to_next_path_target() -> void:
	if (path.size() > 1):
		steering.ArriveOff()
		steering.SeekOn()
	elif path.size() == 1:
		steering.ArriveOn()
		steering.SeekOff()
	else:
		steering.ArriveOff()
		steering.SeekOff()
#		tilemap.take_cell_tilemap(get_instance_id(), tilemap.world_to_map(position))
#			steering.SeparationOff()
#			steering.AlignmentOff()
#			steering.CohesionOff()
	
	if path.size() > 0:
		if tilemap:
#			var cell_info = tilemap.get_cell_info_tilemap(tilemap.world_to_map(path[0]))
			
#			if not cell_info.busy or cell_info.entityId == get_instance_id():
#			tilemap.take_cell_tilemap(get_instance_id(), tilemap.world_to_map(path[0]))
			var minimalTimeToReachPosition : float = CalculateTimeToReachPosition(path[0])
			if moveTimer:
				moveTimer.start(minimalTimeToReachPosition)
			
			steering.SetTarget(path[0])
#			else:
#				steering.SetTarget(initial_position)

#------------------------- UpdateMovement ------------------------------------
#
#  this method is called from the update method. It calculates and applies
#  the steering force for this time-step.
#-----------------------------------------------------------------------------
func UpdateMovement(delta : float) -> void:
	# calculate the combined steering force
	var force : Vector2 = steering.Calculate()

	# if no steering force is produced decelerate the player by applying a
	# braking force
	if force.length() == 0:
		var BrakingRate : float = 0.8
		
		velocity = velocity * BrakingRate
	
	# calculate the acceleration
	var accel : Vector2 = force / mass
	
	# update the velocity
	velocity += accel
	
	# make sure vehicle does not exceed maximum velocity
	velocity = velocity.clamped(maxSpeed)
	
	# if the vehicle has a non zero velocity the heading and side vectors must 
	# be updated
	if velocity.length() > 0:
		if rotatable:
			rotation = rotation_angle(velocity)
		
		heading = velocity.normalized().rotated(rotation)
		
		side = heading.rotated(deg2rad(90.0))
		
		if obstacleAvoidanceArea:
			obstacleAvoidanceArea.rotation = velocity.angle() - rotation
			
		
	
	# update the position
#	var collision = move_and_collide(velocity * delta)
#	if collision:
#		velocity = collision.remainder
#		velocity += collision.normal * speed
#	else:
#		velocity = Vector2()
	velocity = move_and_slide(velocity)
	
	if tilemap:
		var new_tile : Vector2 = tilemap.world_to_map(global_position)
		if new_tile != tilemapPosition:
			tilemapPosition = new_tile
#			tilemap.take_cell_tilemap(get_instance_id(), tilemapPosition)
			emit_signal("update_fog_of_war", ownerId, tilemapPosition, sensoryRadius())

func smoothPath(path : Array) -> Array:
	var smoothed_path : Array = []
	
	var space_state = get_world_2d().direct_space_state
	
	var query = Physics2DShapeQueryParameters.new()
	
	var collisionShape = $CollisionShape2D
	query.set_shape(collisionShape.shape) 
	query.collision_layer = collision_mask
	query.set_exclude([self])

	if not path.empty():
		var i : int = 0
		
		while i < path.size():
			var cycle_aborted : bool = false
			for j in range(i+2, path.size()):
				var motion = path[j] - path[i]
				query.set_motion(motion)
				query.set_transform(Transform2D(0.0, path[i]))
				
				var result = space_state.cast_motion(query)
				if result != [1.0, 1.0]:
					smoothed_path.push_back(path[j-1])
					i = j - 1
					cycle_aborted = true
					break
				
			if not cycle_aborted:
				smoothed_path.push_back(path[path.size() - 1])
				break

	return smoothed_path

func boundingRadius() -> float:
	if collisionShape.shape is CircleShape2D:
		return collisionShape.shape.radius
	elif collisionShape.shape is CapsuleShape2D:
		return max(collisionShape.shape.radius, collisionShape.shape.height/2)
	elif collisionShape.shape is RectangleShape2D:
		return max(collisionShape.shape.extents[0], collisionShape.shape.extents[1])
	else:
		return 1.0
		
func sensoryRadius() -> float:
	if not sensoryShape:
		return 0.0
		
	if sensoryShape.shape is CircleShape2D:
		return sensoryShape.shape.radius
	elif sensoryShape.shape is CapsuleShape2D:
		return max(sensoryShape.shape.radius, sensoryShape.shape.height/2)
	elif sensoryShape.shape is RectangleShape2D:
		return max(sensoryShape.shape.extents[0], sensoryShape.shape.extents[1])
	else:
		return 1.0

func test_motion(position : Vector2, motion : Vector2, rotation : float = 0.0) -> bool:
	var space_state = get_world_2d().direct_space_state
	
	var query = Physics2DShapeQueryParameters.new()
	
	query.set_shape(collisionShape.shape) 
	query.collision_layer = collision_mask
	query.set_exclude([self])
	query.set_motion(motion)
	query.set_transform(Transform2D(rotation, position))
				
	var result = space_state.cast_motion(query)
	return result == [1.0, 1.0]

func can_step_forward() -> bool:
	return test_motion(global_position, (Vector2(1, 0) * boundingRadius() * 2).rotated(global_rotation), global_rotation)
	
func can_step_left() -> bool:
	return test_motion(global_position, (Vector2(0, -1) * boundingRadius() * 2).rotated(global_rotation), global_rotation)

func can_step_right() -> bool:
	return test_motion(global_position, (Vector2(0, 1) * boundingRadius() * 2).rotated(global_rotation), global_rotation)

func can_step_back() -> bool:
	return test_motion(global_position, (Vector2(-1, 0) * boundingRadius() * 2).rotated(global_rotation), global_rotation)

func attack_action():
	var attackTarget = instance_from_id(attackTargetId)
	if attackTargetId < 0 or not attackTarget:
		state = States.IDLE
		return
	
	if not shooting_enabled:
		return
	
#	if global_position.distance_squared_to(attackTarget.global_position) > max_distance_for_attack * max_distance_for_attack:
#		var direction_from_enemy = (global_position - attackTarget.global_position).normalized()
#		move(attackTarget.global_position + direction_from_enemy * (max_distance_for_attack - 0.1 * max_distance_for_attack), States.ATTACK)
#	else:
	shooting_enabled = false
	shaking_enabled = true
	$ShakeTimer.start()
	$ShootSound.play()
	$CooldownTimer.start(shoot_cooldown)
	attackTarget.add_damage(damage, self)

func mine_ore(move_position : Vector2, tile_cell : Vector2):
	if not can_harvest:
		return
	ore_tile_position = tile_cell
	brain.AddGoal_HarvestOre(ore_tile_position)
	return
	move(move_position, States.HARVEST)
	
func no_ore_left():
	state = States.IDLE
	
func deliver_ore(position: Vector2):
	if not can_harvest:
		return
	move(position, States.DELIVER)
	
func no_refinery_left():
	state = States.IDLE
	
func harvest():
	emit_signal("request_closest_ore_tile_position", get_instance_id())
	
func harvest_action():
	pass
	
func deliver():
	emit_signal("request_closest_ore_refinery_position", get_instance_id())

func find_ore(successCB : FuncRef, failedCB : FuncRef):
	emit_signal("request_closest_ore_tile_position_cb", global_position, successCB, failedCB)

func find_refinery(successCB : FuncRef, failedCB : FuncRef):
	emit_signal("request_closest_ore_refinery_position_cb", global_position, ownerId, successCB, failedCB)


func move_unit(point : Vector2, selection : Array = []):
	neighbors = selection
	move(point, States.IDLE)
	
func escape_from_area(area_center : Vector2):
	print("escape_from_area (TODO: check is units mine) ", area_center)
	escape(area_center, state)

func escape_finished():
	print("escape_finished, fix bugs here")
	brain.RemoveGoal_Escape()
	state = plannedState
	
func escape(area_center : Vector2, nextState):
	escapeTarget = area_center
	brain.AddGoal_Escape()
	escape_p = true
	state = States.ESCAPE
	plannedState = nextState
	
func move(point, nextState):
	moveTarget = point
	brain.AddGoal_MoveToPosition(moveTarget)
	move_p = true
	state = States.MOVE
	plannedState = nextState
	
func move_away(nextState):
	originPoint = global_position
	away_p = true
	state = States.MOVE_AWAY
	plannedState = nextState
	
func return_back(nextState):
	away_p = true
	state = States.RETURN_TO_POS
	plannedState = nextState
	
func move_unit_away():
	move_away(States.RETURN_TO_POS)
	
func return_unit_back():
	return_back(States.IDLE)
	
func attack_enemy(enemy, selection : Array = []):
	if not can_attack:
		return

	neighbors = selection
	attack(enemy, States.IDLE)
	
func attack(enemy, nextState : int):
	if not enemy:
		return
		
	attackTargetId = enemy.get_instance_id()
	checkAttackTargetStatus()
	brain.AddGoal_AttackTarget()
	
	attack_p = true
	state = States.ATTACK
	plannedState = nextState

func add_damage(amount : float, attacker):
	attackedBy = attacker
	
	health -= amount
	
	if health <= 0.0:
		isDead = true
		emit_signal("unit_destroyed", get_instance_id())
		queue_free()
	
	if can_attack and (state == States.IDLE or state == States.MOVE) and attackedBy:
		$AttackResponseTimer.start(attack_response_time)

func GetPathToPosition(destination : Vector2) -> Array:
	if movableMaterial != ProductionRecord.MovableMaterial.AIR:
		path = navigation.get_simple_path(position, destination, false)
	else:
		path = [destination]
		
	return path
	
func isAtPosition(pos : Vector2) -> bool:
	var unitTile : Vector2 = tilemap.world_to_map(position)
	var posTile : Vector2 = tilemap.world_to_map(pos)
	return position.distance_squared_to(pos) < 64 and tilemap and (unitTile == posTile or (unitTile - posTile).length_squared() <= 2)
	
func _on_Unit_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == BUTTON_LEFT:
				if is_in_group("Enemies"):
					clicked_by_hostile()
				elif is_in_group("Units"):
					mouse_clicked()


func _on_Unit_mouse_entered():
	mouseOnUnit = true
	emit_signal("unit_mouse_enter", get_instance_id())
#	if is_in_group("Enemies"):
#		print("enemy mouse entered")
#	elif selected and expandable and expansion:
#		print("ready to expand")
#		emit_signal("expand_unit_ready", self)
#	else:
#		print("regular unit entered")


func _on_Unit_mouse_exited():
	mouseOnUnit = false
	emit_signal("unit_mouse_exit", get_instance_id())
#	if is_in_group("Enemies"):
#		print("enemy mouse exited")
#	elif selected and expandable and expansion:
#		print("cancel expand")
#		emit_signal("expand_unit_abort", self)
#	else:
#		print("regular unit exited")


func _on_CooldownTimer_timeout():
	shooting_enabled = true


func _on_ShakeTimer_timeout():
	shaking_enabled = false


func _on_AttackResponseTimer_timeout():
	if not attackedBy:
		return
	attack(attackedBy, state)

func _on_SensoryArea_body_entered(body):
#	neighbors.push_back(body)
	if not can_attack:
		return
		
	if state == States.IDLE:
		if body is get_script() or body is Building:
			if body.ownerId != ownerId: # TODO: there can be allies and neutrals
				attack_enemy(body)

func _on_SensoryArea_body_exited(body):
#	neighbors.erase(body)
	pass

func _on_viewport_entered(viewport : Viewport):
	emit_signal("become_visible", self, viewport)


func _on_viewport_exited(viewport : Viewport):
	emit_signal("become_unvisible", self, viewport)

func get_neighbors() -> Array:
	return flocking_neighbors
	
func get_flocking_neighbors() -> Array:
	return flocking_neighbors
	
func get_obstacles() -> Array:
	return obstacles

func _on_NeighborsArea_body_entered(body):
	if body != self and body is get_script() and body.ownerId == ownerId:
		flocking_neighbors.append(body)


func _on_NeighborsArea_body_exited(body):
	if body in flocking_neighbors:
		flocking_neighbors.erase(body)

func _on_ObstaclesAvoidanceArea_body_entered(body):
	if body != self and not body.get_instance_id() in obstacles:
		obstacles.append(body.get_instance_id())


func _on_ObstaclesAvoidance_body_exited(body):
	if body.get_instance_id() in obstacles:
		obstacles.erase(body.get_instance_id())

#----------------- CalculateExpectedTimeToReachPosition ----------------------
#
#  returns a value indicating the time in seconds it will take the bot
#  to reach the given position at its current speed.
#-----------------------------------------------------------------------------
func CalculateTimeToReachPosition(pos : Vector2, dt : float = 1.0) -> float:
	return max((global_position - pos).length() / (maxSpeed) * 2, Global.minTimeToReachPosition)


func _on_ObstaclesAvoidanceArea_body_exited(body):
	pass # Replace with function body.

func rotation_angle(velocity : Vector2) -> float:
	var dir : Vector2 = velocity.normalized()
	
	var dp : float = 0.0
	var closestDp : float = -1.0
	var closestDir : int = 0
	for dirVec in Global.DirectionVectors.keys():
		dp = dir.dot(Global.DirectionVectors[dirVec])
		if dp > closestDp:
			closestDp = dp
			closestDir = dirVec
			
	return Global.DirectionVectors[closestDir].angle()
			
#------------------------- hasLOSt0 ------------------------------------------
#
#  returns true if the bot has line of sight to the given position.
#-----------------------------------------------------------------------------
func hasLOSto(pos : Vector2, bot_agent = null) -> bool:
	var space_state : = get_world_2d().direct_space_state
	var exclude = [self]
	if bot_agent:
		exclude.push_back(bot_agent)
	var result = space_state.intersect_ray(global_position, pos, exclude)
	return result.empty() #m_pWorld->isLOSOkay(global_position, pos);

func hasLOStoBot(bot_agent) -> bool:
	var space_state : = get_world_2d().direct_space_state
	var intersection_info : = space_state.intersect_ray(global_position, bot_agent.global_position, [self, bot_agent])

	return intersection_info.empty()

func isTargetShootable() -> bool:
	if not can_attack:
		return false
		
	if attackTargetId == -1:
		return false
	
	var targetUnit = instance_from_id(attackTargetId)
	if not targetUnit:
		return false
	
	if movableMaterial != ProductionRecord.MovableMaterial.AIR and not hasLOStoBot(targetUnit):
		return false
		
	return global_position.distance_squared_to(targetUnit.global_position) < max_distance_for_attack * max_distance_for_attack
	
func checkAttackTargetStatus() -> void:
	isAttackSatisfied = isTargetShootable()
	
	if attackTimer:
		attackTimer.start()
