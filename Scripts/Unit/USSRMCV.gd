extends Unit

func _init():
	unitName = "USSRMCV"
	expansionName = "ConstructionYard"
	
	dependencyBuildings = [
		"USSROreRefinery",
		"USSRFactory",
		"USSRPowerPlant"
	]
