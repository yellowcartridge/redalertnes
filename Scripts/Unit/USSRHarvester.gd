extends Unit

func _init():
	can_harvest = true
	state = States.FIND_ORE
	unitName = "USSRHarvester"
	
	dependencyBuildings = [
		"USSROreRefinery",
		"USSRFactory"
	]
