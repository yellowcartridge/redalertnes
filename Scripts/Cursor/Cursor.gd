extends Node2D
class_name Cursor

onready var centeredSprite : AnimatedSprite = $CenteredSprite
onready var leftTopSprite : AnimatedSprite = $LeftTopSprite

func set_arrow():
	centeredSprite.visible = false
	leftTopSprite.visible = true
	leftTopSprite.play("arrow")
	
func set_attack():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("attack")
	
func set_move():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("move")
	
func set_expand():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("expand")

func set_select():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("select")
	
func set_collect():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("collect")
	
func set_sell():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("sell")
	
func set_repair():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("repair")
	
func set_left_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("left_arrow")
	
func set_right_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("right_arrow")
	
func set_up_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("up_arrow")
	
func set_down_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("down_arrow")

func set_up_left_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("up_left_arrow")
	
func set_up_right_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("up_right_arrow")
	
func set_down_left_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("down_left_arrow")
	
func set_down_right_arrow():
	centeredSprite.visible = true
	leftTopSprite.visible = false
	centeredSprite.play("down_right_arrow")

func hide():
	centeredSprite.visible = true
	leftTopSprite.visible = false

#func _process(delta):
#	print(global_position)
#	var fake_input = InputEventMouseMotion.new()
#	fake_input.position = global_position
#	#fake_input.button_index = BUTTON_LEFT
#	#fake_input.doubleclick = false
#	#fake_input.pressed = true
#	get_tree().input_event(fake_input)
