tool
extends Node

enum CollisionLayer {
	Env = 0,
	Units = 2,
	Buildings = 4,
	WaterUnits = 8,
	HybridUnits = 16,
	AirUnits = 32,
	GroundUnits = 64
}

enum Orientation {
	UP = 0,
	DOWN,
	LEFT,
	RIGHT,
	UP_RIGHT,
	DOWN_RIGHT,
	UP_LEFT,
	DOWN_LEFT
}

var DirectionVectors : Dictionary = {
	Orientation.UP: Vector2(0, -1),
	Orientation.DOWN: Vector2(0, 1),
	Orientation.LEFT: Vector2(-1, 0),
	Orientation.RIGHT: Vector2(1, 0),
	Orientation.UP_RIGHT: Vector2(1, -1).normalized(),
	Orientation.DOWN_RIGHT: Vector2(1, 1).normalized(),
	Orientation.UP_LEFT: Vector2(-1, -1).normalized(),
	Orientation.DOWN_LEFT: Vector2(-1, 1).normalized()
}

enum PlayerColor {
	BLUE,
	RED
}

enum PlayerFraction {
	AlliedForces,
	SovietUnion
}

var movableMaterials : Dictionary = {
	ProductionRecord.MovableMaterial.GROUND: ["ground_tile", "ore_tile"],
	ProductionRecord.MovableMaterial.WATER: ["water_tile"],
	ProductionRecord.MovableMaterial.HYBRID: ["ground_tile", "ore_tile", "water_tile"],
	ProductionRecord.MovableMaterial.AIR: [],
}

export (PlayerFraction) var firstPlayerFraction : int = PlayerFraction.AlliedForces
export (PlayerFraction) var secondPlayerFraction : int = PlayerFraction.SovietUnion

export (PlayerColor) var firstPlayerColor : int = PlayerColor.BLUE
export (PlayerColor) var secondPlayerColor : int = PlayerColor.RED

export (bool) var aiEnabled : bool = false
export (bool) var ShowDebugDrawInfo : bool = false

export (float) var minTimeToReachPosition : float = 1.0
