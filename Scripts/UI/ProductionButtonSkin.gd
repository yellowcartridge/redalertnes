extends Resource

export (String) var productionName = ""
export (Texture) var normal_icon = null
export (Texture) var pressed_icon = null
export (Texture) var hover_icon = null
export (Texture) var disabled_icon = null
export (Texture) var started_icon = null
export (Texture) var third_ready_icon = null
export (Texture) var two_third_ready_icon = null
export (Texture) var fully_ready_icon = null
