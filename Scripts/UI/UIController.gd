extends CanvasLayer

enum ProductionStates {
	BUILDINGS,
	UNITS
}

enum Fraction {
	ALLIED_FORCES,
	SOVIET_UNION
}

var productionButtonsSkins : Array = []
export (Array, Resource) var alliedButtonsSkins : Array = []
export (Array, Resource) var sovietButtonsSkins : Array = []

export (ProductionStates) var productionState : int = ProductionStates.UNITS

export (int) var prodButtonsColumns : int = 2
export (int) var prodButtonsWidth : int = 16
export (int) var prodButtonsHeight : int = 16

export (bool) var radar_is_hidden : bool = false setget set_radar_is_hidden

export (NodePath) var parentViewportPath : NodePath = NodePath()
export (NodePath) var productionManagerPath : NodePath = NodePath()
export (NodePath) var cursorPath : NodePath = NodePath()
export (NodePath) var playerControllerPath : NodePath = NodePath()
export (NodePath) var framesLayerPath : NodePath = NodePath()

export (PackedScene) var unitSelectionScene : PackedScene = null

export (String) var accept_action_command : String = ""
export (String) var decline_action_command : String = ""

export (Fraction) var fraction : int = Fraction.ALLIED_FORCES setget set_fraction

onready var parentViewport : Viewport = get_node(parentViewportPath)
onready var productionManager2 = get_node(productionManagerPath)
onready var cursor = get_node(cursorPath)
onready var playerController = get_node(playerControllerPath)

onready var resourceView : Control = $Panel/ResourceView
onready var moneyLabel : Label = $Panel/ResourceView/MoneyLaber
onready var radarView : Control = $Panel/RadarView
onready var radarControl : Control = $Panel/RadarView/Radar
onready var alliedLogo : NinePatchRect = $Panel/RadarView/Logos/AlliedLogo
onready var sovietLogo : NinePatchRect = $Panel/RadarView/Logos/SovietLogo
onready var floatingMenu : Control = $Panel/FloatingMenu/
onready var energyBarProduced : ColorRect = $Panel/FloatingMenu/EnergyBarProduced
onready var energyBarConsumedLE : ColorRect = $Panel/FloatingMenu/EnergyBarConsumedLE
onready var energyBarConsumed : ColorRect = $Panel/FloatingMenu/EnergyBarConsumed
onready var prodButtonsPanel : Control = $Panel/FloatingMenu/ProductionButtons
onready var buildingsProductionsButton : Control = $Panel/FloatingMenu/BuildingsProductionButton
onready var unitsProductionsButton : Control = $Panel/FloatingMenu/UnitsProductionButton
onready var sellButton : Control = $Panel/FloatingMenu/SellButton
onready var repairButton : Control = $Panel/FloatingMenu/RepairButton
onready var framesLayer : CanvasLayer = get_node(framesLayerPath)
onready var selectedUnitsLayer : Control = framesLayer.get_node("SelectedUnits")

onready var energyBar_width : int = energyBarProduced.rect_size.x
onready var energyBar_pos : Vector2 = energyBarProduced.rect_position

onready var floatingMenuPos : Vector2 = floatingMenu.rect_position

var buttons : Array = []

var buildingsButtons : Array = []
var unitsButtons : Array = []

var selectedUnits : Dictionary = {}
var prevEnergyStateIsLow : bool = false

const productionButtonScene : PackedScene = preload("res://Scenes/Buttons/ProductionButton.tscn")

func _ready():
	buildingsProductionsButton.accept_action_command = accept_action_command
	buildingsProductionsButton.decline_action_command = decline_action_command
	unitsProductionsButton.accept_action_command = accept_action_command
	unitsProductionsButton.decline_action_command = decline_action_command
	sellButton.decline_action_command = decline_action_command
	sellButton.accept_action_command = accept_action_command
	repairButton.decline_action_command = decline_action_command
	repairButton.accept_action_command = accept_action_command
	
	productionManager2.connect("available_productions_updated", self, "_on_available_productions_updated")
	productionManager2.connect("productions_progresses_changed", self, "_on_productions_progresses_changed")
	productionManager2.connect("money_changed", self, "_on_money_changed")
	productionManager2.connect("energy_changed", self, "_on_energy_changed")
	
	playerController.connect("selected_unit_become_visible", self, "selected_unit_become_visible")
	playerController.connect("selected_unit_become_unvisible", self, "selected_unit_become_unvisible")
	playerController.connect("fraction_changed", self, "set_fraction")
	playerController.connect("end_game", self, "end_game")
	
	set_fraction(playerController.fraction)
	update_radar_visibility()
	update_radar_logo()
	update_production_buttons_skins()
	
	updateMoney(productionManager2.money)
	prevEnergyStateIsLow = productionManager2.lowEnergyState
	updateEnergy(productionManager2.energyConsumed, productionManager2.energyProduced, productionManager2.lowEnergyState)
	
	productionManager2.update_available_productions()
	_on_BuildingsProductionButton_pressed()
	
func _on_available_productions_updated(productions : Array) -> void:
	destroy_production_buttons()
	create_production_buttons(productions)
	update_panel_tab()
	
func _on_productions_progresses_changed(progresses : Dictionary) -> void:
	for button in buildingsButtons:
		if progresses.has(button.name):
			button.progress = progresses[button.name]
			
	for button in unitsButtons:
		if progresses.has(button.name):
			button.progress = progresses[button.name]
	
func _on_money_changed(amount : int) -> void:
	updateMoney(amount)
	
func _on_energy_changed(amountConsumed : int, amountProduced : int, lowEnergyState : bool) -> void:
	updateEnergy(amountConsumed, amountProduced, lowEnergyState)
	
func updateMoney(value : int) -> void:
	moneyLabel.text = "$" + String(value)
	
func updateEnergy(amountConsumed : int, amountProduced : int, lowEnergyState : bool) -> void:
	if prevEnergyStateIsLow != lowEnergyState:
		prevEnergyStateIsLow = lowEnergyState
		energyStateChanged(lowEnergyState)
		
	if amountProduced == 0:
		energyBarProduced.visible = false
		
		if amountConsumed == 0:
			energyBarConsumed.visible = false
			energyBarConsumedLE.visible = false
			return

	var ratio : float = float(amountConsumed) / float(amountProduced) if amountProduced > 0 else 0.0
	if ratio <= 1.0 and amountProduced > 0:
		energyBarConsumedLE.visible = false
		
		energyBarProduced.visible = true
		energyBarProduced.rect_position = energyBar_pos
		energyBarProduced.rect_size = Vector2(energyBar_width, floatingMenu.rect_size.y)
		
		energyBarConsumed.visible = true
		var bar_height : int = floatingMenu.rect_size.y * ratio
		energyBarConsumed.rect_position = energyBar_pos + Vector2(0, floatingMenu.rect_size.y - bar_height)
		energyBarConsumed.rect_size = Vector2(energyBar_width, bar_height)
	else:
		energyBarProduced.visible = false
		energyBarConsumed.visible = false
		
		if amountConsumed > 0:
			energyBarConsumedLE.visible = true
			energyBarConsumedLE.rect_position = energyBar_pos
			energyBarConsumedLE.rect_size = Vector2(energyBar_width, floatingMenu.rect_size.y)
			
			energyBarProduced.visible = true
			var le_ratio : float = float(amountProduced) / float(amountConsumed)
			var bar_height : int = floatingMenu.rect_size.y * le_ratio
			energyBarProduced.rect_position = energyBar_pos + Vector2(0, floatingMenu.rect_size.y - bar_height)
			energyBarProduced.rect_size = Vector2(energyBar_width, bar_height)
		else:
			energyBarConsumed.visible = false
			energyBarConsumedLE.visible = false
			
			if amountProduced > 0:
				energyBarProduced.visible = true
				energyBarProduced.rect_position = energyBar_pos
				energyBarProduced.rect_size = Vector2(energyBar_width, floatingMenu.rect_size.y)
			else:
				energyBarProduced.visible = false


func _on_Panel_mouse_entered():
	print("_on_Panel_mouse_entered")


func _on_Panel_mouse_exited():
	print("_on_Panel_mouse_exited")


func _on_BuildingsProductionButton_pressed():
	buildingsProductionsButton.set_disabled(true)
	unitsProductionsButton.set_disabled(false)
	productionState = ProductionStates.BUILDINGS
	#updateButtons(productionManager.productionButtons)
	update_panel_tab()

func _on_UnitsProductionButton_pressed():
	buildingsProductionsButton.set_disabled(false)
	unitsProductionsButton.set_disabled(true)
	productionState = ProductionStates.UNITS
	#updateButtons(productionManager.productionButtons)
	update_panel_tab()

func update_panel_tab() -> void:
	remove_production_buttons_from_panel()
	
	if productionState == ProductionStates.BUILDINGS:
		add_buildings_buttons_to_panel()
	else:
		add_units_buttons_to_panel()

func create_production_buttons(buttons: Array) -> void:
	var buttonNum : int = 0
	var buildingsCount : int = 0
	var unitsCount : int = 0
	for buttonInfo in buttons:
		var skin : Resource = get_production_button_skin(buttonInfo.name)
		
		if not skin:
			continue
			
		var but = productionButtonScene.instance()
		but.set_skin(skin)
		but.connect_me(productionManager2, buttonInfo.name)
		
		if buttonInfo.type == "building":
			buttonNum = buildingsCount
			buildingsButtons.append(but)
			buildingsCount += 1
		else:
			buttonNum = unitsCount
			unitsButtons.append(but)
			unitsCount += 1
			
		but.rect_position = Vector2((buttonNum % prodButtonsColumns) * prodButtonsWidth, (buttonNum / prodButtonsColumns) * prodButtonsHeight)
		
		but.set_disabled(not buttonInfo.available and not buttonInfo.awaits)
		
		if buttonInfo.awaits:
			but.toggle_take()
		else:
			but.toggle_produce()
			
		but.accept_action_command = accept_action_command
		but.decline_action_command = decline_action_command

func destroy_production_buttons() -> void:
	remove_production_buttons_from_panel()
	
	for building in buildingsButtons:
		building.queue_free()
	buildingsButtons.clear()
	
	for unit in unitsButtons:
		unit.queue_free()
	unitsButtons.clear()

func remove_production_buttons_from_panel() -> void:
	var buttons : Array = prodButtonsPanel.get_children()
	for button in buttons:
		prodButtonsPanel.remove_child(button)

func add_buildings_buttons_to_panel() -> void:
	for button in buildingsButtons:
		prodButtonsPanel.add_child(button)
	
func add_units_buttons_to_panel() -> void:
	for button in unitsButtons:
		prodButtonsPanel.add_child(button)

func get_production_button_skin(productionName : String) -> Resource:
	for prod in productionButtonsSkins:
		if prod.productionName == productionName:
			return prod
	return null

func _process(delta):
	if cursor:
		buildingsProductionsButton.pointedByMouse(cursor.position)
		unitsProductionsButton.pointedByMouse(cursor.position)
		test_production_buttons_mouse(cursor.position)

func test_production_buttons_mouse(position : Vector2) -> void:
	if productionState == ProductionStates.BUILDINGS:
		for building in buildingsButtons:
			building.pointedByMouse(position)
	else:
		for unit in unitsButtons:
			unit.pointedByMouse(position)

func selected_unit_become_visible(unit : Unit) -> void:
	if selectedUnits.has(unit.get_instance_id()):
		return
	
	var selection = unitSelectionScene.instance()
	selection.Init(unit, playerController)
	selectedUnitsLayer.add_child(selection)
	selectedUnits[unit.get_instance_id()] = selection
	
func selected_unit_become_unvisible(unit : Unit) -> void:
	if selectedUnits.has(unit.get_instance_id()):
		var selection = selectedUnits[unit.get_instance_id()]
		selectedUnitsLayer.remove_child(selection)
		selection.queue_free()
		selectedUnits.erase(unit.get_instance_id())

func set_radar_is_hidden(value : bool) -> void:
	if value != radar_is_hidden:
		radar_is_hidden = value
		update_radar_visibility()

func update_radar_visibility() -> void:
	if not radarView or not floatingMenu:
		return

	if radar_is_hidden:
		radarView.visible = false
		floatingMenu.rect_position = radarView.rect_position
		floatingMenu.rect_size = Vector2(parentViewport.size.x, parentViewport.size.y - resourceView.rect_size.y)
	else:
		floatingMenu.rect_position = floatingMenuPos
		floatingMenu.rect_size = Vector2(parentViewport.size.x, parentViewport.size.y - resourceView.rect_size.y - radarView.rect_size.y)
		radarView.visible = true
	
func update_radar_logo() -> void:
	if not alliedLogo or not sovietLogo:
		return
		
	if fraction == Fraction.ALLIED_FORCES:
		alliedLogo.visible = true
		sovietLogo.visible = false
	else:
		alliedLogo.visible = false
		sovietLogo.visible = true
		
	radarControl.visible = false
		
func update_production_buttons_skins() -> void:
	if fraction == Global.PlayerFraction.AlliedForces:
		productionButtonsSkins = alliedButtonsSkins
	else:
		productionButtonsSkins = sovietButtonsSkins

func set_fraction(value : int) -> void:
	if fraction != value:
		fraction = value
		update_radar_logo()
		update_production_buttons_skins()

func end_game(isWinner : bool) -> void:
	$EndGamePopup/VictoryLabel.visible = isWinner
	$EndGamePopup/LoseLabel.visible = !isWinner
	$EndGamePopup.visible = true


func _on_Viewport_size_changed():
	pass # Replace with function body.

func energyStateChanged(lowEnergyState : bool) -> void:
	if lowEnergyState:
		# play low energy sound
		pass
	else:
		#play high energy sound
		pass
