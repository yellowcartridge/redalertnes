extends UIButton

enum State {
	PRODUCE,
	TAKE
}

export (Resource) var skin : Resource = null setget set_skin
export (float) var progress : float = 1.0 setget set_progress
export (State) var productionState : int = State.PRODUCE

func _ready():
	update_textures()
		

func _pressed():
	emit_signal("accept_action")

func connect_me(obj, button_name):
	connect("accept_action", obj, "button_accept_action", [button_name])
	connect("decline_action", obj, "button_decline_action", [button_name])
	name = button_name

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.pressed:
			emit_signal("decline_action")

func set_progress(amount : float) -> void:
	if progress != amount:
		progress = amount
	
		if not skin:
			return

		if progress < 0.33:
			set_texture_disabled(skin.started_icon)
		elif progress < 0.66:
			set_texture_disabled(skin.third_ready_icon)
		elif progress < 1.0:
			set_texture_disabled(skin.two_third_ready_icon)
		else:
			set_texture_disabled(skin.fully_ready_icon)
			
func toggle_produce():
	productionState = State.PRODUCE
	update_textures()
	
func toggle_take():
	productionState = State.TAKE
	update_textures()

func update_textures() -> void:
	if not skin:
		return
		
	set_texture_hover(skin.hover_icon)
	set_texture_pressed(skin.pressed_icon)
		
	match productionState:
		State.PRODUCE:
			set_texture_normal(skin.normal_icon)
		State.TAKE:
			set_texture_normal(skin.fully_ready_icon)
		_:
			set_texture_normal(skin.normal_icon)
			
	set_texture_disabled(skin.disabled_icon)
	update_state()

func set_skin(res : Resource) -> void:
	if skin != res:
		skin = res
		update_textures()
