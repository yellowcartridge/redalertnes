extends Node2D

export (PackedScene) var EscapeAreaScene : PackedScene = preload("res://Scenes/Areas/EscapeArea/EscapeArea.tscn")

export (NodePath) var worldPath : NodePath = NodePath()

export (Global.PlayerColor) var color : int = Global.PlayerColor.RED
export (Global.PlayerFraction) var fraction : int = Global.PlayerFraction.SovietUnion setget set_fraction
export (bool) var nesUnitsLimitEnabled : bool = true

export (float) var maxBuildDistance : float = 100

export (NodePath) var tilemapPath : NodePath = ""
export (String) var goodBuildingPlaceTileName : String = ""
export (String) var badBuildingPlaceTileName : String = ""
export (String) var groundTileName : String = ""
export (String) var oreTileName : String = ""

export (Array) var buildingsQuery : Array = []

export (Array) var unitsQuery : Array = [
	{"unitName": "Soldier", "count": 5},
	{"unitName": "Tank", "count": 2},
	{"unitName": "Soldier", "count": 5},
	{"unitName": "Tank", "count": 3}
]

export (int) var money : int = 5000
var energy : int = 0

export (int) var playerId : int = 2
export (int) var enemyId : int = 1

onready var productionManager2 := $ProductionManager2
onready var tilemap : TileMap = get_node(tilemapPath)

var selectedTiles : Array = []
var buildingCanBePlaced : bool = false
var tileToPlaceBuilding : Vector2 = Vector2()
var probablyPosition : Vector2 = Vector2()
var productionToPlace : String = ""
var buildingToPlace : Building = null
var buildingAreaShouldBeCleared : bool = false

var unitCanBePlaced : bool = false
var tileToPlaceUnit : Vector2 = Vector2()

var unitsProcessing = {}
var attackingUnits = []
var attackInProgress : bool = false
const INVALID_ID = -1
var attackingTargetId : int = INVALID_ID
var attackLeader : int = INVALID_ID

var mcvsProcessing = {}

onready var gameController = get_node(worldPath)
onready var attackTimer : Timer = $AttackTimer

var productionsWaitForSatisfying : Array = []

var buildings : Array = []
var harvesters : Array = []
var refineries : Array = []

signal request_add_building_to_world
signal request_add_unit_to_world
signal request_expand_unit

signal fraction_changed

var attack_strength_level : int = 0
var readyForAttack : bool = false
var unitsForAttack : Array = []

var attack_waves = [
	{"Soldier": 3, "Tank": 0},
	{"Soldier": 5, "Tank": 0},
	{"Soldier": 7, "Tank": 2},
	{"Soldier": 10, "Tank": 3}
]

func _ready():
	productionManager2.connect("placeBuilding", self, "_on_placeBuilding")
	productionManager2.connect("placeUnit", self, "_on_placeUnit")
	productionManager2.connect("money_changed", self, "_on_money_changed")
	productionManager2.connect("energy_changed", self, "_on_energy_changed")
	productionManager2.connect("production_ready", self, "_on_production_finished")
	
	connect("request_add_building_to_world", gameController, "add_building_to_world")
	connect("request_add_unit_to_world", gameController, "add_unit_to_world")
	connect("request_expand_unit", gameController, "expand_object")
	
	gameController.connect("gain_for_player", self, "gain_for_player")
	gameController.connect("unit_destroyed", self, "unit_destroyed")
	gameController.connect("building_destroyed", self, "building_destroyed")
	gameController.connect("end_game", self, "end_game")
	
	attackTimer.connect("timeout", self, "_on_AttackTimer_timeout")
	
	randomize()
	update_fraction()

func _process(delta):
	buildings = gameController.get_buildings_of_player(playerId)
	refineries = gameController.get_refineries_of_player(playerId)
	harvesters = gameController.get_harvesters_of_player(playerId)
	
	processMCVs()
	processRefinery()
	
	planAttack()
	processUnits()
	processBuildingsQuery()
	processUnitsQuery()

func _on_AttackTimer_timeout() -> void:
	attackInProgress = false

func processMCVs():
	var mcvs = gameController.get_mcvs_of_player(playerId)
	
	if mcvs.empty():
		return
		
	var basePoints = get_tree().get_nodes_in_group("CPUMCVPosition")
	
	if basePoints.empty():
		return
	
	var basePointsIds: Array = []
	for bp in basePoints:
		basePointsIds.push_back(bp.get_instance_id())
	
	for mcv_id in mcvs:
		var mcv = instance_from_id(mcv_id)
		if mcvsProcessing.has(mcv_id):
			if mcvsProcessing.get(mcv_id).distance_squared_to(mcv.global_position) <= 100:
				mcvsProcessing.erase(mcv_id)
				
				emit_signal("request_expand_unit", mcv, playerId, color, productionManager2)
				
		else:
			var pointId = find_closest_object(basePointsIds, mcv.global_position)
			var point = instance_from_id(pointId)
			if not point:
				continue
			mcvsProcessing[mcv_id] = point.global_position
			mcv.move_unit(point.global_position)

func processRefinery() -> void:
	if refineries.empty() and not "OreRefinery" in productionsWaitForSatisfying and not "OreRefinery" in buildingsQuery:
		satisfy_depencies("OreRefinery")
		makeProduction("OreRefinery")
	processHarvesters()

func processHarvesters():
	if not refineries.empty() and ((nesUnitsLimitEnabled and harvesters.empty()) or (not nesUnitsLimitEnabled and refineries.size() > harvesters.size())) and not "Harvester" in productionsWaitForSatisfying and not "Harvester" in buildingsQuery:
		satisfy_depencies("Harvester")
		makeProduction("Harvester")
		return
		
	for harvesterId in harvesters:
		var harvester = instance_from_id(harvesterId)
		if not harvester:
			continue
		if harvester.state == Unit.States.IDLE:
			if harvester.has_ore:
				harvester.state = Unit.States.FIND_REFINERY
			else:
				harvester.state = Unit.States.FIND_ORE

func planAttack() -> void:
	if attack_waves.empty() or attackInProgress or readyForAttack:
		return

	var allMyUnits = gameController.get_units_of_player(playerId)
	
	var wave_units = attack_waves[attack_strength_level].duplicate()
	
	var plan_satisfied : bool = false
	
	unitsForAttack = []
	
	for unitId in allMyUnits:
		var unit = instance_from_id(unitId)
		
		if not unit:
			continue

		if wave_units.has(unit.unitName) and wave_units[unit.unitName] > 0:
			wave_units[unit.unitName] -= 1
			unitsForAttack.push_back(unit)
		if wave_units.has(unit.unitName) and wave_units[unit.unitName] == 0:
			wave_units.erase(unit.unitName)
		if wave_units.empty():
			plan_satisfied = true
			break
	
	var unitsLeft : int = 0
	for unit in wave_units.keys():
		unitsLeft += wave_units[unit]
		
	if not unitsLeft:
		readyForAttack = true
		attack_strength_level = min(attack_strength_level+1, attack_waves.size()-1)

func processUnits():
	var allMyUnits = gameController.get_units_of_player(playerId)
	
	if attackInProgress:
		for unit_id in attackingUnits:
			if not instance_from_id(unit_id):
				attackingUnits.erase(unit_id)
				
		if attackingUnits.empty():
			attackInProgress = false
			attackTimer.stop()
			attackingTargetId = INVALID_ID
			attackingUnits.clear()
			
#		if attackInProgress and not instance_from_id(attackLeader):
#			attackLeader = attackingUnits[0]
			
		if not instance_from_id(attackingTargetId):
			readyForAttack = true
			attackInProgress = false
			attackTimer.stop()
			
	if not attackInProgress:
		if readyForAttack:
			var allPlayersBuildings = gameController.get_buildings_of_player(enemyId)
			var allPlayersUnits = gameController.get_units_of_player(enemyId)
			
			var target = null
			
#			var leaderUnit = INVALID_ID
#			var leader = null
#			while not unitsForAttack.empty() and not leader:
#				leader = unitsForAttack[0]
#
#				if not leader:
#					unitsForAttack.pop_front()
#					continue
#
#				leaderUnit = leader.get_instance_id()
			if unitsForAttack.empty():
				attackInProgress = false
				attackTimer.stop()
				return
				
			var leader = unitsForAttack.front()
			
			if not leader:
				return
				
#			attackLeader = leaderUnit

			if not allPlayersBuildings.empty():
				attackingTargetId = find_closest_object(allPlayersBuildings, leader.global_position)
			elif not allPlayersUnits.empty():
				attackingTargetId = find_closest_object(allPlayersUnits, leader.global_position)
			
			if attackingTargetId == INVALID_ID:
				return 
				
			target = instance_from_id(attackingTargetId)
			if not target:
				return
			
			var maxTimeToReachPosition : float = 0.0
			attackingUnits = []
			for unit in unitsForAttack:
				attackingUnits.push_back(unit.get_instance_id())
				unit.attack_enemy(target, attackingUnits)
				var timeToReachPos = unit.CalculateTimeToReachPosition(target.global_position)
				if timeToReachPos > maxTimeToReachPosition:
					maxTimeToReachPosition = timeToReachPos
					
			attackTimer.start(maxTimeToReachPosition * 3)
			
			unitsForAttack.clear()
			attackInProgress = true
			readyForAttack = false

func find_closest_object(objects: Array, position : Vector2) -> int:
	var MaxDistSqr: float = SteeringBehavior.MaxFloat
	var closestObjectId = INVALID_ID
	for objectId in objects:
		var object = instance_from_id(objectId)
		if not object:
			continue
		var dist = position.distance_squared_to(object.global_position)
		if dist < MaxDistSqr:
			MaxDistSqr = dist
			closestObjectId = objectId
			
	return closestObjectId

func processBuildingsQuery():
	if buildingsQuery.empty(): # or productionManager.is_building_in_progress():
		return
	
	var topBuilding = buildingsQuery[0]

	var productionParams : Resource = productionManager2.get_production_params(topBuilding)
	if not productionParams:
		return
	
	if not productionManager2.is_depencies_satisfied(productionParams):
		satisfy_depencies(topBuilding)

		#if not productionManager2.is_production_limit_reached(productionParams):
		#	buildingsQuery.push_back(topBuilding)
	elif productionManager2.is_production_limit_reached(productionParams) or productionManager2.produce(topBuilding):
		buildingsQuery.pop_front()
#	else:
#		buildingsQuery.pop_front()
#
#		if not productionManager2.is_production_limit_reached(productionParams):
#			buildingsQuery.push_back(topBuilding)

func makeProduction(productionName : String) -> void:
	buildingsQuery.push_back(productionName)
		
func _on_production_finished(productionName : String) -> void:
	buildingCanBePlaced = true
	productionToPlace = productionName
	
	var production = productionManager2.get_production_instance(productionToPlace)
	if production is Building:
		place_building(production)				
	else:
		place_unit(production)
		
	var satisfied_productions : Array = []
	for productionName in productionsWaitForSatisfying:
		var productionParams : Resource = productionManager2.get_production_params(productionName)
		if not productionParams:
			continue
		if productionManager2.is_depencies_satisfied(productionParams):
			satisfied_productions.push_back(productionName)
			
	for productionName in satisfied_productions:
		productionsWaitForSatisfying.erase(productionName)

onready var unitsQueryIndex : int = 0
func processUnitsQuery():
	if unitsQuery.empty() or not buildingsQuery.empty(): # or productionManager.is_building_in_progress():
		return
	
	var topUnit = unitsQuery[unitsQueryIndex]
	unitsQueryIndex = (unitsQueryIndex + 1) % unitsQuery.size()
	
	var productionParams : Resource = productionManager2.get_production_params(topUnit.unitName)
	if not productionParams:
		return
	if not productionManager2.is_production_limit_reached(productionParams):
		if not topUnit.unitName in productionsWaitForSatisfying and not topUnit.unitName in buildingsQuery:
			satisfy_depencies(topUnit.unitName)
			makeProduction(topUnit.unitName)

func _on_placeBuilding(_building):
	pass

func _on_placeUnit(_building):
	pass
	
func _on_money_changed(amount : int) -> void:
	updateMoney(amount)

func _on_energy_changed(amountConsumed : int, amountProduced : int, lowEnergyState : bool) -> void:
	updateEnergy(amountConsumed, amountProduced)
	
func _on_productionButtons_changed(productionButtons : Array) -> void:
	pass
	
func updateMoney(value : int) -> void:
	money = value
	
func updateEnergy(amountConsumed : int, amountProduced : int) -> void:
	energy = amountProduced

func place_building(building : Building) -> bool:
	if find_building_place_position(building):
		buildingToPlace = building
		if buildingAreaShouldBeCleared:
			clear_area()
		else:
			area_cleared()
		return true
	
	return false
	
func clear_area():
	var escapeArea = EscapeAreaScene.instance()
	escapeArea.Init(buildingToPlace.get_tiles(), tilemap.cell_size)
	escapeArea.set_global_position(tilemap.map_to_world(tileToPlaceBuilding))
	escapeArea.connect("area_cleared", self, "area_cleared")
	gameController.add_child(escapeArea)
	
func area_cleared():
	emit_signal("request_add_building_to_world", buildingToPlace, productionToPlace, tileToPlaceBuilding, playerId, color, productionManager2)

func init_production(position : Vector2, production) -> void:
	production.global_position = position
	production.ownerId = playerId
	production.color = color

func place_unit(unit : Unit):
	if find_unit_place_position(unit):
		emit_signal("request_add_unit_to_world", unit, productionToPlace, tileToPlaceUnit, playerId, color, productionManager2)
		return true
	
	return false

func calcel_place_building():
	clear_building_placement()
	
func clear_building_placement():
	if not selectedTiles.empty():
		for cell in selectedTiles:
			tilemap.set_cell(cell.x, cell.y, cell.tile)
	selectedTiles.clear()
	
func draw_building_placement(building : Building):	
	clear_building_placement()
			
	var good_building_place_tile_index = tilemap.tile_set.find_tile_by_name(goodBuildingPlaceTileName)
	var bad_building_place_tile_index = tilemap.tile_set.find_tile_by_name(badBuildingPlaceTileName)
	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
	
	var main_tile_pos = tilemap.world_to_map(probablyPosition)
	
	tileToPlaceBuilding = main_tile_pos
	
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(tilemap.cell_size / 2 - Vector2(1, 1))
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_shape(shape)
	
	var allCellsSucceded:bool = true
	for cell in building.get_tiles():
		var cell_pos = Vector2(main_tile_pos.x + cell.x, main_tile_pos.y + cell.y)
		var cellInfo : Dictionary = {}
		cellInfo.x = cell_pos.x
		cellInfo.y = cell_pos.y
		cellInfo.tile = tilemap.get_cell(cell_pos.x, cell_pos.y)
		
		if not selectedTiles.has(cellInfo):
			selectedTiles.push_back(cellInfo)
			
			var tile_position = tilemap.map_to_world(cell_pos) + (tilemap.cell_size / 2)

			query.set_transform(Transform2D(0.0, tile_position))
			var res : Array = space_state.intersect_shape(query) 
			
			if res.empty() and tilemap.get_cell(cell_pos.x, cell_pos.y) == ground_tile_index:
				tilemap.set_cell(cell_pos.x, cell_pos.y, good_building_place_tile_index)
			else:
				tilemap.set_cell(cell_pos.x, cell_pos.y, bad_building_place_tile_index)
				allCellsSucceded = false
	
	buildingCanBePlaced = allCellsSucceded
	
func clear_unit_placement():
	if not selectedTiles.empty():
		for cell in selectedTiles:
			tilemap.set_cell(cell.x, cell.y, cell.tile)
	selectedTiles.clear()

func get_production_depencies_query(productionName : String) -> Array:
	var dependencies : Array = []
	var query : Array = []
	
	var production : Resource = productionManager2.get_production_params(productionName)
	dependencies += production.dependencies
	
	while not dependencies.empty():
		var dependency : String = dependencies.pop_front()
		
		if dependency in dependencies or dependency in buildingsQuery or dependency in productionsWaitForSatisfying:
			continue
		production = productionManager2.get_production_params(dependency)
		if production:
			if not productionManager2.is_depencies_satisfied(production):
				productionsWaitForSatisfying.push_back(dependency)
				dependencies += production.dependencies
			if not productionManager2.is_production_exists(dependency):
				query.push_front(dependency)
	
	return query
	
func satisfy_depencies(productionName : String) -> void:
	if productionName in productionsWaitForSatisfying:
		return
	
	var production : Resource = productionManager2.get_production_params(productionName)
	if production and not productionManager2.is_depencies_satisfied(production):
		productionsWaitForSatisfying.push_back(productionName)
		buildingsQuery += get_production_depencies_query(productionName)

func is_unit_can_be_placed_at_position(unit : Unit, tile_coord : Vector2, ground_tile_index : int , ore_tile_index : int) -> bool:
	var cell : int = tilemap.get_cell(tile_coord.x, tile_coord.y)
	if not (cell == ground_tile_index) and not (cell == ore_tile_index):
		return false
		
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(tilemap.cell_size / 2 - Vector2(1, 1))
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_shape(shape)
	var tile_position = tilemap.map_to_world(tile_coord) + (tilemap.cell_size / 2)
	query.set_transform(Transform2D(0.0, tile_position))
	var res : Array = space_state.intersect_shape(query) 
	return res.empty()

func is_building_can_be_placed_at_position(building : Building, tile_coord : Vector2, ground_tile_index : int , ore_tile_index : int) -> bool:
	var border_tiles : Array = []
	var building_tiles : Array = []
	
	buildingAreaShouldBeCleared = false
	
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(tilemap.cell_size / 2 - Vector2(1, 1))
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_shape(shape)

	for cell in building.get_tiles():
		var cell_pos = Vector2(tile_coord.x + cell.x, tile_coord.y + cell.y)
		var cellInfo : Dictionary = {}
		cellInfo.x = cell_pos.x
		cellInfo.y = cell_pos.y
		cellInfo.tile = tilemap.get_cell(cell_pos.x, cell_pos.y)
		
		var tile_position : Vector2 = tilemap.map_to_world(cell_pos) + (tilemap.cell_size / 2)

		if cell_pos in border_tiles:
			border_tiles.erase(cell_pos)
		

		if tilemap.get_cell(cell_pos.x, cell_pos.y) == ground_tile_index:
			building_tiles.push_back(cell_pos)
			
			var neighbours : Array = get_neighbour_tiles(cell_pos)
			for neigbour_tile in neighbours:
				if not (neigbour_tile in border_tiles) and not (neigbour_tile in building_tiles):
					border_tiles.push_back(neigbour_tile)
					
			query.set_transform(Transform2D(0.0, tile_position))
			var res : Array = space_state.intersect_shape(query) 
			
			if not res.empty():
				for collision in res:
					if collision.collider is Unit and collision.collider.ownerId == playerId:
						buildingAreaShouldBeCleared = true
					else:
						return false
		else:
			return false

	for border_tile in border_tiles:
		var cell : int = tilemap.get_cell(border_tile.x, border_tile.y)
		if not (cell == ground_tile_index) and not (cell == ore_tile_index):
			return false
			
	return true
	
func get_tiles_in_region(center : Vector2, radius : float) -> Array:
	var tiles : Array = []
	var tile_pos = tilemap.world_to_map(center)
	var square_side : float = radius * sqrt(2)
	
	var horizontal_tiles_count = square_side / tilemap.cell_size.x
	var vertical_tiles_count = square_side / tilemap.cell_size.y
	
	for i in range(tile_pos.x - horizontal_tiles_count / 2, tile_pos.x + horizontal_tiles_count / 2):
		for j in range(tile_pos.y - vertical_tiles_count / 2, tile_pos.y + vertical_tiles_count / 2):
			tiles.push_back(Vector2(i, j))
			
	return tiles
	
func get_neighbour_tiles(tile : Vector2) -> Array:
	return [
		tile + Vector2(-1, 0),
		tile + Vector2(-1, -1),
		tile + Vector2(0, -1),
		tile + Vector2(1, -1),
		tile + Vector2(1, 0),
		tile + Vector2(1, 1),
		tile + Vector2(0, 1),
		tile + Vector2(-1, 1)
	]

func find_building_place_position(building : Building) -> bool:
	var ground_tile_index : int = tilemap.tile_set.find_tile_by_name(groundTileName)
	var ore_tile_index : int = tilemap.tile_set.find_tile_by_name(oreTileName)
	
	var buildingsCount : int = buildings.size()
	if not buildingsCount:
		return false

	var buildingIndex : int = randi() % buildingsCount
	for _iteration in range(0, buildingsCount):
		var existingBuilding : Building = instance_from_id(buildings[buildingIndex]);
		if not existingBuilding:
			buildingIndex = (buildingIndex + 1) % buildingsCount
			continue
			
		var tiles : Array = get_tiles_in_region(existingBuilding.global_position, maxBuildDistance)
		
		var tileIndex : int = randi() % tiles.size()
		for _i in range(0, tiles.size()):
			var tile = tiles[tileIndex]
			tileIndex = (tileIndex + 1) % tiles.size()
			if is_building_can_be_placed_at_position(building, tile, ground_tile_index, ore_tile_index):
				tileToPlaceBuilding = tile
				return true
		
		buildingIndex = (buildingIndex + 1) % buildingsCount
				
	return false

func find_unit_place_position(unit : Unit) -> bool:
	var ground_tile_index : int = tilemap.tile_set.find_tile_by_name(groundTileName)
	var ore_tile_index : int = tilemap.tile_set.find_tile_by_name(oreTileName)
	
	# TODO: replace ref to building in buildings array to instance_id	
	var buildingsCount : int = buildings.size()
	if not buildingsCount:
		return false
		
	var buildingIndex : int = randi() % buildingsCount

	for _iteration in range(0, buildingsCount):
		var existingBuilding : Building = instance_from_id(buildings[buildingIndex]);
		if not existingBuilding:
			buildingIndex = (buildingIndex + 1) % buildingsCount
			continue
		var tiles : Array = get_tiles_in_region(existingBuilding.global_position, maxBuildDistance)
		
		for tile in tiles:
			if is_unit_can_be_placed_at_position(unit, tile, ground_tile_index, ore_tile_index):
				tileToPlaceUnit = tile
				return true
		
		buildingIndex = (buildingIndex + 1) % buildingsCount
				
	return false

func unit_destroyed(unit_id) -> void:
	var unit : Unit = instance_from_id(unit_id)
	if unit and unit.ownerId == playerId:
		productionManager2.productionDestroyed(unit.unitName)
		
func building_destroyed(building_id, _position: Vector2, _tiles : Array) -> void:
	var building : Building = instance_from_id(building_id)
	if building and building.ownerId == playerId:
		productionManager2.productionDestroyed(building.buildingName)
	
func end_game(_isWinner : bool) -> void:
	set_process(false)

func set_fraction(value : int) -> void:
	if fraction != value:
		fraction = value
		
		update_fraction()
		
func update_fraction() -> void:
	emit_signal("fraction_changed", fraction)

func gain_for_player(plId : int, gain : int) -> void:
	if plId == playerId:
		productionManager2.increase_money(gain)
