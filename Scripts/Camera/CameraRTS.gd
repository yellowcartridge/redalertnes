extends RemoteTransform2D
class_name PlayerController

onready var mousePos : Vector2 = Vector2()
onready var mouseGlobalPos : Vector2 = Vector2()

export (String) var up_action_command : String = ""
export (String) var down_action_command : String = ""
export (String) var left_action_command : String = ""
export (String) var right_action_command : String = ""
export (String) var select_action_command : String = ""
export (String) var start_action_command : String = ""
export (String) var accept_action_command : String = ""
export (String) var decline_action_command : String = ""

export (bool) var mouse_control_enabled : bool = false

export (NodePath) var worldPath : NodePath = NodePath()
export (NodePath) var cursorPath : NodePath = NodePath()
export (NodePath) var selectionFramePath : NodePath = NodePath()

export (int) var marginX : int = 10
export (int) var marginY : int = 10

export (int) var panSpeed : int = 100

export (float) var maxBuildDistance : float = 100

export (Global.PlayerColor) var color : int = Global.PlayerColor.BLUE
export (Global.PlayerFraction) var fraction : int = Global.PlayerFraction.AlliedForces setget set_fraction

enum AllyState {
	ALLY,
	ENEMY,
	NEUTRAL
}

export (int) var playerId : int = 1

export (Dictionary) var relationship : Dictionary = {
	2 : AllyState.ENEMY
}

export (String) var selectionTileName : String = "selection_tile"
export (String) var goodBuildingPlaceTileName : String = "good_building_place_tile"
export (String) var badBuildingPlaceTileName : String = "bad_building_place_tile"
export (String) var groundTileName : String = "ground_tile"
export (String) var waterTileName : String = "water_tile"
export (String) var oreTileName : String = "ore_tile"

export (NodePath) var drawingViewport : NodePath = NodePath()
export (NodePath) var framesLayerPath : NodePath = NodePath()

export (PackedScene) var goodBuildingPlaceScene : PackedScene = null
export (PackedScene) var badBuildingPlaceScene : PackedScene = null

onready var gameController = get_node(worldPath)
onready var viewport = get_node(drawingViewport)
onready var viewportSize = viewport.size

onready var tilemap : TileMap = gameController.get_node("Navigation/TileMap")
onready var cursor : Cursor = get_node(cursorPath) #get_tree().get_root().get_node("World/CursorLayer/Cursor")
onready var productionManager2 = $ProductionManager2
onready var selection_frame : ColorRect = get_node(selectionFramePath)
onready var framesLayer : CanvasLayer = get_node(framesLayerPath)
onready var placingTiles : Control = framesLayer.get_node("PlacingTiles")

var cameraTile : Vector2 = Vector2()
var prev_position : Vector2 = Vector2()

var is_dragging : bool = false
var startFrameGlobal : Vector2 = Vector2()
var endFrameGlobal : Vector2 = Vector2()
var startFrameLocal : Vector2 = Vector2()
var endFrameLocal : Vector2 = Vector2()

var startFrameTilePos : Vector2 = Vector2()
var endFrameTilePos : Vector2 = Vector2()

var move_to_point : Vector2 = Vector2()

var selectedTiles : Array = []
var placementMarks : Array = []

const accidentallyClickTresholdSqr : int = 16

enum PlayerStates {
	IDLE,
	MOVE,
	ATTACK,
	EXPAND,
	SELL,
	REPAIR,
	PLACE_BUILDING,
	PLACE_UNIT,
	GAME_ENDED
};

var state = PlayerStates.IDLE

var buildingCanBePlaced : bool = false
var unitCanBePlaced : bool = false
var tileToPlaceBuilding : Vector2 = Vector2()
var tileToPlaceUnit : Vector2 = Vector2()
var productionToPlace : Node = null
var productionToPlaceName : String = ""

var expandingUnit = null
const INVALID_OBJECT : int = -1
onready var mousePointsToObject : int = INVALID_OBJECT

export (float) var cursor_acceleration : float = 10.0
onready var cursor_position : Vector2 = Vector2()
onready var prev_cursor_position : Vector2 = Vector2()
onready var cursor_speed : Vector2 = Vector2()

onready var just_created : bool = true

var selected_units : Array = []

var fog_of_war_size : Vector2 = Vector2()
var fog_of_war_matrix : Array = []

signal request_add_building_to_world
signal request_add_unit_to_world
signal request_expand_unit

signal selected_unit_become_visible
signal selected_unit_become_unvisible

signal fog_of_war_changed

signal fraction_changed

signal end_game

func _ready():
	update_position = true
	
	gameController.connect("unit_mouse_enter", self, "unit_mouse_enter")
	gameController.connect("unit_mouse_exit", self, "unit_mouse_exit")
	gameController.connect("unit_destroyed", self, "unit_destroyed")
	gameController.connect("building_mouse_enter", self, "building_mouse_enter")
	gameController.connect("building_mouse_exit", self, "building_mouse_exit")
	gameController.connect("building_destroyed", self, "building_destroyed")
	gameController.connect("end_game", self, "end_game")
	
	connect("request_add_building_to_world", gameController, "add_building_to_world")
	connect("request_add_unit_to_world", gameController, "add_unit_to_world")
	connect("request_expand_unit", gameController, "expand_object")
	
	productionManager2.connect("placeBuilding", self, "_on_placeBuilding")
	productionManager2.connect("placeUnit", self, "_on_placeUnit")
	
	gameController.connect("gain_for_player", self, "gain_for_player")
	
	gameController.connect("unit_become_visible", self, "unit_become_visible")
	gameController.connect("unit_become_unvisible", self, "unit_become_unvisible")
	
	gameController.connect("update_fog_of_war", self, "update_fog_of_war")
	gameController.connect("update_building_fog_of_war", self, "update_building_fog_of_war")
	
	cursor_position = cursor.position
	
	update_fraction()
	
	set_global_position(gameController.player_ready(self))
	
	if tilemap:
		var rect : Rect2 = tilemap.get_used_rect()
		fog_of_war_size = Vector2(rect.size.x, rect.size.y)
		var matrix_size : int = fog_of_war_size.x * fog_of_war_size.y
		fog_of_war_matrix.resize(matrix_size)
		for i in range(matrix_size):
			fog_of_war_matrix[i] = 0
		
		cameraTile = tilemap.world_to_map(global_position)
		prev_position = position
		emit_signal("fog_of_war_changed", cameraTile)
		

func destroy_placement_mark() -> void:
	for mark in placementMarks:
		placingTiles.remove_child(mark)
		mark.queue_free()
	placementMarks.clear()

func _on_placeBuilding(productionName : String):
	state = PlayerStates.PLACE_BUILDING
	productionToPlace = productionManager2.get_production_instance(productionName)
	productionToPlaceName = productionName
	
func _on_placeUnit(productionName : String):
	state = PlayerStates.PLACE_UNIT
	productionToPlace = productionManager2.get_production_instance(productionName)
	productionToPlaceName = productionName
	
func _input(event):
	if not mouse_control_enabled:
		return
		
	if event is InputEventMouse or event is InputEventScreenTouch:
		mousePos = event.position
		mouseGlobalPos = global_position + (mousePos - viewportSize / 2)
		cursor_position = mousePos
		cursor.position = cursor_position
		
	if event is InputEventMouseMotion:
		if not is_dragging and Input.is_action_pressed(accept_action_command):
			startFrameGlobal = mouseGlobalPos
			startFrameLocal = mousePos
			is_dragging = true
		if is_dragging and Input.is_action_pressed(accept_action_command):
			endFrameGlobal = mouseGlobalPos
			endFrameLocal = mousePos
			draw_selection_frame()
		
func _process(delta):
	# smooth movement
	var inpx = (int(Input.is_action_pressed(right_action_command)) - int(Input.is_action_pressed(left_action_command)))
	var inpy = (int(Input.is_action_pressed(down_action_command)) - int(Input.is_action_pressed(up_action_command)))

	cursor_position.x = lerp(cursor_position.x, cursor_position.x + inpx * cursor_acceleration, cursor_acceleration * delta)
	cursor_position.y = lerp(cursor_position.y, cursor_position.y + inpy * cursor_acceleration, cursor_acceleration * delta)
	
	cursor_position.x = clamp(cursor_position.x, 0, viewportSize.x)
	cursor_position.y = clamp(cursor_position.y, 0, viewportSize.y)
	
	cursor.position = cursor_position
	mousePos = cursor.position
	mouseGlobalPos = global_position + (cursor.position - viewportSize / 2)
	
	var cursor_moved : bool = (prev_cursor_position != cursor.position)
	prev_cursor_position = cursor.position
	
	var space_state = get_world_2d().direct_space_state
	var res : Array = space_state.intersect_point(mouseGlobalPos, 1, [], 6)
	
	if not res.empty():
		mousePointsToObject = res[0].collider_id
	else:
		mousePointsToObject = INVALID_OBJECT
	
	if state == PlayerStates.GAME_ENDED:
		if Input.is_action_just_released(accept_action_command) or Input.is_action_just_released(decline_action_command) or Input.is_action_just_released("ui_start"):
			get_tree().change_scene("res://Scenes/TitleScreen/TitleScreen.tscn")
		return
		
	if not is_dragging:
		clear_building_placement()
	
	var moveLeft : bool = false
	var moveRight : bool = false
	var moveUp : bool = false
	var moveDown : bool = false
	var drag_action_just_released : bool = false

	if cursor_position.x < marginX:
		moveLeft = true
		position.x = lerp(position.x, position.x - abs(mousePos.x - marginX) / marginX * panSpeed, delta)
	elif cursor_position.x > viewportSize.x - marginX:
		moveRight = true
		position.x = lerp(position.x, position.x + abs(mousePos.x - viewportSize.x + marginX) / marginX * panSpeed, delta)

	if cursor_position.y < marginY:
		moveUp = true
		position.y = lerp(position.y, position.y - abs(mousePos.y - marginY) / marginY * panSpeed, delta)
	elif cursor_position.y > viewportSize.y - marginY:
		moveDown = true
		position.y = lerp(position.y, position.y + abs(mousePos.y - viewportSize.y + marginY) / marginY * panSpeed, delta)

	var tilemap_rect : Rect2 = tilemap.get_used_rect()
	position.x = clamp(position.x, tilemap_rect.position.x * tilemap.cell_size.x + viewportSize.x / 2, tilemap_rect.end.x * tilemap.cell_size.x - viewportSize.x / 2 + get_parent().get_node("UI/Panel").rect_size.x)
	position.y = clamp(position.y, tilemap_rect.position.y * tilemap.cell_size.y + viewportSize.y / 2, tilemap_rect.end.y * tilemap.cell_size.y - viewportSize.y / 2)
		
	if prev_position != position:
		prev_position = position
		cameraTile = tilemap.world_to_map(position)
		emit_signal("fog_of_war_changed", cameraTile)

	if cursor_position.x > viewportSize.x - get_parent().get_node("UI/Panel").rect_size.x:
		cursor.set_arrow()
	else:
	
		match state:
			PlayerStates.IDLE:
				if not selected_units.empty() and Input.is_action_just_released(decline_action_command):
					deselect_all()

				if (moveUp or moveDown or moveLeft or moveRight) and is_dragging and Input.is_action_pressed(accept_action_command):
					endFrameGlobal = mouseGlobalPos
					endFrameLocal = mousePos
					draw_selection_frame()
				elif is_dragging and Input.is_action_just_released(accept_action_command):
					drag_action_just_released = true
					if startFrameLocal.distance_squared_to(mousePos) > accidentallyClickTresholdSqr:
						endFrameGlobal = mouseGlobalPos
						endFrameLocal = mousePos
						is_dragging = false
						draw_selection_frame(false)
						area_selected()
					else:
						endFrameGlobal = startFrameGlobal
						is_dragging = false
						draw_selection_frame(false)
						
				if cursor_moved:
					if not is_dragging and Input.is_action_pressed(accept_action_command):
						startFrameGlobal = mouseGlobalPos
						startFrameLocal = mousePos
						is_dragging = true
					if is_dragging and Input.is_action_pressed(accept_action_command):
						endFrameGlobal = mouseGlobalPos
						endFrameLocal = mousePos
						draw_selection_frame()
				
				
				if mousePointsToObject != INVALID_OBJECT and not is_dragging and not drag_action_just_released:
					var instance = instance_from_id(mousePointsToObject)
					
					if instance is Unit:
						if is_enemy(instance):
							if not selected_units.empty() and units_in_selection_can_attack():
								set_attack_action(instance)
							else:
								cursor.set_arrow()
						else:
							if instance.selected and instance.expandable:
								set_expand_action(instance)
							else:
								set_select_action(instance)
							
					elif instance is Building:
						if is_enemy(instance):
							if not selected_units.empty() and units_in_selection_can_attack():
								set_attack_action(instance)
							else:
								cursor.set_arrow()
						else:
							if not selected_units.empty() and units_in_selection_can_harvest() and instance.can_collect_ore:
								set_deliver_action(instance)
							else:
								cursor.set_arrow()
					else:
						cursor.set_arrow()
						
				else:
					if is_mouse_points_to_ground():
						if not selected_units.empty():
							set_move_action(drag_action_just_released)
						else:
							cursor.set_arrow()
					elif is_mouse_points_to_ore():
						if not selected_units.empty():
							if units_in_selection_can_harvest():
								set_harvest_action(mouseGlobalPos)
							else:
								set_move_action(drag_action_just_released)
						else:
							cursor.set_arrow()
					else:
						cursor.set_arrow()

			PlayerStates.PLACE_BUILDING:
				draw_building_placement(productionToPlace, mouseGlobalPos)
				
				if Input.is_action_just_released(accept_action_command):
					if buildingCanBePlaced:
						clear_building_placement()
						emit_signal("request_add_building_to_world", productionToPlace, productionToPlaceName, tileToPlaceBuilding, playerId, color, productionManager2)
						state = PlayerStates.IDLE
					else:
						clear_area()
						
				if Input.is_action_just_released(decline_action_command):
					clear_building_placement()
					productionToPlace.queue_free()
					productionToPlace = null
					productionToPlaceName = ""
					state = PlayerStates.IDLE
			PlayerStates.PLACE_UNIT:
				draw_unit_placement(productionToPlace)
				
				if Input.is_action_just_released(accept_action_command):
					if unitCanBePlaced:
						clear_unit_placement()
						emit_signal("request_add_unit_to_world", productionToPlace, productionToPlaceName, tileToPlaceUnit, playerId, color, productionManager2)
						state = PlayerStates.IDLE
									
				if Input.is_action_just_released(decline_action_command):
					clear_unit_placement()
					productionToPlace.queue_free()
					productionToPlace = null
					productionToPlaceName = ""
					state = PlayerStates.IDLE
				
			_:
				cursor.set_arrow()
				
	if moveUp:
		if moveLeft:
			cursor.set_up_left_arrow()
		elif moveRight:
			cursor.set_up_right_arrow()
		else:
			cursor.set_up_arrow()
	elif moveDown:
		if moveLeft:
			cursor.set_down_left_arrow()
		elif moveRight:
			cursor.set_down_right_arrow()
		else:
			cursor.set_down_arrow()
	elif moveLeft:
		if moveUp:
			cursor.set_up_left_arrow()
		elif moveDown:
			cursor.set_down_left_arrow()
		else:
			cursor.set_left_arrow()
	elif moveRight:
		if moveUp:
			cursor.set_up_right_arrow()
		elif moveDown:
			cursor.set_down_right_arrow()
		else:
			cursor.set_right_arrow()

func set_move_action(drag_action_just_released : bool):
	cursor.set_move()
								
	if not is_dragging and Input.is_action_just_released(accept_action_command) and not drag_action_just_released:
		start_move_selection(mouseGlobalPos)

func set_select_action(instance):
	cursor.set_select()
	
	if not is_dragging and Input.is_action_just_released(accept_action_command):
		deselect_all()
		instance.set_selected(true)
		selected_units.push_back(instance.get_instance_id())
		emit_signal("selected_unit_become_visible", instance)

func set_attack_action(object_to_attack):
	cursor.set_attack()
	
	if Input.is_action_just_released(accept_action_command):
		start_attack_selection(object_to_attack)
	
func set_harvest_action(tilePosition: Vector2):
	cursor.set_attack()
	
	if Input.is_action_just_released(accept_action_command):
		start_harvest_selection(tilePosition)
		
func set_deliver_action(building: Building):
	cursor.set_collect()
	
	if Input.is_action_just_released(accept_action_command):
		if building.can_collect_ore:
			start_deliver_selection(building.get_delivery_position())
	
func set_expand_action(object_to_expand):
	cursor.set_expand()
	
	if object_to_expand and "expansionName" in object_to_expand:
		#TODO: move tiles to Resource and don't instantiate expansion here
		var expansion = productionManager2.get_production_instance(object_to_expand.expansionName)
		draw_building_placement(expansion, object_to_expand.global_position, false, [object_to_expand])
		expansion.queue_free()
	
	if Input.is_action_just_released(accept_action_command):
		if buildingCanBePlaced and object_to_expand and "expansionName" in object_to_expand:
			clear_building_placement()
			emit_signal("request_expand_unit", object_to_expand, playerId, color, productionManager2)
			state = PlayerStates.IDLE

func get_units_in_area(rect : Rect2) -> Array:
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(rect.size / 2)
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_shape(shape)
	query.set_transform(Transform2D(0.0, rect.position + rect.size / 2))
	query.set_exclude([tilemap])
	query.collision_layer = 2
	
	var res : Array = space_state.intersect_shape(query) 
	
	var units : Array = []
	
	if not res.empty():
		for collision in res:
			if collision.collider is Unit and collision.collider.ownerId == playerId:
				units.push_back(collision.collider)
	
	return units
	
func start_move_selection(point : Vector2):
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if not unit:
			selected_units.erase(unit_id)
		else:
			unit.move_unit(point, selected_units)
		
func start_attack_selection(object_to_attack):
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if not unit:
			selected_units.erase(unit_id)
		else:
			unit.attack_enemy(object_to_attack, selected_units)
		
func start_harvest_selection(position_to_harvest: Vector2):
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if not unit:
			selected_units.erase(unit_id)
		else:
			var tile_cell = tilemap.world_to_map(position_to_harvest)
			unit.mine_ore(position_to_harvest, tile_cell)

func start_deliver_selection(position_to_deliver: Vector2):
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if not unit:
			selected_units.erase(unit_id)
		else:
			unit.deliver_ore(position_to_deliver)

func units_in_selection_can_attack():
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if unit and unit.can_attack:
			return true
	return false
	
func units_in_selection_can_harvest():
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if unit and unit.can_harvest:
			return true
	return false


func deselect_all():
	while selected_units.size() > 0:
		var unit = instance_from_id(selected_units[0])
		if unit:
			unit.set_selected(false)
			emit_signal("selected_unit_become_unvisible", unit)

		selected_units.erase(selected_units[0])

func deselect_unit(unit_id : int):
	if unit_id in selected_units:
		selected_units.erase(unit_id)
		var unit = instance_from_id(unit_id)
		if unit:
			unit.set_selected(false)
			emit_signal("selected_unit_become_unvisible", unit)

func area_selected():
	var start = startFrameTilePos
	var end = endFrameTilePos
	print(start, " ", end)
	#var rectTopLeftPosition : Vector2 = Vector2(min(start.x, end.x), min(start.y, end.y))
	#var rectBottomRightPosition : Vector2 = Vector2(max(start.x, end.x), max(start.y, end.y))
	#var area : Rect2 = Rect2(rectTopLeftPosition, rectBottomRightPosition - rectTopLeftPosition)
	var area : Rect2 = Rect2(start, end - start)
	var ut = get_units_in_area(area)
	#if not Input.is_key_pressed(KEY_SHIFT):
	#	deselect_all()
	deselect_all()
	for u in ut:
		selected_units.push_back(u.get_instance_id())
		u.selected = true
		emit_signal("selected_unit_become_visible", u)

func transform_world_to_screen_coord(world: Vector2) -> Vector2:
	return global_transform.xform_inv(world) + viewportSize / 2

func draw_selection_frame(show_frame : bool = true):
	if not selection_frame:
		return
		
	var frame_pos = Vector2(min(startFrameGlobal.x, endFrameGlobal.x), min(startFrameGlobal.y, endFrameGlobal.y))
	var frame_size = Vector2(abs(startFrameGlobal.x - endFrameGlobal.x), abs(startFrameGlobal.y - endFrameGlobal.y))

	var frame_tile_pos = tilemap.world_to_map(frame_pos)
	var frame_tile_pos_end = tilemap.world_to_map(frame_pos + frame_size)

	startFrameTilePos = tilemap.map_to_world(frame_tile_pos)
	endFrameTilePos = tilemap.map_to_world(frame_tile_pos_end + Vector2(1, 1))

	var frame_local_pos = transform_world_to_screen_coord(startFrameTilePos)
	
	selection_frame.rect_size = endFrameTilePos - startFrameTilePos
	selection_frame.rect_global_position = frame_local_pos

	selection_frame.rect_size *= int(show_frame)
#	if not selectedTiles.empty():
#		for cell in selectedTiles:
#			tilemap.set_cell(cell.x, cell.y, cell.tile)
#	selectedTiles.clear()
#
#	if not show_frame:
#		return
#
#	var selection_tile_index = tilemap.tile_set.find_tile_by_name(selectionTileName)
#
#	var frame_pos = Vector2(min(startFrameGlobal.x, endFrameGlobal.x), min(startFrameGlobal.y, endFrameGlobal.y))
#	var frame_size = Vector2(abs(startFrameGlobal.x - endFrameGlobal.x), abs(startFrameGlobal.y - endFrameGlobal.y))
#
#	var frame_tile_pos = tilemap.world_to_map(frame_pos)
#	var frame_tile_pos_end = tilemap.world_to_map(frame_pos + frame_size)
#
#	startFrameTilePos = tilemap.map_to_world(frame_tile_pos)
#	endFrameTilePos = tilemap.map_to_world(frame_tile_pos_end + Vector2(1, 1))
#
#	for i in range(frame_tile_pos.x, frame_tile_pos_end.x + 1):
#		for j in range(frame_tile_pos.y, frame_tile_pos_end.y + 1):
#			var cellInfo : Dictionary = {}
#			cellInfo.x = i
#			cellInfo.y = j
#			cellInfo.tile = tilemap.get_cell(i, j)
#
#			if not selectedTiles.has(cellInfo):
#				selectedTiles.push_back(cellInfo)
#				tilemap.set_cell(i, j, selection_tile_index)
	
func clear_building_placement():
	destroy_placement_mark()
	
func draw_building_placement(building : Building, position : Vector2, check_distance : bool = true, exclusions : Array = []):	
	clear_building_placement()
			
#	var good_building_place_tile_index = tilemap.tile_set.find_tile_by_name(goodBuildingPlaceTileName)
#	var bad_building_place_tile_index = tilemap.tile_set.find_tile_by_name(badBuildingPlaceTileName)
	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
	
	var main_tile_pos = tilemap.world_to_map(position)
	tileToPlaceBuilding = main_tile_pos

#	var main_tile_index : int = tilemap.get_cell(main_tile_pos.x, main_tile_pos.y)
#
	building.calculate_orientation(tilemap, main_tile_pos)
	
	if check_distance:
		var closest_building : Building = find_closest_building(position)
		
		if closest_building:
			var building_main_tile = tilemap.world_to_map(closest_building.global_position)
			var main_tile_world_coord = tilemap.map_to_world(main_tile_pos) + tilemap.cell_size / 2
			var building_tile_world_coord = tilemap.map_to_world(building_main_tile) + tilemap.cell_size / 2
			
			if main_tile_world_coord.distance_squared_to(building_tile_world_coord) > maxBuildDistance*maxBuildDistance:
				buildingCanBePlaced = false
				
				for cell in building.get_tiles():
					var cell_pos = Vector2(main_tile_pos.x + cell.x, main_tile_pos.y + cell.y)
					var mark = badBuildingPlaceScene.instance()
					mark.set_global_position(transform_world_to_screen_coord(tilemap.map_to_world(cell_pos) + tilemap.cell_size / 2))
					placementMarks.push_back(mark)
					placingTiles.add_child(mark)
				return
	
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(tilemap.cell_size / 2 - Vector2(1, 1))
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_collision_layer(Global.CollisionLayer.Units)
	query.set_shape(shape)
	query.set_exclude(exclusions)
	
	var allCellsSucceded:bool = true
	for cell in building.get_tiles():
		var cell_pos = Vector2(main_tile_pos.x + cell.x, main_tile_pos.y + cell.y)
		var tile_position = tilemap.map_to_world(cell_pos)
		var tile_position_centered = tile_position + (tilemap.cell_size / 2)

		query.set_transform(Transform2D(0.0, tile_position_centered))
		var res : Array = space_state.intersect_shape(query) 
		
		var materials : Array = []
		
		if cell.has("materials"):
			materials = cell.materials
		
		var tile_index : int = tilemap.get_cell(cell_pos.x, cell_pos.y)
		var tile_name : String = tilemap.tile_set.tile_get_name(tile_index)
		
		var mark
		if res.empty() and ((materials.empty() and tile_index == ground_tile_index) or tile_name in materials):
			mark = goodBuildingPlaceScene.instance()
		else:
			mark = badBuildingPlaceScene.instance()
			allCellsSucceded = false
		
		mark.set_global_position(transform_world_to_screen_coord(tilemap.map_to_world(cell_pos) + tilemap.cell_size / 2))
		
		placementMarks.push_back(mark)
		placingTiles.add_child(mark)
	
	buildingCanBePlaced = allCellsSucceded
	
func find_closest_building(position : Vector2):
	var buildings : Array = gameController.get_buildings_of_player(playerId)
	
	var closestDistSqr : float = SteeringBehavior.MaxFloat
	var closestBuilding: Building = null

	for buildingId in buildings:
		var building : Building = instance_from_id(buildingId)
		
		if not building:
			continue
			
		var distToBuilding = position.distance_squared_to(building.global_position)
		if distToBuilding < closestDistSqr:
			closestDistSqr = distToBuilding
			closestBuilding = building
			
	return closestBuilding

func clear_unit_placement():
	destroy_placement_mark()
	
func draw_unit_placement(unit : Unit):	
	clear_unit_placement()
			
#	var good_place_tile_index = tilemap.tile_set.find_tile_by_name(goodBuildingPlaceTileName)
#	var bad_place_tile_index = tilemap.tile_set.find_tile_by_name(badBuildingPlaceTileName)
#	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
#	var ore_tile_index = tilemap.tile_set.find_tile_by_name(oreTileName)
	
	var cursor_tile_pos = tilemap.world_to_map(mouseGlobalPos)
	var main_tile_world_coord = tilemap.map_to_world(cursor_tile_pos)
	var main_tile_world_coord_centered = main_tile_world_coord + tilemap.cell_size / 2
	
	tileToPlaceUnit = cursor_tile_pos
	
	var closest_building : Building = find_closest_building(main_tile_world_coord_centered)
	if closest_building:
		var building_main_tile = tilemap.world_to_map(closest_building.global_position)
		
		var building_tile_world_coord = tilemap.map_to_world(building_main_tile) + tilemap.cell_size / 2
		
		if main_tile_world_coord_centered.distance_squared_to(building_tile_world_coord) > maxBuildDistance*maxBuildDistance:
			unitCanBePlaced = false

			var mark = badBuildingPlaceScene.instance()
			mark.set_global_position(transform_world_to_screen_coord(main_tile_world_coord))
			placementMarks.push_back(mark)
			placingTiles.add_child(mark)
			return
	
	var space_state = get_world_2d().direct_space_state
	var shape : RectangleShape2D = RectangleShape2D.new()
	shape.set_extents(tilemap.cell_size / 2 - Vector2(1, 1))
	var query : Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	query.set_collision_layer(Global.CollisionLayer.Units)
	query.set_shape(shape)

	var tile_position = tilemap.map_to_world(cursor_tile_pos)
	var tile_position_centered = tile_position + (tilemap.cell_size / 2)

	query.set_transform(Transform2D(0.0, tile_position_centered))
	var res : Array = space_state.intersect_shape(query) 
	
	var tile_index = tilemap.get_cell(cursor_tile_pos.x, cursor_tile_pos.y)
	var tile_name : String = tilemap.tile_set.tile_get_name(tile_index)
	
	var materials : Array = []
	if "movableMaterials" in unit:
		materials = unit.movableMaterials

	var mark
	if res.empty() and (materials.empty() or tile_name in materials):
		mark = goodBuildingPlaceScene.instance()
		unitCanBePlaced = true
	else:
		mark = badBuildingPlaceScene.instance()
		unitCanBePlaced = false
		
	mark.set_global_position(transform_world_to_screen_coord(tile_position_centered))
	placementMarks.push_back(mark)
	placingTiles.add_child(mark)

func unit_mouse_enter(unit_id) -> void:
	mousePointsToObject = unit_id
	
func unit_mouse_exit(unit_id) -> void:
	if mousePointsToObject == unit_id:
		mousePointsToObject = INVALID_OBJECT

func unit_destroyed(unit_id) -> void:
	if mousePointsToObject == unit_id:
		mousePointsToObject = INVALID_OBJECT

	var unit : Unit = instance_from_id(unit_id)
	if unit.ownerId == playerId:
		deselect_unit(unit_id)
		productionManager2.productionDestroyed(unit.unitName)
		
func building_mouse_enter(building_id) -> void:
	mousePointsToObject = building_id
	
func building_mouse_exit(building_id) -> void:
	if mousePointsToObject == building_id:
		mousePointsToObject = INVALID_OBJECT
		
func building_destroyed(building_id, _position: Vector2, _tiles : Array) -> void:
	if mousePointsToObject == building_id:
		mousePointsToObject = INVALID_OBJECT
	
	var building : Building = instance_from_id(building_id)
	if building and building.ownerId == playerId:
		productionManager2.productionDestroyed(building.buildingName)
	
func is_mouse_points_to_ground() -> bool:
	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
	var water_tile_index = tilemap.tile_set.find_tile_by_name(waterTileName)
	
	var cursor_tile_pos = tilemap.world_to_map(mouseGlobalPos)
	
	var tilemapCell = tilemap.get_cell(cursor_tile_pos.x, cursor_tile_pos.y)
	return tilemapCell == ground_tile_index or tilemapCell == water_tile_index
	
func is_mouse_points_to_ore() -> bool:
	var ore_tile_index = tilemap.tile_set.find_tile_by_name(oreTileName)
	
	var cursor_tile_pos = tilemap.world_to_map(mouseGlobalPos)
	var tilemapCell = tilemap.get_cell(cursor_tile_pos.x, cursor_tile_pos.y)
	return tilemapCell == ore_tile_index

func end_game(winnerId : int):
	state = PlayerStates.GAME_ENDED
	emit_signal("end_game", winnerId == playerId)

func is_enemy(instance) -> bool:
	return is_in_relationship(instance, AllyState.ENEMY)
	
func is_in_relationship(instance, relations_state : int) -> bool:
	if "ownerId" in instance:
		var ownerId : int = instance.ownerId
		if relationship.has(ownerId) and relations_state == relationship[ownerId]:
			return true
			
	return false

func gain_for_player(plId : int, gain : int) -> void:
	if plId == playerId:
		productionManager2.increase_money(gain)

func unit_become_visible(unit : Unit, vp : Viewport) -> void:
	if vp == viewport:
		if unit.selected and unit.ownerId == playerId:
			emit_signal("selected_unit_become_visible", unit)

func unit_become_unvisible(unit : Unit, vp : Viewport) -> void:
	if vp == viewport:
		if unit.selected and unit.ownerId == playerId:
			emit_signal("selected_unit_become_unvisible", unit)

func set_fraction(value : int) -> void:
	if fraction != value:
		fraction = value
		
		update_fraction()
		
func update_fraction() -> void:
	emit_signal("fraction_changed", fraction)

func get_fog_of_war_size() -> Vector2:
	return fog_of_war_size

func get_tilemap_rect() -> Rect2:
	return tilemap.get_used_rect()
	
func get_tilemap_cell_size() -> Vector2:
	return tilemap.cell_size
		
func get_fog_of_war_matrix() -> Array:
	return fog_of_war_matrix

func update_fog_of_war(id : int, tile : Vector2, radius : float) -> void:
	if playerId != id:
		return
	
	if fog_of_war_matrix.empty():
		yield(self, "ready")
	
	var rect : Rect2 = get_tilemap_rect()
	var radius_in_tiles : Vector2 = Vector2(ceil(radius / tilemap.cell_size.x), ceil(radius / tilemap.cell_size.y))
	var left_top_tile : Vector2 = tile - radius_in_tiles
	var right_bottom_tile : Vector2 = tile + radius_in_tiles
	
	left_top_tile.x = clamp(left_top_tile.x, rect.position.x, rect.position.x + rect.size.x)
	left_top_tile.y = clamp(left_top_tile.y, rect.position.y, rect.position.y + rect.size.y)
	right_bottom_tile.x = clamp(right_bottom_tile.x, rect.position.x, rect.position.x + rect.size.x)
	right_bottom_tile.y = clamp(right_bottom_tile.y, rect.position.y, rect.position.y + rect.size.y)
	
	for x in range(left_top_tile.x, right_bottom_tile.x):
		for y in range(left_top_tile.y, right_bottom_tile.y):
			var index : int = (x - rect.position.x) * rect.size.y + (y - rect.position.y)
			if fog_of_war_matrix[index]  == 1:
				continue
			if tile.distance_squared_to(Vector2(x, y)) > radius_in_tiles.x * radius_in_tiles.y:
				continue
			fog_of_war_matrix[index] = 1
	
	emit_signal("fog_of_war_changed", cameraTile)

func update_building_fog_of_war(id : int, tile : Vector2) -> void:
	update_fog_of_war(id, tile, maxBuildDistance)


export (PackedScene) var EscapeAreaScene : PackedScene = preload("res://Scenes/Areas/EscapeArea/EscapeArea.tscn")
	
func clear_area():
	var escapeArea = EscapeAreaScene.instance()
	escapeArea.Init(productionToPlace.get_tiles(), tilemap.cell_size)
	escapeArea.set_global_position(tilemap.map_to_world(tileToPlaceBuilding))
	escapeArea.connect("area_cleared", self, "area_cleared")
	gameController.add_child(escapeArea)
	
func area_cleared():
	print("area_cleared")
	#emit_signal("request_add_building_to_world", buildingToPlace, productionToPlace, tileToPlaceBuilding, playerId, color, productionManager2)
