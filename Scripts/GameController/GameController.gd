extends Node2D

export (NodePath) var streamPlayer1 : NodePath = NodePath()
export (NodePath) var streamPlayer2 : NodePath = NodePath()
export (NodePath) var streamPlayer3 : NodePath = NodePath()

onready var BGMusicStreams: Array = [
	get_node(streamPlayer1),
	get_node(streamPlayer2),
	get_node(streamPlayer3)
]

onready var ground_navigation: Navigation2D = $Navigation
onready var water_navigation: Navigation2D = $WaterNavigation
onready var hybrid_navigation: Navigation2D = $WaterNavigation
onready var tilemap: TileMap = $Navigation/TileMap
onready var water_tilemap: TileMap = $WaterNavigation/TileMap
onready var hybrid_tilemap: TileMap = $WaterNavigation/TileMap
onready var air_tilemap: TileMap = $Navigation/TileMap

export (String) var oreTileName: String = "ore_tile"
export (String) var groundTileName: String = "ground_tile"
export (String) var waterTileName: String = "water_tile"
export (String) var waterTileTraversableName: String = "traversable_water_tile"
export (String) var stoneTileName: String = "stone_tile"

export (NodePath) var firstPlayerPath: NodePath = NodePath()
export (NodePath) var secondPlayerPath: NodePath = NodePath()


onready var buildingsByPlayer : Dictionary = {}
onready var unitsByPlayer : Dictionary = {}
onready var refineriesByPlayer : Dictionary = {}
onready var harvestersByPlayer : Dictionary = {}
onready var mcvsByPlayer : Dictionary = {}

onready var aiController = $AIController
onready var firstPlayer : PlayerController = get_node_or_null(firstPlayerPath)
onready var secondPlayer : PlayerController = get_node_or_null(secondPlayerPath)

signal expand_unit_ready
signal expand_unit_abort
signal unit_mouse_enter
signal unit_mouse_exit
signal building_mouse_enter
signal building_mouse_exit
signal building_destroyed
signal unit_destroyed

signal end_game

signal gain_for_player
signal gain_for_ai

signal unit_become_visible
signal unit_become_unvisible

signal update_fog_of_war
signal update_building_fog_of_war

func _ready():
#	var screen_size = OS.get_screen_size()
#	var window_size = OS.get_window_size()

#	OS.set_window_position(screen_size*0.5 - window_size*0.5)

	randomize()
	init_navigation()
#	play_random_music()
	
	#units = get_tree().get_nodes_in_group("Units")
	#enemies = get_tree().get_nodes_in_group("Enemies")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	if aiController:
		aiController.set_process(Global.aiEnabled)
		aiController.playerId = 2
		aiController.color = Global.secondPlayerColor
		aiController.fraction = Global.secondPlayerFraction
		if Global.aiEnabled:
			init_player(aiController)
	
	if firstPlayer:
		firstPlayer.playerId = 1
		firstPlayer.color = Global.firstPlayerColor
		firstPlayer.fraction = Global.firstPlayerFraction
		
	if secondPlayer:
		secondPlayer.playerId = 2
		secondPlayer.color = Global.secondPlayerColor
		secondPlayer.fraction = Global.secondPlayerFraction
		
func _process(_delta):
	OS.set_window_title("RA Demastered | fps: " + str(Engine.get_frames_per_second()))
	
	if get_units_of_player(2).empty() and get_buildings_of_player(2).empty():
		emit_signal("end_game", 1)
	elif get_units_of_player(1).empty() and get_buildings_of_player(1).empty():
		emit_signal("end_game", 2)

func init_navigation() -> void:
	var tile_index = tilemap.tile_set.find_tile_by_name(waterTileName)
	var traversable_water_tile_index = tilemap.tile_set.find_tile_by_name(waterTileTraversableName)
	var tiles = tilemap.get_used_cells_by_id(tile_index)

	for tile in tiles:
		water_tilemap.set_cell(tile.x, tile.y, traversable_water_tile_index)

func player_ready(player : PlayerController) -> Vector2:
	return init_player(player)

func init_player(player) -> Vector2:
	print(player.name)
	var basePositions : Array = get_tree().get_nodes_in_group("MCVPosition")
	if basePositions.empty():
		return Vector2()
	
	var index : int = 0
	if basePositions.size() > 1:
		index = randi() % basePositions.size()
		
	var playerPos : Vector2 = basePositions[index].global_position
	basePositions[index].get_parent().remove_child(basePositions[index])
	basePositions[index].queue_free()
	
	add_mcv(player.productionManager2, playerPos, player.playerId, player.color)
	
	return playerPos

func play_random_music():
	for player in BGMusicStreams:
		player.stop()

	var rand_nb = randi() % BGMusicStreams.size()
	BGMusicStreams[rand_nb].play()


func _on_StreamPlayer_finished():
	#streamPlayer.play()
	play_random_music()

var units = []
var selected_units = []
var enemies = []
#var buttons = []

#onready var button = preload("res://Scenes/Button.tscn")

func has_selection():
	return not selected_units.empty()
	
func units_in_selection_can_attack():
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if unit and unit.can_attack:
			return true
	return false
	
func units_in_selection_can_harvest():
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if unit and unit.can_harvest:
			return true
	return false

func select_unit(unit):
	if not selected_units.has(unit.get_instance_id()):
		selected_units.append(unit.get_instance_id())
	print("selected %s" % unit.name)
#	create_buttons()
	
func deselect_unit(unit):
	if selected_units.has(unit.get_instance_id()):
		selected_units.erase(unit.get_instance_id())
	print("deselected %s" % unit.name)
#	create_buttons()

func select_unit_exclusive(unit):
	if not Input.is_key_pressed(KEY_SHIFT):
		deselect_all()

	select_unit(unit)
	print("selected exclusively %s" % unit.name)


func deselect_all():
	print("deselect all")
	while selected_units.size() > 0:
		var unit = instance_from_id(selected_units[0])
		if unit:
			unit.set_selected(false)
		else:
			selected_units.erase(selected_units[0])

func unit_destroyed(unit_id):
	if unit_id in selected_units:
		selected_units.erase(unit_id)
	
	var unit : Unit = instance_from_id(unit_id)
	remove_unit(unit, unit.unitName, unit.ownerId)
	emit_signal("unit_destroyed", unit_id)
		
#func was_pressed(obj):
#	for unit in selected_units:
#		if unit.name == obj.name:
#			unit.set_selected(false)
#			break

func get_units_in_area(area):
	var u = []
	units = get_tree().get_nodes_in_group("Units")
	for unit in units:
		if unit.position.x > area[0].x and unit.position.x < area[1].x:
			if unit.position.y > area[0].y and unit.position.y < area[1].y:
				u.append(unit)
	return u

func area_selected(obj):
	var start = obj.startFrameTilePos
	var end = obj.endFrameTilePos
	var area = []
	area.append(Vector2(min(start.x, end.x), min(start.y, end.y)))
	area.append(Vector2(max(start.x, end.x), max(start.y, end.y)))
	var ut = get_units_in_area(area)
	if not Input.is_key_pressed(KEY_SHIFT):
		deselect_all()
	for u in ut:
		u.selected = not u.selected

# units control

			
func enemy_clicked(enemy):
	for unit_id in selected_units:
		var unit = instance_from_id(unit_id)
		if not unit:
			selected_units.erase(unit_id)
		else:
			if unit.can_attack:
				unit.attack_enemy(enemy)

func expand_unit_ready(unit):
	print("unit expanding")
	emit_signal("expand_unit_ready", unit)

func expand_unit_abort(unit):
	emit_signal("expand_unit_abort", unit)
	
func unit_mouse_enter(unit_id):
	emit_signal("unit_mouse_enter", unit_id)
	
func unit_mouse_exit(unit_id):
	emit_signal("unit_mouse_exit", unit_id)
	
func building_mouse_enter(building_id):
	emit_signal("building_mouse_enter", building_id)
	
func building_mouse_exit(building_id):
	emit_signal("building_mouse_exit", building_id)
	
func building_destroyed(building_id : int, playerId : int, position : Vector2, tiles : Array):
	clear_building_place(position, tiles)
	emit_signal("building_destroyed", building_id, position, tiles)
	remove_instance_for_player(playerId, buildingsByPlayer, building_id)
	
func gain_for_player(playerId : int, gain : int):
	emit_signal("gain_for_player", playerId, gain)
	
func gain_for_ai(gain : int):
	emit_signal("gain_for_ai", gain)

func request_closest_ore_tile_position(harvester_id : int) -> void:
	var harvester = instance_from_id(harvester_id)
	if harvester:
		var closestTilePos = find_closest_ore_tile(harvester.global_position)
		if closestTilePos.success:
			harvester.mine_ore(closestTilePos.position, closestTilePos.tile)
		else:
			harvester.no_ore_left()

func request_closest_ore_tile_position_cb(position : Vector2, successCB : FuncRef, failedCB : FuncRef) -> void:
		var closestTilePos = find_closest_ore_tile(position)
		if closestTilePos.success:
			if successCB.is_valid():
				successCB.call_func(closestTilePos.tile)
		else:
			if failedCB.is_valid():
				failedCB.call_func()

func find_closest_ore_tile(position: Vector2):
	return tilemap.find_closest_tile_by_name(position, oreTileName)
	
func request_closest_ore_refinery_position(harvester_id : int):
	var harvester = instance_from_id(harvester_id)
	if harvester:
		var closestRefPos = find_closest_ore_refinery(harvester.global_position, harvester.ownerId)
		if closestRefPos.success:
			harvester.deliver_ore(closestRefPos.position)
		else:
			harvester.no_refinery_left()
			
func request_closest_ore_refinery_position_cb(position : Vector2, ownerId : int, successCB : FuncRef, failedCB : FuncRef):
	var closestRefPos = find_closest_ore_refinery(position, ownerId)
	if closestRefPos.success:
		if successCB.is_valid():
			successCB.call_func(closestRefPos.position)
	else:
		if failedCB.is_valid():
			failedCB.call_func()
		
func find_closest_ore_refinery(position: Vector2, playerId : int):
	var refineries = get_refineries_of_player(playerId)
	
	var closestDistSqr : float = SteeringBehavior.MaxFloat
	var closestRef: Vector2 = Vector2()
	var refineryFound: bool = false
	for refineryId in refineries:
		var refinery = instance_from_id(refineryId)
		if not refinery:
			continue
		var ref_position = refinery.get_delivery_position()
		var distToRef = position.distance_squared_to(ref_position)
		if distToRef < closestDistSqr:
			closestDistSqr = distToRef
			closestRef = ref_position
			refineryFound = true
			
	return {"success": refineryFound, "position": closestRef}

func ore_tile_collected(tile_cell : Vector2) -> void:
	var ore_tile_index = tilemap.tile_set.find_tile_by_name(oreTileName)
	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
	if ore_tile_index < 0 or ground_tile_index < 0:
		return
	
	if tilemap.get_cell(tile_cell.x, tile_cell.y) == ore_tile_index:
		tilemap.set_cell(tile_cell.x, tile_cell.y, ground_tile_index)

func unit_created(_unit : Unit) -> void:
	pass
	
#func unit_destroyed(unit : Unit) -> void:
#	pass

func building_created(_building : Building) -> void:
	pass
	
#func building_destroyed(building : Building) -> void:
#	pass

func replace_tiles(_tiles : Array) -> void:
	pass

func add_building_to_world(building : Building, productionName : String, tileToPlace : Vector2, playerId : int, playerColor : int, productionManager : ProductionManager):
	init_production(tilemap.map_to_world(tileToPlace), building, playerId, playerColor)
	building.calculate_orientation(tilemap, tileToPlace)
	var buildingTiles : Array = building.get_tiles()
	for tile in buildingTiles:
		tilemap.set_cell(tileToPlace.x + tile.x, tileToPlace.y + tile.y, tilemap.tile_set.find_tile_by_name(tile.name))

	building.connect("building_destroyed", self, "building_destroyed")

	add_child(building)
	register_instance_for_player(playerId, buildingsByPlayer, building.get_instance_id())
	
	productionManager.productionPlaced(productionName)
		
	if productionName == "OreRefinery":
		register_instance_for_player(playerId, refineriesByPlayer, building.get_instance_id())
		if is_instance_container_of_player_empty(playerId, harvestersByPlayer):
			var harvester = productionManager.get_production_instance("Harvester")
			add_unit(productionManager, harvester, "Harvester", building.deliveryPosition.global_position, playerId, playerColor)
	
	emit_signal("update_building_fog_of_war", playerId, tileToPlace)
	
func add_unit(productionManager : ProductionManager, unit : Unit, productionName : String, position : Vector2, playerId : int, playerColor : int) -> void:
	if productionName == "Harvester":
		register_instance_for_player(playerId, harvestersByPlayer, unit.get_instance_id())
	elif productionName == "MCV":
		register_instance_for_player(playerId, mcvsByPlayer, unit.get_instance_id())
	
	init_production(position, unit, playerId, playerColor)
	unit.connect("unit_destroyed", self, "unit_destroyed")
	unit.connect("request_closest_ore_tile_position", self, "request_closest_ore_tile_position")
	unit.connect("request_closest_ore_tile_position_cb", self, "request_closest_ore_tile_position_cb")
	unit.connect("request_closest_ore_refinery_position", self, "request_closest_ore_refinery_position")
	unit.connect("request_closest_ore_refinery_position_cb", self, "request_closest_ore_refinery_position_cb")
	unit.connect("gain_for_player", self, "gain_for_player")
	unit.connect("ore_tile_collected", self, "ore_tile_collected")
	unit.connect("become_visible", self, "unit_become_visible")
	unit.connect("become_unvisible", self, "unit_become_unvisible")
	unit.connect("update_fog_of_war", self, "update_fog_of_war")
	
	match unit.movableMaterial:
		ProductionRecord.MovableMaterial.GROUND:
			unit.tilemap = tilemap
			unit.navigation = ground_navigation
		ProductionRecord.MovableMaterial.WATER:
			unit.tilemap = water_tilemap
			unit.navigation = water_navigation
		ProductionRecord.MovableMaterial.HYBRID:
			unit.tilemap = hybrid_tilemap
			unit.navigation = hybrid_navigation
		ProductionRecord.MovableMaterial.AIR:
			unit.tilemap = air_tilemap
		_:
			unit.tilemap = tilemap
	
	add_child(unit)
	register_instance_for_player(playerId, unitsByPlayer, unit.get_instance_id())
	productionManager.productionPlaced(productionName)
	
func remove_unit(unit : Unit, productionName : String, playerId : int) -> void:
	if productionName == "Harvester":
		remove_instance_for_player(playerId, harvestersByPlayer, unit.get_instance_id())
	elif productionName == "MCV":
		remove_instance_for_player(playerId, mcvsByPlayer, unit.get_instance_id())
		
	remove_instance_for_player(playerId, unitsByPlayer, unit.get_instance_id())

func clear_building_place(position: Vector2, tiles : Array):
	var tile_position = tilemap.world_to_map(position)
	var ground_tile_index = tilemap.tile_set.find_tile_by_name(groundTileName)
	for tile in tiles:
		tilemap.set_cell(tile_position.x + tile.x, tile_position.y + tile.y, ground_tile_index)

func add_mcv(productionManager : ProductionManager, position: Vector2, playerId : int, playerColor : int):
	var mcv = productionManager.get_production_instance("MCV")
	add_unit(productionManager, mcv, "MCV", position, playerId, playerColor)


func init_production(position : Vector2, production, playerId : int, color : int) -> void:
	production.global_position = position
	production.ownerId = playerId
	production.color = color

func add_unit_to_world(unit : Unit, productionName : String, tileToPlace : Vector2, playerId : int, playerColor : int, productionManager : ProductionManager):
	add_unit(productionManager, unit, productionName, tilemap.map_to_world(tileToPlace) + tilemap.cell_size / 2, playerId, playerColor)
	
func expand_object(object_to_expand, playerId : int, playerColor : int, productionManager : ProductionManager):
	if not "expansionName" in object_to_expand:
		return
	
	var tilemap_position = tilemap.world_to_map(object_to_expand.global_position)
	var production = productionManager.get_production_instance(object_to_expand.expansionName)
	if not production:
		return
	
	if production is Building:
		add_building_to_world(production, object_to_expand.expansionName, tilemap_position, playerId, playerColor, productionManager)
	else:
		add_unit_to_world(production, object_to_expand.expansionName, tilemap_position, playerId, playerColor, productionManager)
	
	object_to_expand.set_selected(false)
	object_to_expand.emit_signal("unit_destroyed", object_to_expand.get_instance_id())
	object_to_expand.queue_free()

func register_instance_for_player(playerId: int, instanceContainer : Dictionary, instanceId: int) -> void:
	if not instanceContainer.has(playerId):
		instanceContainer[playerId] = []
	instanceContainer[playerId].push_back(instanceId)
	
func remove_instance_for_player(playerId: int, instanceContainer : Dictionary, instanceId: int) -> void:
	if not instanceContainer.has(playerId):
		return
	if instanceId in instanceContainer[playerId]:
		instanceContainer[playerId].erase(instanceId)

func is_instance_container_of_player_empty(playerId: int, instanceContainer : Dictionary) -> bool:
	if not instanceContainer.has(playerId):
		instanceContainer[playerId] = []
	return instanceContainer[playerId].empty()
	
func get_instance_container_of_player(playerId: int, instanceContainer : Dictionary) -> Array:
	if not instanceContainer.has(playerId):
		instanceContainer[playerId] = []
	return instanceContainer[playerId]
	
func get_units_of_player(playerId: int) -> Array:
	return get_instance_container_of_player(playerId, unitsByPlayer)
	
func get_buildings_of_player(playerId: int) -> Array:
	return get_instance_container_of_player(playerId, buildingsByPlayer)
	
func get_refineries_of_player(playerId: int) -> Array:
	return get_instance_container_of_player(playerId, refineriesByPlayer)
	
func get_harvesters_of_player(playerId: int) -> Array:
	return get_instance_container_of_player(playerId, harvestersByPlayer)

func get_mcvs_of_player(playerId: int) -> Array:
	return get_instance_container_of_player(playerId, mcvsByPlayer)

func unit_become_visible(unit : Unit, viewport : Viewport) -> void:
	emit_signal("unit_become_visible", unit, viewport)
	
func unit_become_unvisible(unit : Unit, viewport : Viewport) -> void:
	emit_signal("unit_become_unvisible", unit, viewport)

func update_fog_of_war(playerId : int, tile : Vector2, radius : float) -> void:
	emit_signal("update_fog_of_war", playerId, tile, radius)
