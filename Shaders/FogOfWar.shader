shader_type canvas_item;
render_mode blend_mix;

uniform vec4 selection_color;
uniform vec2 camera_position;
uniform vec2 cell_size;

void fragment(){
    vec4 color = texture(TEXTURE, UV);
//	color = vec4(selection_color.r,selection_color.g,selection_color.b,color.a);
//    COLOR = color;
	
	if (int(FRAGCOORD.x / cell_size.x) % 2 == 0 && int(FRAGCOORD.y / cell_size.y) % 2 != 0 ||
		int(FRAGCOORD.x / cell_size.x) % 2 != 0 && int(FRAGCOORD.y / cell_size.y) % 2 == 0)
		COLOR = vec4(0.0,0.0,0.0,1.0);
	else
		COLOR = vec4(selection_color.r,selection_color.g,selection_color.b,0.0);
}