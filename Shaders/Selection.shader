shader_type canvas_item;
render_mode blend_mix;

uniform vec4 selection_color;

void fragment(){
    vec4 color = texture(TEXTURE, UV);
	color = vec4(selection_color.r,selection_color.g,selection_color.b,color.a);
    COLOR = color;
}