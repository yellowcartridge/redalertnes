extends Node

enum MovementState {
	NoState,
	WaitForPath,
	FollowPath,
	GoalReached,
	IncreaseWaypoint
};

enum CollisionType {

	NoCollision,
	SameCell,
	TowardsCell,
	ItsCell,
	DiagonalCrossing
};

var unitGroups : Array = [] # contains all the existing groups

#class SingleUnit:
#	pass
#class UnitGroup:
#	pass


func construct() -> void:
	pass

# Destructor
func destruct() -> void:
	pass

func Update(dt : float) -> bool:
	return true

func DebugDraw() -> void:
	for group in unitGroups:
		for unit in group.units:

			if unit.movementState != MovementState.NoState and unit.movementState != MovementState.GoalReached:

				var col : Color = unit.unit.GetColor()

				if unit.movementState != MovementState.WaitForPath and unit.nextTile.x > -1 and unit.nextTile.y > -1:

					# Raycast a line between the unit and the nextTile
					Vector2 offset = { App->map->data.tile_width / 2, App->map->data.tile_height / 2 };
					Vector2 nextPos = App->map->MapToWorld((*unit)->nextTile.x, (*unit)->nextTile.y);
					App->render->DrawLine((*unit)->unit->GetPos().x + offset.x, (*unit)->unit->GetPos().y + offset.y, nextPos.x + offset.x, nextPos.y + offset.y, 255, 255, 255, 255);
					App->render->DrawCircle(nextPos.x + offset.x, nextPos.y + offset.y, 10, 255, 255, 255, 255);

					# Draw unit's path
					if (App->scene->debugDrawPath) {

						for (uint i = 0; i < (*unit)->path.size(); ++i)
						{
							iPoint pos = App->map->MapToWorld((*unit)->path.at(i).x, (*unit)->path.at(i).y);
							SDL_Rect rect = { pos.x, pos.y, App->map->data.tile_width, App->map->data.tile_height };
							App->render->DrawQuad(rect, col.r, col.g, col.b, 50);
						}
					}

				# Draw unit's goal
				iPoint pos = App->map->MapToWorld((*unit)->goal.x, (*unit)->goal.y);
				SDL_Rect rect = { pos.x, pos.y, App->map->data.tile_width, App->map->data.tile_height };
				App->render->DrawQuad(rect, col.r, col.g, col.b, 200);

# Called before quitting
func CleanUp() -> bool:
	var ret : bool = true

	list<UnitGroup*>::const_iterator it = unitGroups.begin();

	while not unitGroups.empty():
		var group : UnitGroup = unitGroups.pop_back()
		group.free()

	unitGroups.clear()

	return ret;

# Creates a group from a list of units. Returns the pointer to the group created or nullptr
func CreateGroupFromUnits(units : Array) -> UnitGroup:
	var group : UnitGroup = null

	for unit in units:
		# If a unit from the list belongs to an existing group, delete the unit from the group
		group = GetGroupByUnit(unit);

		if group != null:
			group.RemoveUnit(unit.GetSingleUnit())

			# If the group is empty, delete it
			if group.GetSize() == 0:
				RemoveGroup(group)

	group = UnitGroup.new()
	group.init(units)
	unitGroups.push_back(group)

	return group
	
# Creates a group from a single unit. Returns the pointer to the group created or nullptr
func CreateGroupFromUnit(unit) -> UnitGroup:
	# If a unit from the list belongs to an existing group, delete the unit from the group
	var group : UnitGroup = null
	group = GetGroupByUnit(unit);

	if group != null:
		group.RemoveUnit(unit.GetSingleUnit())

		# If the group is empty, delete it
		if group.GetSize() == 0:
			RemoveGroup(group)

	group = UnitGroup.new()
	group.init(units)
	unitGroups.push_back(group)

	return group
	
# Returns the last group created or null
func GetLastGroup() -> UnitGroup:
	if unitGroups.empty():
		return null
		
	return unitGroups[unitGroups.size() - 1]

# Returns an existing group by a pointer to one of its units or nullptr
func GetGroupByUnit(unit) -> UnitGroup:
	var group : UnitGroup = null

	for groups in unitGroups:
		for units in groups.units:
			if units.unit == unit:
				group = groups
				break

	return group

# Returns an existing group by a list to all of its units or nullptr
func GetGroupByUnits(units : Array) -> UnitGroup:
	var size : int = 0

	for groups in unitGroups:

		for singleUnits in groups.units:

			for unit in units:

				if singleUnits.unit == unit:
					size += 1

		if size == units.size() and size == groups.GetSize():
			return groups

		size = 0

	return null
	
func RemoveGroup(unitGroup : UnitGroup) -> bool:
	var ret : bool = false

	var groupIndex : int = unitGroups.find(unitGroup)
	
	if groupIndex >= 0:
		# Remove the group
		unitGroups[groupIndex].free()

		# Remove the group from the list of groups
		unitGroups.remove(groupIndex)

		ret = true

	return ret

# Moves a unit applying the principles of group movement. Returns the state of the unit's movement
## Call this method from a unit's update
func MoveUnit(unit, dt : float) -> int: #MovementState /// Not const because it needs to keep track of the number of paths created at the current update
	if GetGroupByUnit(unit) == null:
		return MovementState.NoState

	var singleUnit : SingleUnit = unit.GetSingleUnit()

	if singleUnit == null:
		return MovementState.NoState

	# ---------------------------------------------------------------------

	Vector2 nextPos = App->map->MapToWorld(singleUnit.nextTile.x, singleUnit.nextTile.y);
	Vector2 movePos
	Vector2 newGoal

	match singleUnit.movementState:

		MovementState.WaitForPath:

			# The unit is still
			singleUnit.unit.SetIsStill(true)

			# The goal of a unit cannot be the goal of another unit
			if not IsValidTile(singleUnit, singleUnit.goal, false, false, true):

				singleUnit.isGoalNeeded = true

			if singleUnit.isGoalNeeded:
			
				# Only one unit at a time can change its goal
				if not IsAnyUnitDoingSomething(singleUnit, true):
					print("ADD PATH SEARCH HERE!")
					#singleUnit.unit.GetPathPlanner().RequestDijkstra(singleUnit.goal, FindActiveTrigger::ActiveTriggerType_Goal)

					singleUnit.isSearching = true # The unit is changing its goal

			# ***IS THE TILE READY?***
			if singleUnit.isSearching:

				if singleUnit.unit.GetPathPlanner().IsSearchCompleted():

					singleUnit.goal = singleUnit.unit.GetPathPlanner().GetTile()
					singleUnit.unit.GetPathPlanner().SetSearchRequested(false)

					singleUnit.isSearching = false # The unit has finished changing its goal
					singleUnit.isGoalNeeded = false
				continue
			
			if not singleUnit.isGoalNeeded and not singleUnit.isSearching:

				# Request a new path for the unit
				print("REQUEST PATH SEARCH HERE!")
				#singleUnit.unit.GetPathPlanner().RequestAStar(singleUnit.currTile, singleUnit.goal)

				# ***IS THE PATH READY?***
				if singleUnit.unit.GetPathPlanner().IsSearchCompleted():

					singleUnit.path = singleUnit.unit.GetPathPlanner().GetPath()
					singleUnit.unit.GetPathPlanner().SetSearchRequested(false)

					singleUnit.movementState = MovementState.IncreaseWaypoint
				continue

		MovementState.FollowPath:

			# ---------------------------------------------------------------------
			# MOVEMENT CALCULATION
			# ---------------------------------------------------------------------

			# Calculate the difference between the nextPos and the current position of the unit and store the result inside the temporary iPoint called movePos
			var unit_position : Vector2 = singleUnit.unit.get_global_position()
			movePos = nextPos - unit_position

			# Normalize the movePos. It should be in the interval [-1,1]
			movePos = movePos.normalized()

			# Update the direction of the unit with the normalized movePos
			singleUnit.unit.SetUnitDirectionByValue(movePos)

			# Apply the speed and the dt to the movePos
			movePos *= singleUnit.unit.GetSpeed() * dt;

			# ---------------------------------------------------------------------
			# COLLISION CALCULATION
			# ---------------------------------------------------------------------

			CheckForFutureCollision(singleUnit)

			if singleUnit.coll != CollisionType.NoCollision:

				# The unit is still
				singleUnit.unit.SetIsStill(true)

				# waitUnit doesn't exist
				if singleUnit.waitUnit == null:
					# If waitUnit doesn't exist, there cannot be a collision...
					singleUnit->ResetUnitCollisionParameters()
					continue

				# waitUnit is dead
				# TODO: Check that this works fine and doesn't crash
				if singleUnit.waitUnit.unit.isRemove:
					singleUnit->waitUnit = null
					continue

				# ---------------------------------------------------------------------
				# SPECIAL CASES
				# ---------------------------------------------------------------------

				# a) The other unit is attacking and won't respond to any movement order
				if singleUnit.waitUnit.unit.IsHitting():
				
					# Current unit must react to the collision
					# Current unit moves
					if not singleUnit.isSearching:

						# 1. Check only tiles in front of the unit (3)
						var newTile : Vector2 = FindNewValidTile(singleUnit, true)

						if newTile.x != -1 and newTile.y != -1:

							# Request a new path for the unit
							singleUnit.unit.GetPathPlanner().RequestAStar(newTile, singleUnit.goal)
							singleUnit.unit.GetPathPlanner().SetCheckingCurrTile(true)

							singleUnit.isSearching = true # The unit is changing its nextTile

							continue
						else:

							# 2. Check all possible tiles (8)
							newTile = FindNewValidTile(singleUnit)

							if newTile.x != -1 and newTile.y != -1:

								# Request a new path for the unit
								singleUnit.unit.GetPathPlanner().RequestAStar(newTile, singleUnit.goal)
								singleUnit.unit.GetPathPlanner().SetCheckingCurrTile(true)

								singleUnitisSearching = true # The unit is changing its nextTile

								continue
							continue
						continue

					# IS THE UNIT REALLY CHANGING ITS NEXTTILE?
					if singleUnit.isSearching:

						# ***IS THE PATH READY?***
						if singleUnit.unit.GetPathPlanner().IsSearchCompleted():

							singleUnit.path = singleUnit.unit.GetPathPlanner().GetPath()
							singleUnit.unit.GetPathPlanner().SetSearchRequested(false)

							singleUnit.isSearching = false # The unit has finished changing its nextTile

							# Update the unit's nextTile
							singleUnit.nextTile = singleUnit.path.front()

							if singleUnit.unit.isSelected:
								print(singleUnit.waitUnit.unit.GetColorName(), " MOVED AWAY ", singleUnit.unit.GetColorName())

							# COLLISION RESOLVED
							singleUnit->ResetUnitCollisionParameters()

							continue
						continue
					else:

						# WARNING: THIS IS NOT BEING USED!!!
						# If the unit cannot change its nextTile, ask the waitUnit to move
						singleUnit.reversePriority = true

						if singleUnit.unit.isSelected:
							print(singleUnit->waitUnit->unit->GetColorName(), " reversed its priority")

						continue
					continue

				# b) The waitUnit is still on its goal
				if singleUnit.nextTile == singleUnit.waitUnit.goal and singleUnit.waitUnit.currTile == singleUnit.waitUnit.goal:

					# Wake up the unit (it may not be in the UnitState.Walk state)
					singleUnit.waitUnit.WakeUp()

					# The unit with the higher priority politely asks the other unit to move
					if singleUnit.unit.GetPriority() >= singleUnit.waitUnit.unit.GetPriority() or singleUnit.reversePriority:

						# 1. waitUnit moves
						singleUnit.unit.GetPathPlanner().RequestDijkstra(singleUnit.waitUnit.goal, FindActiveTrigger::ActiveTriggerType_Goal)
						singleUnit.unit.GetPathPlanner().SetCheckingCurrTile(true)
						singleUnit.unit.GetPathPlanner().SetCheckingNextTile(true) # TODO: trigger must be fully customizable
						singleUnit.isSearching = true

						# ***IS THE TILE READY?***
						if (singleUnit->unit->GetPathPlanner()->IsSearchCompleted()) {

							singleUnit->waitUnit->unit->GetBrain()->AddGoal_MoveToPosition(singleUnit->unit->GetPathPlanner()->GetTile());
							singleUnit->unit->GetPathPlanner()->SetSearchRequested(false);
							singleUnit->isSearching = false;

							LOG("DEL changed path waitUnit Still Goal %i, %i", singleUnit->unit->GetPathPlanner()->GetTile().x, singleUnit->unit->GetPathPlanner()->GetTile().y);

							if (singleUnit->unit->isSelected)
								LOG("%s: MOVED AWAY %s", singleUnit->unit->GetColorName().data(), singleUnit->waitUnit->unit->GetColorName().data());

							/// COLLISION RESOLVED
							singleUnit->ResetUnitCollisionParameters();
							continue
						}
						continue
					else {

						// 2. Current unit moves
						if (!singleUnit->isSearching) {

							// 1. Check only tiles in front of the unit (3)
							iPoint newTile = FindNewValidTile(singleUnit, true);

							if (newTile.x != -1 && newTile.y != -1) {

								// Request a new path for the unit
								singleUnit->unit->GetPathPlanner()->RequestAStar(newTile, singleUnit->goal);
								singleUnit->unit->GetPathPlanner()->SetCheckingCurrTile(true);

								singleUnit->isSearching = true; /// The unit is changing its nextTile
								LOG("DEL waitUnit Still Goal1 %i, %i", newTile.x, newTile.y);
								continue
							}
							else {

								// 2. Check all possible tiles (8)
								newTile = FindNewValidTile(singleUnit);

								if (newTile.x != -1 && newTile.y != -1) {

									// Request a new path for the unit
									singleUnit->unit->GetPathPlanner()->RequestAStar(newTile, singleUnit->goal);
									singleUnit->unit->GetPathPlanner()->SetCheckingCurrTile(true);

									singleUnit->isSearching = true; /// The unit is changing its nextTile
									LOG("DEL waitUnit Still Goal2 %i, %i", newTile.x, newTile.y);
									continue
								}
								continue
							}
							continue
						}

						// IS THE UNIT REALLY CHANGING ITS NEXTTILE?
						if (singleUnit->isSearching) {

							// ***IS THE PATH READY?***
							if (singleUnit->unit->GetPathPlanner()->IsSearchCompleted()) {

								singleUnit->path = singleUnit->unit->GetPathPlanner()->GetPath();
								singleUnit->waitUnit->unit->GetPathPlanner()->SetSearchRequested(false);

								singleUnit->isSearching = false;/// The unit has finished changing its nextTile

								// Update the unit's nextTile
								singleUnit->nextTile = singleUnit->path.front();

								if (singleUnit->unit->isSelected)
									LOG("%s: MOVED AWAY %s", singleUnit->waitUnit->unit->GetColorName().data(), singleUnit->unit->GetColorName().data());

								/// COLLISION RESOLVED
								singleUnit->ResetUnitCollisionParameters();

								continue
							}
							continue
						}
						else {

							// If the unit cannot change its nextTile, ask the waitUnit to move
							singleUnit->reversePriority = true;

							if (singleUnit->unit->isSelected)
								LOG("%s: reversed its priority", singleUnit->waitUnit->unit->GetColorName().data());

							continue
						}
						continue
					}
					continue

				# ---------------------------------------------------------------------
				# NORMAL CASES
				# ---------------------------------------------------------------------

				if (singleUnit->coll == CollisionType_ItsCell) {

					if (singleUnit->waitUnit->currTile != singleUnit->waitTile) {

						singleUnit->ResetUnitCollisionParameters();

						if (singleUnit->unit->isSelected)
							LOG("%s: RESOLVED ITS CELL", singleUnit->unit->GetColorName().data());

						break;
					}
					break;
				}
				else if (singleUnit->coll == CollisionType_SameCell) {

					if (singleUnit->waitUnit->nextTile != singleUnit->waitTile) {

						singleUnit->ResetUnitCollisionParameters();

						if (singleUnit->unit->isSelected)
							LOG("%s: RESOLVED SAME CELL", singleUnit->unit->GetColorName().data());

						break;
					}
					break;
				}
				else if (singleUnit->coll == CollisionType_DiagonalCrossing) {

					if (singleUnit->waitUnit->currTile == singleUnit->waitTile) {

						singleUnit->ResetUnitCollisionParameters();

						if (singleUnit->unit->isSelected)
							LOG("%s: RESOLVED CROSSING", singleUnit->unit->GetColorName().data());

						break;
					}
					break;
				}
				else if (singleUnit->coll == CollisionType_TowardsCell) {

					// Current unit moves
					if (!singleUnit->isSearching) {

						// 1. Check only tiles in front of the unit (3)
						iPoint newTile = FindNewValidTile(singleUnit, true);

						if (newTile.x != -1 && newTile.y != -1) {

							// Request a new path for the unit
							singleUnit->unit->GetPathPlanner()->RequestAStar(newTile, singleUnit->goal);
							singleUnit->unit->GetPathPlanner()->SetCheckingCurrTile(true);

							singleUnit->isSearching = true; /// The unit is changing its nextTile
							LOG("DEL towards1 %i, %i", newTile.x, newTile.y);
							break;
						}
						else {

							// 2. Check all possible tiles (8)
							newTile = FindNewValidTile(singleUnit);

							if (newTile.x != -1 && newTile.y != -1) {

								// Request a new path for the unit
								singleUnit->unit->GetPathPlanner()->RequestAStar(newTile, singleUnit->goal);
								singleUnit->unit->GetPathPlanner()->SetCheckingCurrTile(true);

								singleUnit->isSearching = true; /// The unit is changing its nextTile
								LOG("DEL towards2 %i, %i", newTile.x, newTile.y);
								break;
							}
							break;
						}
						break;
					}

					// IS THE UNIT REALLY CHANGING ITS NEXTTILE?
					if (singleUnit->isSearching) {

						// ***IS THE PATH READY?***
						if (singleUnit->unit->GetPathPlanner()->IsSearchCompleted()) {

							singleUnit->path = singleUnit->unit->GetPathPlanner()->GetPath();
							singleUnit->waitUnit->unit->GetPathPlanner()->SetSearchRequested(false);

							singleUnit->isSearching = false;/// The unit has finished changing its nextTile

							// Update the unit's nextTile
							singleUnit->nextTile = singleUnit->path.front();

							if (singleUnit->unit->isSelected)
								LOG("%s: MOVED AWAY TOWARDS %s", singleUnit->waitUnit->unit->GetColorName().data(), singleUnit->unit->GetColorName().data());

							/// COLLISION RESOLVED
							singleUnit->waitUnit->wait = false;
							singleUnit->ResetUnitCollisionParameters();

							break;
						}
						break;
					}
					else {

						// WARNING: THIS IS NOT BEING USED!!!
						// If the unit cannot change its nextTile, change the collision with the waitUnit
						singleUnit->reversePriority = true;

						if (singleUnit->unit->isSelected)
							LOG("%s: reversed its priority TOWARDS", singleUnit->waitUnit->unit->GetColorName().data());
					}
					break;
				}
				break;

			if (singleUnit->wait) {

				// The unit is still
				singleUnit->unit->SetIsStill(true);
				break;
			}

			# ---------------------------------------------------------------------
			# TILE FITTING
			# ---------------------------------------------------------------------

			{
				# Predict where the unit will be after moving and store the result it inside the temporary fPoint called endPos
				fPoint endPos = { singleUnit->unit->GetPos().x + movePos.x, singleUnit->unit->GetPos().y + movePos.y };

				# Check if the unit would reach the nextTile during this move. If the answer is yes, then:
				if (singleUnit->IsTileReached(nextPos, endPos)) {

					# Update the unit's position with the nextPos
					singleUnit->unit->SetPos({ (float)nextPos.x, (float)nextPos.y });

					# Update the unit's currTile with the nextTile
					singleUnit->currTile = singleUnit->nextTile;

					# Set the unit's movement state to IncreaseWaypoint
					singleUnit->movementState = MovementState_IncreaseWaypoint;

					break;
				}
			}

			singleUnit->unit->SetIsStill(false);

			# Add the movePos to the unit's current position
			singleUnit->unit->AddToPos(movePos);

			break;

		MovementState.IncreaseWaypoint:
			# If the unit's path contains waypoints:
			if singleUnit.path.size() > 0:
				# Get the next waypoint to head to
				singleUnit.nextTile = singleUnit.path.pop_front()

				singleUnit.movementState = MovementState.FollowPath
			else:
				# If the unit's path is out of waypoints, it means that the unit has reached the goal
				singleUnit.movementState = MovementState.GoalReached

		MovementState.GoalReached:
			if singleUnit.group.isShapedGoal:

				if singleUnit.goal != singleUnit.shapedGoal:

					singleUnit.goal = singleUnit.shapedGoal
					singleUnit.isGoalChanged = true

			# The unit is still
			singleUnit.unit.SetIsStill(true)

		MovementState.NoState:
			pass
			
		_:
			# The unit is still
			singleUnit.unit.SetIsStill(true)

	return singleUnit.movementState

# -----

# Checks for future collisions between the current unit and the rest of the units
## Sets the collision of the unit with the type of collision found or CollisionType_NoCollision
func CheckForFutureCollision(singleUnit : SingleUnit) -> void:
	# We don't check the walkability of the tile since the A* algorithm already did it for us
	for groups in unitGroups:
		for units in groups.units:

			if units != singleUnit:

				# TOWARDS COLLISION
				# Check if two units would reach the tile of each other. Make sure that the collision would be frontal!
				# The unit with the lower priority must deal with the collision

				if singleUnit.nextTile == units.currTile and units.nextTile == singleUnit.currTile:

					if IsOppositeDirection(singleUnit, units):

						if singleUnit.unit.GetPriority() >= units.unit.GetPriority():

							if singleUnit.coll == CollisionType.TowardsCell and singleUnit.waitUnit == units:
								break

							elif units.coll != CollisionType.TowardsCell:
								units.SetCollisionParameters(CollisionType.TowardsCell, singleUnit, units.nextTile)
								singleUnit.wait = true
								print(units.unit.GetColorName(), " TOWARDS with ", singleUnit.unit.GetColorName())
						else:

							if units.coll == CollisionType.TowardsCell and units.waitUnit == singleUnit:
								break

							elif singleUnit.coll != CollisionType.TowardsCell and units.waitUnit != singleUnit:
								singleUnit.SetCollisionParameters(CollisionType.TowardsCell, units, singleUnit.nextTile)
								units.wait = true
								print(singleUnit->unit->GetColorName(), ": TOWARDS with ", units.unit.GetColorName())

				if singleUnit.coll != CollisionType.TowardsCell:

					# ITS CELL. A reaches B's tile
					# Check if the unit would reach another unit's tile
					## The unit who would reach another unit's tile must deal with the collision

					if singleUnit.nextTile == units.currTile:
						
						singleUnit.waitUnit = units
						singleUnit.waitTile = singleUnit.nextTile

						if not singleUnit.wait:
							singleUnit.coll = CollisionType.ItsCell
							singleUnit.wait = true

							if singleUnit.unit.isSelected:
								print(singleUnit.unit.GetColorName(), ": ITS CELL with ", units.unit.GetColorName())

					# SAME CELL. A and B reach the same tile
					# Check if two units would reach the same tile
					## The unit with the highest area inside the nextTile must deal with the collision

					elif singleUnit.nextTile == units.nextTile and not units.wait:

						singleUnit.waitUnit = units
						singleUnit.waitTile = singleUnit.nextTile

						# The agent that is already inside the tile has the priority. If none of them are, choose one randomly
						var posA : Rect2 = Rect2( (int)singleUnit.unit.GetPos().x, (int)singleUnit->unit->GetPos().y, App->map->data.tile_width, App->map->data.tile_height )
						var posB : Rect2 = Rect2( (int)units.unit.GetPos().x, (int)(*units)->unit->GetPos().y,  App->map->data.tile_width, App->map->data.tile_height )
						var tilePos : Vector2 = App->map->MapToWorld(singleUnit->nextTile.x, singleUnit->nextTile.y);
						var tile : Rect2 = Rect2( tilePos.x, tilePos.y, App->map->data.tile_width, App->map->data.tile_height )
						var resultA : Rect2, resultB : Rect2

						resultA = posA.clip(tile)
						resultB = posB.clip(tile)
						# Compare the areas of the two intersections
						var areaA : int = resultA.w * resultA.h;
						var areaB : int = resultB.w * resultB.h;

						# Decide which unit waits
						# The unit who has the smaller area waits
						if areaA <= areaB:
							if not singleUnit.wait:
								singleUnit.coll = CollisionType.SameCell
								singleUnit.wait = true

								if singleUnit.unit.isSelected:
									print(singleUnit.unit.GetColorName(), ": SAME CELL with ", units.unit.GetColorName())
							
						else:
							units.coll = CollisionType.SameCell
							units.wait = true

							if units.unit.isSelected:
								print(units.unit.GetColorName(), ": SAME CELL with ", singleUnit.unit.GetColorName())
						

					# DIAGONAL CROSSING

					else {

						iPoint up = { (*units)->currTile.x, (*units)->currTile.y - 1 };
						iPoint down = { (*units)->currTile.x, (*units)->currTile.y + 1 };
						iPoint left = { (*units)->currTile.x - 1, (*units)->currTile.y };
						iPoint right = { (*units)->currTile.x + 1, (*units)->currTile.y };

						iPoint myUp = { singleUnit->currTile.x, singleUnit->currTile.y - 1 };
						iPoint myDown = { singleUnit->currTile.x, singleUnit->currTile.y + 1 };
						iPoint myLeft = { singleUnit->currTile.x - 1, singleUnit->currTile.y };
						iPoint myRight = { singleUnit->currTile.x + 1, singleUnit->currTile.y };

						// The agent that is already inside the tile has the priority. If none of them are, choose one randomly
						SDL_Rect posA = { (int)singleUnit->unit->GetPos().x, (int)singleUnit->unit->GetPos().y, App->map->data.tile_width, App->map->data.tile_height };
						SDL_Rect posB = { (int)(*units)->unit->GetPos().x, (int)(*units)->unit->GetPos().y,  App->map->data.tile_width, App->map->data.tile_height };

						iPoint tilePosA = App->map->MapToWorld(singleUnit->nextTile.x, singleUnit->nextTile.y);
						iPoint tilePosB = App->map->MapToWorld((*units)->nextTile.x, (*units)->nextTile.y);
						SDL_Rect tileA = { tilePosA.x, tilePosA.y, App->map->data.tile_width, App->map->data.tile_height };
						SDL_Rect tileB = { tilePosB.x, tilePosB.y, App->map->data.tile_width, App->map->data.tile_height };
						SDL_Rect resultA, resultB;

						SDL_IntersectRect(&posA, &tileA, &resultA);
						SDL_IntersectRect(&posB, &tileB, &resultB);

						// Compare the areas of the two intersections
						int areaA = resultA.w * resultA.h;
						int areaB = resultB.w * resultB.h;

						if (singleUnit->nextTile == up) {

							// We are sure than nextTile of this unit is valid, but what about the other unit's nextTile? We haven't check it yet
							if ((*units)->nextTile == myUp) {

								// Decide which unit waits
								// The unit who has the smaller area waits
								if (areaA <= areaB) {

									if (!(*units)->wait && IsValidTile(nullptr, (*units)->nextTile, true)) {

										singleUnit->SetCollisionParameters(CollisionType_DiagonalCrossing, *units, myUp);
										LOG("%s: Up CROSSING", singleUnit->unit->GetColorName().data());
									}
								}
								else {

									if (!singleUnit->wait && IsValidTile(nullptr, singleUnit->nextTile, true)) {

										(*units)->SetCollisionParameters(CollisionType_DiagonalCrossing, singleUnit, up);
										LOG("%s: Up CROSSING", (*units)->unit->GetColorName().data());
									}
								}
							}
						}
						else if (singleUnit->nextTile == down) {

							// We are sure than nextTile of this unit is valid, but what about the other unit's nextTile? We haven't check it yet
							if ((*units)->nextTile == myDown) {
							
								// Decide which unit waits
								// The unit who has the smaller area waits
								if (areaA <= areaB) {

									if (!(*units)->wait && IsValidTile(nullptr, (*units)->nextTile, true)) {

										singleUnit->SetCollisionParameters(CollisionType_DiagonalCrossing, *units, myDown);
										LOG("%s: Down CROSSING", singleUnit->unit->GetColorName().data());
									}
								}
								else {

									if (!singleUnit->wait && IsValidTile(nullptr, singleUnit->nextTile, true)) {

										(*units)->SetCollisionParameters(CollisionType_DiagonalCrossing, singleUnit, down);
										LOG("%s: Down CROSSING", (*units)->unit->GetColorName().data());
									}
								}
							}
						}
						else if (singleUnit->nextTile == left) {

							// We are sure than nextTile of this unit is valid, but what about the other unit's nextTile? We haven't check it yet
							if ((*units)->nextTile == myLeft) {
							
								// Decide which unit waits
								// The unit who has the smaller area waits
								if (areaA <= areaB) {

									if (!(*units)->wait && IsValidTile(nullptr, (*units)->nextTile, true)) {

										singleUnit->SetCollisionParameters(CollisionType_DiagonalCrossing, *units, myLeft);
										LOG("%s: Left CROSSING", singleUnit->unit->GetColorName().data());
									}
								}
								else {

									if (!singleUnit->wait && IsValidTile(nullptr, singleUnit->nextTile, true)) {

										(*units)->SetCollisionParameters(CollisionType_DiagonalCrossing, singleUnit, left);
										LOG("%s: Left CROSSING", (*units)->unit->GetColorName().data());
									}
								}
							} 
						}
						else if (singleUnit->nextTile == right) {

							// We are sure than nextTile of this unit is valid, but what about the other unit's nextTile? We haven't check it yet
							if ((*units)->nextTile == myRight) {
							
								// Decide which unit waits
								// The unit who has the smaller area waits
								if (areaA <= areaB) {

									if (!(*units)->wait && IsValidTile(nullptr, (*units)->nextTile, true)) {

										singleUnit->SetCollisionParameters(CollisionType_DiagonalCrossing, *units, myRight);
										LOG("%s: Right CROSSING", singleUnit->unit->GetColorName().data());
									}
								}
								else {

									if (!singleUnit->wait && IsValidTile(nullptr, singleUnit->nextTile, true)) {

										(*units)->SetCollisionParameters(CollisionType_DiagonalCrossing, singleUnit, right);
										LOG("%s: Right CROSSING", (*units)->unit->GetColorName().data());
									}
								}
							} 
						}
					}


# Returns true if a tile is valid
## Checks for all the units (except for the unit passed as an argument) if a unit's tile (currTile, nextTile or goalTile) matches the tile passed as an argument
func IsValidTile(SingleUnit* singleUnit, iPoint tile, bool currTile = false, bool nextTile = false, bool goalTile = false) -> bool:

# Returns a new valid tile for the unit or { -1,-1 }
## If checkOnlyFront is true, it only checks the three tiles that are in front of the unit passed as an argument
## If checkOnlyFront is false, it checks the eight tiles surrounding the unit passed as an argument
## The new tile is searched using a Priority Queue containing the neighbors of the current tile of the unit passed as an argument
func FindNewValidTile(singleUnit : SingleUnit, checkOnlyFront : bool = false) -> Vector2:

# Returns true if two units are heading towards opposite directions
func IsOppositeDirection(singleUnitA : SingleUnit, singleUnitB : SingleUnit) -> bool:

# Returns true if another unit has any of the booleans passed as arguments to true
func IsAnyUnitDoingSomething(singleUnit : SingleUnit, isSearching : bool = false) -> bool:
	for groups in unitGroups:
		for units int groups.units:
			if units != singleUnit:
				if isSearching:
					if units.isSearching:
						return true

	return false
	
func IsNeighborTile(iPoint tile, iPoint neighbor) -> bool:


// ---------------------------------------------------------------------
// UnitGroup: struct representing a group of units
// ---------------------------------------------------------------------

struct UnitGroup
{
	UnitGroup(DynamicEntity* unit);

	UnitGroup(list<DynamicEntity*> units);

	~UnitGroup();

	// Adds a singleUnit (unit) to the group. Returns false if the singleUnit was already in the group
	bool AddUnit(SingleUnit* singleUnit);

	// Removes a singleUnit (unit) from the group. Returns true if success
	bool RemoveUnit(SingleUnit* singleUnit);

	// Returns true if the singleUnit (unit) is in the group
	bool IsUnitInGroup(SingleUnit* singleUnit) const;

	// Returns the size of the group (the number of singleUnits in the group)
	uint GetSize() const;

	// Sets the destination tile (goal) of the group
	bool SetGoal(iPoint goal);

	// Returns the destination tile (goal) of the group
	iPoint GetGoal() const;

	bool DrawShapedGoal(iPoint mouseTile);

	bool SetShapedGoal();

	// -----

	list<SingleUnit*> units; // contains all the units of a given group
	iPoint goal = { -1,-1 }; // current goal of the group

	vector<iPoint> shapedGoal;
	bool isShapedGoal = false;
};

// ---------------------------------------------------------------------
// SingleUnit: struct representing a single unit
// ---------------------------------------------------------------------

struct SingleUnit
{
	SingleUnit(DynamicEntity* entity, UnitGroup* group);

	~SingleUnit();

	// Returns true if the unit would reach its next tile during this move
	/// nextPos is the next tile that the unit is heading to
	/// endPos is the tile that the unit would reach during this move
	bool IsTileReached(iPoint nextPos, fPoint endPos) const;

	// Resets the parameters of the unit (general info)
	void ResetUnitParameters();

	// Resets the collision parameters of the unit
	void ResetUnitCollisionParameters();

	// When detected a collision, to set the collision parameters of the unit
	void SetCollisionParameters(CollisionType collisionType, SingleUnit* waitUnit, iPoint waitTile);

	// Prepares the unit for its next movement cycle
	void GetReadyForNewMove();

	// Sets the state of the unit to UnitState_Walk
	void WakeUp();

	bool IsFittingTile() const;

	void SetGoal(iPoint goal);

	# -----

	DynamicEntity* unit = nullptr;
	UnitGroup* group = nullptr;
	MovementState movementState = MovementState_NoState;
	bool wakeUp = false; // sets a unit's unitState to UnitState_Walk

	vector<iPoint> path; // path to the unit's goal
	iPoint currTile = { -1,-1 }; // position of the unit in map coords
	iPoint nextTile = { -1,-1 }; // next waypoint of the path (next tile the unit is heading to in map coords)

	iPoint goal = { -1,-1 }; // goal of the unit
	iPoint shapedGoal = { -1,-1 }; // shaped goal of the unit

	/// Changed goals
	bool isGoalChanged = false; // if true, it means that the goal has been changed

	/// Goal planning
	bool isGoalNeeded = false; // if true, it means that the unit needs to search for a new goal using Dijkstra
	bool isSearching = false; // if true, it means that the unit is searching a tile using Dijkstra

	bool reversePriority = false; // if true, the priority of the unit is not taken into account

	// COLLISION AVOIDANCE
	bool wait = false;
	/// If a unit is not in the UnitState_Walk and another unit needs this unit to move away, set wakeUp to true
	iPoint waitTile = { -1,-1 }; // conflict tile (tile where the collision has been found)
	SingleUnit* waitUnit = nullptr; // conflict unit (unit whom the collision has been found with)
	CollisionType coll = CollisionType_NoCollision; // type of collision
};

// ---------------------------------------------------------------------
// Helper class to establish a priority to an iPoint
// ---------------------------------------------------------------------
class iPointPriority
{
public:
	iPointPriority() {}
	iPointPriority(iPoint point, int priority) :point(point), priority(priority) {}
	iPointPriority(const iPointPriority& i)
	{
		point = i.point;
		priority = i.priority;
	}

	iPoint point = { 0,0 };
	uint priority = 0;
};

// ---------------------------------------------------------------------
// Helper class to compare two iPoints by its priority values
// ---------------------------------------------------------------------
class Comparator
{
public:
	int operator() (const iPointPriority a, const iPointPriority b)
	{
		return a.priority > b.priority;
	}
};

#endif //__j1MOVEMENT_H__
