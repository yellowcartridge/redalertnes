extends GoalEvaluator
class_name AttackTargetEvaluator


func _init(bias : float).(bias):
	pass
  
#------------------ CalculateDesirability ------------------------------------
# 
#   returns a value between 0 and 1 that indicates the Rating of a bot (the
#   higher the score, the stronger the bot).
#-----------------------------------------------------------------------------
func CalculateDesirability(agent) -> float:
	var Desirability : float = 0.0;

	# only do the calculation if there is a target present
	if agent.targetSystem.isTargetPresent():
		var Tweaker : float = 1.0
		Desirability = Tweaker * Feature.Health(agent) * Feature.TotalWeaponStrength(agent)

		# bias the value according to the personality of the bot
		Desirability *= characterBias
	
	return Desirability


#----------------------------- SetGoal ---------------------------------------
#-----------------------------------------------------------------------------
func SetGoal(agent) -> void:
	agent.brain.AddGoal_AttackTarget()

func GetInfo(agent) -> String:
	return String("AttackTarget: ") + String(CalculateDesirability(agent))
