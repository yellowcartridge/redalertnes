extends Reference
class_name Feature

#-----------------------------------------------------------------------------
#           Class that implements methods to extract feature specific
#           information from the Raven game world and present it as 
#          a value in the range 0 to 1
#-----------------------------------------------------------------------------


# returns a value between 0 and 1 based on the bot's closeness to the 
# given item. the further the item, the higher the rating. If there is no
# item of the given type present in the game world at the time this method
# at called the value returned is 1
#-----------------------------------------------------------------------------
static func DistanceToItem(agent, ItemType : int) -> float:
	# determine the distance to the closest instance of the item type
	var DistanceToItem : float = agent.pathPlanner.GetCostToClosestItem(ItemType);

	# if the previous method returns a negative value then there is no item of
	# the specified type present in the game world at this time.
	if DistanceToItem < 0:
		return 1.0

	# these values represent cutoffs. Any distance over MaxDistance results in
	# a value of 0, and value below MinDistance results in a value of 1
	var MaxDistance : float = 500.0
	var MinDistance : float = 50.0

	DistanceToItem = clamp(DistanceToItem, MinDistance, MaxDistance)

	return DistanceToItem / MaxDistance


#----------------------- GetMaxRoundsBotCanCarryForWeapon --------------------
#
#  helper function to tidy up IndividualWeapon method
#  returns the maximum rounds of ammo a bot can carry for the given weapon
#-----------------------------------------------------------------------------
static func GetMaxRoundsBotCanCarryForWeapon(WeaponType : int) -> int:
	match WeaponType:
		Weapon.WeaponType.RailGun:
			return RailGun.Params.MaxRoundsCarried
		
		Weapon.WeaponType.RocketLauncher:
			return RocketLauncher.Params.MaxRoundsCarried
		
		Weapon.WeaponType.Shotgun:
			return Shotgun.Params.MaxRoundsCarried
		
		_:
			print("trying to calculate  of unknown weapon")
			return 0


#----------------------- IndividualWeaponStrength ----------------------
# returns a value between 0 and 1 based on how much ammo the bot has for
# the given weapon, and the maximum amount of ammo the bot can carry. The
# closer the amount carried is to the max amount, the higher the score
#-----------------------------------------------------------------------------
static func IndividualWeaponStrength(agent, WeaponType : int) -> float:
	# grab a pointer to the gun (if the bot owns an instance)
	var wp = agent.weaponSys.GetWeaponFromInventory(WeaponType)

	if wp:
		return wp.NumRoundsRemaining() / GetMaxRoundsBotCanCarryForWeapon(WeaponType)
	else:
		return 0.0
 

#--------------------- TotalWeaponStrength --------------
# returns a value between 0 and 1 based on the total amount of ammo the
# totals carrying each of the weapons. Each of the three weapons a bot can
# pick up can contribute a third to the score. In other words, if a bot
# is carrying a RL and a RG and has max ammo for the RG but only half max
# for the RL the rating will be 1/3 + 1/6 + 0 = 0.5
#-----------------------------------------------------------------------------
static func TotalWeaponStrength(agent) -> float:
	var MaxRoundsForShotgun : float = GetMaxRoundsBotCanCarryForWeapon(Weapon.WeaponType.Shotgun)
	var MaxRoundsForRailgun : float = GetMaxRoundsBotCanCarryForWeapon(Weapon.WeaponType.RailGun)
	var MaxRoundsForRocketLauncher : float = GetMaxRoundsBotCanCarryForWeapon(Weapon.WeaponType.RocketLauncher)
	var TotalRoundsCarryable : float = MaxRoundsForShotgun + MaxRoundsForRailgun + MaxRoundsForRocketLauncher

	var NumSlugs : float      = agent.weaponSys.GetAmmoRemainingForWeapon(Weapon.WeaponType.RailGun)
	var NumCartridges : float = agent.weaponSys.GetAmmoRemainingForWeapon(Weapon.WeaponType.Shotgun)
	var NumRockets : float    = agent.weaponSys.GetAmmoRemainingForWeapon(Weapon.WeaponType.RocketLauncher)

	# the value of the tweaker (must be in the range 0-1) indicates how much
	# desirability value is returned even if a bot has not picked up any weapons.
	# (it basically adds in an amount for a bot's persistent weapon -- the blaster)
	var Tweaker : float = 0.1

	return Tweaker + (1-Tweaker)*(NumSlugs + NumCartridges + NumRockets)/(MaxRoundsForShotgun + MaxRoundsForRailgun + MaxRoundsForRocketLauncher)


#------------------------------- HealthScore ---------------------------------
#
# returns a value between 0 and 1 based on the bot's health. The better
# the health, the higher the rating
#-----------------------------------------------------------------------------
static func Health(agent) -> float:
	return agent.health / agent.maxHealth
