extends Reference
class_name GoalEvaluator

#-----------------------------------------------------------------------------
#   class template that defines an interface for objects that are
#   able to evaluate the desirability of a specific strategy level goal
#-----------------------------------------------------------------------------

# when the desirability score for a goal has been evaluated it is multiplied 
# by this value. It can be used to create bots with preferences based upon
# their personality
var characterBias : float = 0.0

func _init(bias : float):
	characterBias = bias

# returns a score between 0 and 1 representing the desirability of the
# strategy the concrete subclass represents
func CalculateDesirability(agent) -> float:
	return 0.0
  
# adds the appropriate goal to the given bot's brain
func SetGoal(agent) -> void:
	pass

func GetInfo(agent) -> String:
	return String(CalculateDesirability(agent))
