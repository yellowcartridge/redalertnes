extends GoalEvaluator
class_name ExploreGoalEvaluator


func _init(bias : float).(bias):
	pass
	
#---------------- CalculateDesirability -------------------------------------
#-----------------------------------------------------------------------------
func CalculateDesirability(agent) -> float:
	var Desirability : float = 0.05;

	Desirability *= characterBias

	return Desirability;


#----------------------------- SetGoal ---------------------------------------
#-----------------------------------------------------------------------------
func SetGoal(agent) -> void:
	if agent.patrolPath.empty():
		agent.brain.AddGoal_Explore()
	else:
		agent.brain.AddGoal_PatrolPath(agent.patrolPath)


func GetInfo(agent) -> String:
	return String("ExploreGoal: ") + String(CalculateDesirability(agent))
