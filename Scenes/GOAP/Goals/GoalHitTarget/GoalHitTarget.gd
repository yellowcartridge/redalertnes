extends Goal
class_name GoalHitTarget

func _init(bot_agent).(bot_agent, GoalType.HitTarget):
	pass

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
		



#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	# it is possible for a bot's target to die whilst this goal is active so we
	# must test to make sure the bot always has an active target
	if agent.attackTargetId == -1:
		status = GoalStatus.Completed
	elif not instance_from_id(agent.attackTargetId):
		agent.attackTargetId = -1
		status = GoalStatus.Completed
	elif not agent.isAttackSatisfied:
		status = GoalStatus.Failed
	else:
		agent.attack_action()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
