extends GoalComposite
class_name GoalThink

#-----------------------------------------------------------------------------
#   class to arbitrate between a collection of high level goals, and
#   to process those goals.
#-----------------------------------------------------------------------------

var evaluators : Array = []

func _init(bot_agent).(bot_agent, GoalType.Think):
	pass

#func _notification(what : int) -> void:
#	if what == NOTIFICATION_PREDELETE:
#		#for evaluator in evaluators:
#		#	evaluator.free()
#		evaluators.clear()

# this method iterates through each goal evaluator and selects the one
# that has the highest score as the current goal
#func Arbitrate() -> void:
#	var best : float = 0.0
#	var MostDesirable = null
#
#	# iterate through all the evaluators to see which produces the highest score
#	for curDes in evaluators:
#		var desirabilty : float = curDes.CalculateDesirability(agent)
#
#		if desirabilty >= best:
#			best = desirabilty
#			MostDesirable = curDes
#
#	if not MostDesirable:
#		print("<GoalThink::Arbitrate>: no evaluator selected")
#		return
#
#	MostDesirable.SetGoal(agent)

# returns true if the given goal is not at the front of the subgoal list
func notPresent(GoalType : int) -> bool:
	if not subgoals.empty():
		return subgoals.front().GetType() != GoalType
	
	return true

func isIdle() -> bool:
	return subgoals.empty()

# the usual suspects
func Process() -> int:
	_activateIfInactive()
	
	var SubgoalStatus : int = ProcessSubgoals()
	
	if SubgoalStatus == GoalStatus.Completed or SubgoalStatus == GoalStatus.Failed:
#		if not agent.isPossessed():
			status = GoalStatus.Inactive
			
	return status

func Activate() -> void:
#	if not agent.isPossessed():
#		Arbitrate()
	
	status = GoalStatus.Active
	
func Terminate() -> void:
	pass
  
# top level goal types
func AddGoal_MoveToPosition(pos : Vector2) -> void:
	AddSubgoal(GoalMoveToPosition.new(agent, pos))
		
func AddGoal_PatrolPath(path : PoolVector2Array) -> void:
	if notPresent(GoalType.PatrolPath):
		RemoveAllSubgoals()
		AddSubgoal(GoalPatrolPath.new(agent, path))
	
func AddGoal_AttackTarget() -> void:
	if notPresent(GoalType.AttackTarget):
		RemoveAllSubgoals()
		AddSubgoal(GoalAttackTarget.new(agent))
		
func AddGoal_HarvestOre(ore_tile : Vector2) -> void:
	if notPresent(GoalType.Harvest):
		RemoveAllSubgoals()
		AddSubgoal(GoalHarvest.new(agent, ore_tile))
	
func AddGoal_DeliveryOre() -> void:
	pass
	
func AddGoal_Mining() -> void:
	if notPresent(GoalType.Mining):
		RemoveAllSubgoals()
		AddSubgoal(GoalMining.new(agent))
	
func AddGoal_Escape() -> void:
	if notPresent(GoalType.Escape):
		RemoveAllSubgoals()
		AddSubgoal(GoalEscape.new(agent))
		
func RemoveGoal_Escape() -> void:
	if not notPresent(GoalType.Escape):
		RemoveAllSubgoals()
		#RemoveSubgoals(GoalType.Escape)
	
func AddGoal_MoveAway() -> void:
	pass

# this adds the MoveToPosition goal to the *back* of the subgoal list.
func QueueGoal_MoveToPosition(pos : Vector2) -> void:
	subgoals.push_back(GoalMoveToPosition.new(agent, pos))

func GetEvaluations() -> String:
	var text : String = "Evaluations:\n"
	for evaluator in evaluators:
		text += evaluator.GetInfo(agent) + "\n"
	return text
