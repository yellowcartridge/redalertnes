extends GoalComposite
class_name GoalMining

var selectingGoal : bool = false

func _init(bot_agent).(bot_agent, GoalType.Mining):
	pass

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active

	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()
	
	selectGoal()


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	if subgoals.empty() and not selectingGoal:
		selectGoal()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	_reactivateIfFailed()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
	
func selectGoal() -> void:
	if not subgoals.empty() or selectingGoal:
		return
		
	selectingGoal = true
		
	if agent.has_ore:
		agent.find_refinery(funcref(self, "refineryFound"), funcref(self, "noRefineryAvailable"))
	else:
		agent.find_ore(funcref(self, "oreTileFound"), funcref(self, "noOreAvailable"))
		
func AddHarvestGoal(ore_tile : Vector2) -> void:
	if agent.has_ore:
		return

	AddSubgoal(GoalHarvest.new(agent, ore_tile))
	
func AddDeliveryGoal(refinery_position : Vector2) -> void:
	if not agent.has_ore:
		return

	AddSubgoal(GoalDelivery.new(agent, refinery_position))

func oreTileFound(ore_tile : Vector2) -> void:
	AddHarvestGoal(ore_tile)
	selectingGoal = false
	
func noOreAvailable() -> void:
	selectingGoal = false
	status = GoalStatus.Failed
	
func refineryFound(refinery_position : Vector2) -> void:
	AddDeliveryGoal(refinery_position)
	selectingGoal = false
	
func noRefineryAvailable() -> void:
	selectingGoal = false
	status = GoalStatus.Failed
