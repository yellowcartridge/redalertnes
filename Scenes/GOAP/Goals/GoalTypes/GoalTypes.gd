enum GoalType {
	Think
	Explore
	ArriveAtPosition
	SeekToPosition
	FollowPath
	TraverseEdge
	MoveToPosition
	GetHealth
	GetShotgun
	GetRocketLauncher
	GetRailgun
	Wander
	NegotiateDoor
	AttackTarget
	HuntTarget
	Strafe
	AdjustRange
	SayPhrase
}

func ConvertToString(gt : int) -> String:
	match gt:
		GoalType.Explore:
			return "Explore";
		
		GoalType.Think:
			return "Think";
		
		GoalType.ArriveAtPosition:
			return "ArriveAtPosition";
		
		GoalType.SeekToPosition:
			return "SeekToPosition";
		
		GoalType.FollowPath:
			return "FollowPath";
		
		GoalType.TraverseEdge:
			return "TraverseEdge";
		
		GoalType.MoveToPosition:
			return "MoveToPosition";
		
		GoalType.GetHealth:
			return "GetHealth";
		
		GoalType.GetShotgun:
			return "GetShotgun";
		
		GoalType.GetRailgun:
			return "GetRailgun";
		
		GoalType.GetRocketLauncher:
			return "GetRocketLauncher";
		
		GoalType.Wander:
			return "Wander";
		
		GoalType.NegotiateDoor:
			return "NegotiateDoor";
		
		GoalType.AttackTarget:
			return "AttackTarget";
		
		GoalType.HuntTarget:
			return "HuntTarget";
		
		GoalType.Strafe:
			return "Strafe";
		
		GoalType.AdjustRange:
			return "AdjustRange";
		
		GoalType.SayPhrase:
			return "SayPhrase";
		
		_:
			return "UNKNOWN GOAL TYPE!";
