extends GoalComposite
class_name GoalHarvest

var tile_to_harvest : Vector2 = Vector2()

func _init(bot_agent, ore_tile : Vector2).(bot_agent, GoalType.Harvest):
	tile_to_harvest = ore_tile

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active

	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()

	AddSubgoal(GoalHarvestOre.new(agent, tile_to_harvest))

	# if the target is not visible, go hunt it.
	if agent.tilemap.world_to_map(agent.global_position) != agent.tilemap.world_to_map(tile_to_harvest):
		AddSubgoal(GoalMoveToPosition.new(agent, agent.tilemap.map_to_world(tile_to_harvest) + agent.tilemap.cell_size / 2))


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	_reactivateIfFailed()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
