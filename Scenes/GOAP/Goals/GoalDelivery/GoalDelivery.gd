extends GoalComposite
class_name GoalDelivery

var position_to_deliver : Vector2 = Vector2()

func _init(bot_agent, delivery_pos : Vector2).(bot_agent, GoalType.Delivery):
	position_to_deliver = delivery_pos

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active

	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()

	AddSubgoal(GoalDeliveryOre.new(agent, position_to_deliver))

	# if the target is not visible, go hunt it.
	if agent.tilemap.world_to_map(agent.global_position) != agent.tilemap.world_to_map(position_to_deliver):
		AddSubgoal(GoalMoveToPosition.new(agent, position_to_deliver))


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	_reactivateIfFailed()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
