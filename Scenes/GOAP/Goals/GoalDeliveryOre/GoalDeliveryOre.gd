extends Goal
class_name GoalDeliveryOre

var deliveryPosition : Vector2 = Vector2()

func _init(bot_agent, position : Vector2).(bot_agent, GoalType.DeliveryOre):
	deliveryPosition = position

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
		
	if agent.isAtPosition(deliveryPosition):
		agent.start_delivery()
	else:
		status = GoalStatus.Failed


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	if not agent.has_ore:
		agent.collect_money()
		status = GoalStatus.Completed
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
