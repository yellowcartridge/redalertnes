extends Goal
class_name GoalAdjustRange

var target
var idealRange : float = 0

func _init(bot_agent).(bot_agent, GoalType.AdjustRange):
	idealRange = 0


#---------------------------- Initialize -------------------------------------
#-----------------------------------------------------------------------------  
func Activate() -> void:
	agent.steering().target = agent.GetTargetBot().global_position


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	if agent.weaponSys.currentWeapon.isInIdealWeaponRange():
		status = GoalStatus.Completed
		
	return status


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	pass
