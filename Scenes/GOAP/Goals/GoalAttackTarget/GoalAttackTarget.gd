extends GoalComposite
class_name GoalAttackTarget

func _init(bot_agent).(bot_agent, GoalType.AttackTarget):
	pass

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active

	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()

	# it is possible for a bot's target to die whilst this goal is active so we
	# must test to make sure the bot always has an active target
	if agent.attackTargetId == -1:
		status = GoalStatus.Completed
		
		return

	# if the bot is able to shoot the target (there is LOS between bot and
	# target), then select a tactic to follow while shooting
	
		# if the bot has space to strafe then do so
#		var dummy :Vector2
#		if agent.canStepLeft(dummy) or agent.canStepRight(dummy):
#			AddSubgoal(GoalDodgeSideToSide.new(agent))

		# if not able to strafe, head directly at the target's position 
#		else:
	AddSubgoal(GoalHitTarget.new(agent))

	# if the target is not visible, go hunt it.
	if not agent.isAttackSatisfied:
		var enemy = instance_from_id(agent.attackTargetId)
		if not enemy:
			agent.attackTargetId = -1
			status = GoalStatus.Completed
			return
			
		AddSubgoal(GoalMoveToPosition.new(agent, enemy.global_position))


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	# The unit was chasing their target, but the attack distance has been suddenly satisfied
	if agent.isAttackSatisfied and subgoals.front().GetType() == GoalType.MoveToPosition:
		subgoals.front().Terminate()
#		subgoals.front().free()
		subgoals.pop_front()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	_reactivateIfFailed()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
