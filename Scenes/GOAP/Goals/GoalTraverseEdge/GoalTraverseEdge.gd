extends Goal
class_name GoalTraverseEdge

# the edge the bot will follow
var edge : Vector2

# true if m_Edge is the last in the path.
var lastEdgeInPath : bool

# the estimated time the bot should take to traverse the edge
var timeExpected : float
  
# this records the time this goal was activated
var startTime : float

#---------------------------- ctor -------------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, Edge : Vector2, LastEdge : bool).(bot_agent, GoalType.TraverseEdge):
	edge = Edge
	timeExpected = 0.0
	lastEdgeInPath = LastEdge


#---------------------------- Activate -------------------------------------
#-----------------------------------------------------------------------------
# factor in a margin of error for any reactive behavior
var MarginOfError : float = 2.0
func Activate() -> void:
	status = GoalStatus.Active

	agent.maxSpeed = agent.MaxSpeed
	
	
	# record the time the bot starts this goal
	startTime = OS.get_ticks_msec()
	
	# calculate the expected time required to reach the this waypoint. This value
	# is used to determine if the bot becomes stuck 
	#timeExpected = agent.CalculateTimeToReachPosition(edge) * 1000	# covert to ms
	var minimalTimeToReachPosition : float = agent.CalculateTimeToReachPosition(edge)
	agent.stuck = false
	if agent.moveTimer:
		agent.moveTimer.start(minimalTimeToReachPosition)
	
	timeExpected += MarginOfError

	# set the steering target
	agent.steering.SetTarget(edge)
	
	# Set the appropriate steering behavior. If this is the last edge in the path
	# the bot should arrive at the position it points to, else it should seek
	if lastEdgeInPath:
		agent.steering.ArriveOn()
	else:
		agent.steering.SeekOn()


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# if the bot has reached the end of the edge return completed
	if agent.isAtPosition(edge):
		if agent.moveTimer:
			agent.moveTimer.stop()
		status = GoalStatus.Completed
	# if the bot has become stuck return failure
	elif isStuck():
		status = GoalStatus.Failed
	
	return status

#--------------------------- isBotStuck --------------------------------------
#
#  returns true if the bot has taken longer than expected to reach the 
#  currently active waypoint
#-----------------------------------------------------------------------------
func isStuck() -> bool:
	return agent.stuck
#	var TimeTaken : float = OS.get_ticks_msec() - startTime;
#
#	if TimeTaken > timeExpected:
#		print("traverse edge, BOT ", agent, " IS STUCK!!, time taken: ", TimeTaken, " , time expected: ", timeExpected)
#		return true
#
#	return false


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	# turn off steering behaviors.
	agent.steering.SeekOff()
	agent.steering.ArriveOff()
	
	# return max speed back to normal
	agent.maxSpeed = agent.MaxSpeed
	

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	if Global.ShowDebugDrawInfo:
		print("GoalTraverseEdge::draw")
		
	if status == GoalStatus.Active:
		var destination_in_local = agent.global_transform.xform_inv(edge)
		canvas.draw_line(Vector2(), destination_in_local, Color("#0000ff"))
		canvas.draw_circle( destination_in_local, 3.0, Color("#00ff00"))
