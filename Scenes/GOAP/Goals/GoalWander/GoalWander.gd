extends Goal
class_name GoalWander


func _init(bot_agent).(bot_agent, GoalType.Wander):
	pass

#---------------------------- Initialize -------------------------------------
#-----------------------------------------------------------------------------  
func Activate() -> void:
	status = GoalStatus.Active
	agent.steering.WanderOn()


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()

	return status


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	agent.steering.WanderOff()
