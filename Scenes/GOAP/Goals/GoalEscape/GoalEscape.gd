extends Goal
class_name GoalEscape

func _init(bot_agent).(bot_agent, GoalType.Escape):
	pass

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
		
	if agent.global_position.distance_squared_to(agent.escapeTarget) > 1:
		agent.steering.SetTarget(agent.escapeTarget)
	else:
		agent.steering.SetTarget(agent.global_position + Vector2(rand_range(1.0, 10.0), rand_range(1.0, 10.0)))
		
	agent.steering.FleeOn()


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
	agent.steering.FleeOff()
   
