extends Goal
class_name GoalHarvestOre

var tile_to_harvest : Vector2 = Vector2()

func _init(bot_agent, ore_tile : Vector2).(bot_agent, GoalType.HarvestOre):
	tile_to_harvest = ore_tile

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
		
	if agent.tilemap.world_to_map(agent.global_position) != tile_to_harvest:
		status = GoalStatus.Failed
	else:
		agent.start_harvest()

#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	if agent.has_ore:
		agent.collect_ore(tile_to_harvest)
		status = GoalStatus.Completed
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Failed
   
