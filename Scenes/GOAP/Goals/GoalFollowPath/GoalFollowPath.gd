extends GoalComposite
class_name GoalFollowPath

#a local copy of the path returned by the path planner
var path : Array = []

#------------------------------ ctor -----------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, pth : Array).(bot_agent, GoalType.FollowPath):
	path = pth


#------------------------------ Activate -------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	if path.empty():
		status = GoalStatus.Failed
		return
		
	status = GoalStatus.Active
	
	# get a reference to the next edge
	var edge : Vector2 = path.front()
	
	# remove the edge from the path
	path.pop_front() 
	
	AddSubgoal(GoalTraverseEdge.new(agent, edge, path.empty()))


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	status = ProcessSubgoals()
	
	# if there are no subgoals present check to see if the path still has edges.
	# remaining. If it does then call activate to grab the next edge.
	if status == Goal.GoalStatus.Completed and not path.empty():
		Activate()
		
	return status
	

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	if Global.ShowDebugDrawInfo:
		print("GoalFollowPath::draw")
		
	# render all the path waypoints remaining on the path list
	var prev_pos : Vector2 = agent.global_position
	for segment in path:
		var source_in_local = agent.global_transform.xform_inv(prev_pos)
		var destination_in_local = agent.global_transform.xform_inv(segment)
		canvas.draw_line(source_in_local, destination_in_local, Color("#000000"))
		canvas.draw_circle(destination_in_local, 3.0, Color("#ff0000"))
		prev_pos = segment
		
	# forward the request to the subgoals
	.draw(canvas)
 
