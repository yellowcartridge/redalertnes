extends Goal
class_name GoalSeekToPosition

# the position the bot is moving to
var position : Vector2

# the approximate time the bot should take to travel the target location
var timeToReachPos : float
  
# this records the time this goal was activated
var startTime : float

#---------------------------- ctor -------------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, target : Vector2).(bot_agent, GoalType.SeekToPosition):
	position = target
	timeToReachPos = 0.0

											 
#---------------------------- Activate -------------------------------------
#-----------------------------------------------------------------------------  
func Activate() -> void:
	status = GoalStatus.Active
	
	# record the time the bot starts this goal
	startTime = OS.get_system_time_msecs() / 1000.0
	
	# This value is used to determine if the bot becomes stuck 
	timeToReachPos = agent.CalculateTimeToReachPosition(position)
	
	# factor in a margin of error for any reactive behavior
	var MarginOfError : float = 1.0
	
	timeToReachPos += MarginOfError
	
	
	agent.steering.SetTarget(position)
	
	agent.steering.SeekOn()


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# test to see if the bot has become stuck
	if isStuck():
		status = GoalStatus.Failed
	# test to see if the bot has reached the waypoint. If so terminate the goal
	else:
		if agent.isAtPosition(position):
			status = GoalStatus.Completed
	
	return status

#--------------------------- isBotStuck --------------------------------------
#
#  returns true if the bot has taken longer than expected to reach the 
#  currently active waypoint
#-----------------------------------------------------------------------------
func isStuck() -> bool:
	var TimeTaken : float = OS.get_system_time_msecs() / 1000.0 - startTime
	
	if TimeTaken > timeToReachPos:
		print("Seek to pos, BOT ", agent, " IS STUCK!!");
		
		return true
	
	return false


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
  agent.steering.SeekOff()
  agent.steering.ArriveOff()

  status = GoalStatus.Completed

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	if Global.ShowDebugDrawInfo:
		print("GoalSeekToPosition::draw")
		
	if status != GoalStatus.Active and status != GoalStatus.Inactive:
		return
	
	var color
	if status == GoalStatus.Active:
		color = Color("#00FF00")
	elif status == GoalStatus.Inactive:
		color = Color("#00FF00")
		
	var target_in_local = agent.global_transform.xform_inv(position)
	canvas.draw_line(Vector2(), target_in_local, color)
	canvas.draw_circle( target_in_local, 3.0, color)
