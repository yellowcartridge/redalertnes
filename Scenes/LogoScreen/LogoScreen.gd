extends Control

onready var logo : NinePatchRect = $Logo

export (int) var lines : int = 1000
export (float) var wave_amplitude : float = 0.05
export (float) var time : float = 0
export (float) var alpha : float = 1.0

func _ready():
	var screen_size = OS.get_screen_size()
	var window_size = OS.get_window_size()

	OS.set_window_position(screen_size*0.5 - window_size*0.5)
	
	logo.material.set("shader_param/lines", lines)
	$AnimationPlayer.play("Wave")

func _process(delta):
	logo.modulate.a = alpha
	logo.material.set("shader_param/wave_amplitude", wave_amplitude)
	logo.material.set("shader_param/time", time)

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://Scenes/TitleScreen/TitleScreen.tscn")
