extends Control

onready var fractionSelect : Control = $FractionSelect
onready var mainScreen : Control = $MainScreen
onready var mainMenu : Control = $MainScreen/MainMenu

func _ready():
	fractionSelect.connect("go_back", self, "turn_main_menu")
	fractionSelect.connect("fraction_selected", self, "go_game")
	mainMenu.connect("select_fraction", self, "turn_fraction_select")
	mainMenu.connect("two_players_selected", self, "go_game")
	mainMenu.connect("credits_selected", self, "go_game")
	
	turn_main_menu()

func turn_fraction_select() -> void:
	fractionSelect.visible = true
	fractionSelect.locked = false
	fractionSelect.set_process(true)
	mainScreen.visible = false
	mainMenu.locked = true
	mainMenu.set_process(false)
	
func turn_main_menu() -> void:
	fractionSelect.visible = false
	fractionSelect.locked = true
	fractionSelect.set_process(false)
	mainScreen.visible = true
	mainMenu.locked = false
	mainMenu.set_process(true)

func go_game() -> void:
	$Music.stop()
	$AcceptSound.play()
