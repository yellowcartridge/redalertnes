extends Control


func _process(delta):
	if Input.is_action_just_pressed("player_1_up") or Input.is_action_just_pressed("player_2_up") or Input.is_action_just_pressed("player_1_left") or Input.is_action_just_pressed("player_2_left") or Input.is_action_just_pressed("player_1_right") or Input.is_action_just_pressed("player_2_right") or Input.is_action_just_pressed("player_1_down") or Input.is_action_just_pressed("player_2_down") or Input.is_action_just_pressed("player_1_select") or Input.is_action_just_pressed("player_2_select") or Input.is_action_just_pressed("player_1_A") or Input.is_action_just_pressed("player_2_A") or Input.is_action_just_pressed("player_1_B") or Input.is_action_just_pressed("player_2_B") or Input.is_action_just_pressed("player_1_start") or Input.is_action_just_pressed("player_2_start"):
		get_tree().change_scene("res://Scenes/TitleScreen/TitleScreen.tscn")
