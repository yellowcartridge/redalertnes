tool
extends TextureRect

export (NodePath) var playerPath : NodePath = NodePath()

onready var player : Node = get_node_or_null(playerPath)

var imageTexture : ImageTexture = null
var dynImage : Image = null
var cell_size : Vector2 = Vector2()

func _ready():
	if not player:
		set_process(false)
		return
		
	cell_size = player.get_tilemap_cell_size()
	
	imageTexture = ImageTexture.new()
	dynImage = Image.new()
	
	dynImage.create(rect_size.x,rect_size.y,false,Image.FORMAT_RGBA8)
	dynImage.fill(Color(0,0,0,0))
	
	imageTexture.create_from_image(dynImage)
	self.texture = imageTexture
	
#func _process(delta):
#	update_image()
#
#func update_image() -> void:
#	var matrix_size : Vector2 = player.get_fog_of_war_size()
#	var fow_matrix : Array = player.get_fog_of_war_matrix()
#
#
#
#	dynImage.lock()
#	for i in range( matrix_size.x):
#		for j in range( matrix_size.y):
#			if fow_matrix[i*matrix_size.y + j] == 0:
#				for ki in range(cell_size.x):
#					for kj in range(cell_size.y):
#						dynImage.set_pixel(i*cell_size.x + ki, j*cell_size.y + kj, Color(0, 0, 0, 1))
#	dynImage.unlock()
#
#	imageTexture.set_data(dynImage)
#	self.texture = imageTexture
