extends Control

onready var BGMusicStreams: Array = [
	$HellMarch,
	$Radio,
	$Grinder,
	$Bigfoot,
	$CnCActoi,
	$CnCDemol,
	$CnCFire,
	$CnCPr4bt,
	$CnCProwl,
	$CnCRadio,
	$CnCWarfr
]

func _ready():
	for player in BGMusicStreams:
		player.connect("finished", self, "_on_StreamPlayer_finished")
		
	play_random_music()

func play_random_music():
	for player in BGMusicStreams:
		player.stop()

	var rand_nb = randi() % BGMusicStreams.size()
	BGMusicStreams[rand_nb].play()

func _on_StreamPlayer_finished():
	play_random_music()
