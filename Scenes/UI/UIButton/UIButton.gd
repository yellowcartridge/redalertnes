extends Control
class_name UIButton

enum ButtonState {
	NORMAL,
	PRESSED,
	HOVER,
	DISABLED,
	FOCUSED
}

signal accept_action
signal decline_action

export (String) var accept_action_command : String = ""
export (String) var decline_action_command : String = ""

export (bool) var disabled : bool = false setget set_disabled

export (Texture) var texture_normal : Texture = null setget set_texture_normal
export (Texture) var texture_pressed : Texture = null setget set_texture_pressed
export (Texture) var texture_hover : Texture = null setget set_texture_hover
export (Texture) var texture_disabled : Texture = null setget set_texture_disabled
export (Texture) var texture_focused : Texture = null setget set_texture_focused

onready var sprite_normal : Sprite = $texture_normal
onready var sprite_pressed : Sprite = $texture_pressed
onready var sprite_hover : Sprite = $texture_hover
onready var sprite_disabled : Sprite = $texture_disabled
onready var sprite_focused : Sprite = $texture_focused

var state : int = ButtonState.NORMAL setget set_state
var pressed : bool = false setget set_pressed
var hover : bool = false setget set_hover

func _ready():
	update_sprites()
	update_disabled_state()
	update_state()

func set_texture_normal(texture : Texture) -> void:
	if texture_normal == texture:
		return
		
	texture_normal = texture
		
	if sprite_normal:
		sprite_normal.texture = texture_normal
	update_state()

func set_texture_pressed(texture : Texture) -> void:
	if texture_pressed == texture:
		return
	texture_pressed = texture
	if sprite_pressed:
		sprite_pressed.texture = texture_pressed
	update_state()
	
func set_texture_hover(texture : Texture) -> void:
	if texture_hover == texture:
		return
	texture_hover = texture
	if sprite_hover:
		sprite_hover.texture = texture_hover
	update_state()
	
func set_texture_disabled(texture : Texture) -> void:
	if texture_disabled == texture:
		return
	texture_disabled = texture
	if sprite_disabled:
		sprite_disabled.texture = texture_disabled
	update_state()
	
func set_texture_focused(texture : Texture) -> void:
	if texture_focused == texture:
		return
	texture_focused = texture
	if sprite_focused:
		sprite_focused.texture = texture_focused
	update_state()
	
func set_disabled(value : bool) -> void:
	if disabled != value:
		disabled = value
		
		if disabled:
			if hover:
				set_hover(false)
			if pressed:
				set_pressed(false)
		
		update_disabled_state()
			
func update_disabled_state() -> void:
	if disabled:
		toggle_disabled()
	else:
		toggle_normal()
			
func set_pressed(value : bool) -> void:
	if pressed != value:
		pressed = value
		
		if pressed:
			toggle_pressed()
		else:
			if hover:
				toggle_hover()
			else:
				toggle_normal()
			
func set_hover(value : bool) -> void:
	if hover != value:
		hover = value
		
		if hover:
			toggle_hover()
		else:
			if disabled:
				toggle_disabled()
			else:
				toggle_normal()
	
func pointedByMouse(point : Vector2):
	if disabled:
		return

	if get_global_rect().has_point(point):
		set_hover(true)
		set_pressed(Input.is_action_pressed(accept_action_command) or Input.is_action_pressed(decline_action_command))
	else:
		set_hover(false)
		set_pressed(false)

func _input(event):
	if disabled:
		return
		
	if accept_action_command.empty() or decline_action_command.empty():
		print("Button ", name, " does not have action commands!")
		return
		
	if Input.is_action_just_released(accept_action_command):
		if hover:
			emit_signal("accept_action")
	elif Input.is_action_just_released(decline_action_command):
		if hover:
			emit_signal("decline_action")
		
func toggle_normal() -> void:
	set_state(ButtonState.NORMAL)
	
func toggle_pressed() -> void:
	set_state(ButtonState.PRESSED)
	
func toggle_hover() -> void:
	set_state(ButtonState.HOVER)
	
func toggle_disabled() -> void:
	set_state(ButtonState.DISABLED)
	
func toggle_focused() -> void:
	set_state(ButtonState.FOCUSED)

func set_state(value : int) -> void:
	if state != value:
		state = value
		update_state()

func update_sprites() -> void:
	if sprite_normal:
		sprite_normal.texture = texture_normal
	if sprite_pressed:
		sprite_pressed.texture = texture_pressed
	if sprite_hover:
		sprite_hover.texture = texture_hover
	if sprite_disabled:
		sprite_disabled.texture = texture_disabled
	if sprite_focused:
		sprite_focused.texture = texture_focused
	

func update_state() -> void:
	if not sprite_normal or not sprite_pressed or not sprite_hover or not sprite_disabled or not sprite_focused:
		return
	
	sprite_normal.visible = false
	sprite_pressed.visible = false
	sprite_hover.visible = false
	sprite_disabled.visible = false
	sprite_focused.visible = false

	match state:
		ButtonState.NORMAL:
			sprite_normal.visible = true
		ButtonState.PRESSED:
			if texture_pressed:
				sprite_pressed.visible = true
			else:
				sprite_normal.visible = true
		ButtonState.HOVER:
			if texture_hover:
				sprite_hover.visible = true
			else:
				sprite_normal.visible = true
		ButtonState.DISABLED:
			if texture_disabled:
				sprite_disabled.visible = true
			else:
				sprite_normal.visible = true
		ButtonState.FOCUSED:
			if texture_focused:
				sprite_focused.visible = true
			else:
				sprite_normal.visible = true
