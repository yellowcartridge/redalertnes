extends Node2D

export (Array, Color) var health_colors = [
	Color("#881400"),
	Color("#e40058"),
	Color("#f83800"),
	Color("#fca044"),
	Color("#f8d878"),
	Color("#d8f878"),
	Color("#b8f818"),
	Color("#00b800")
]

onready var animationPlayer : AnimationPlayer = $AnimationPlayer
onready var sprite : Sprite = $Sprite

var trackingInstance = null
var playerController = null

func get_health_color(health : float, max_health : float) -> Color:
	var steps : int = health_colors.size()
	
	if not steps or not max_health:
		return Color()
	
	var health_in_step : float = max_health / steps
	var health_level : int = max(floor(health / health_in_step)-1, 0)
	
	if health_level >= steps:
		return Color()
		
	return health_colors[health_level]
	
	
func Init(instanceToTrack, player) -> void:
	trackingInstance = instanceToTrack
	playerController = player
	
	update_icon()

func _ready():
	if not trackingInstance or not "sprite" in trackingInstance:
		return
		
	sprite.texture = trackingInstance.sprite.frames.get_frame(trackingInstance.sprite.animation, trackingInstance.sprite.frame)
	sprite.rotation = trackingInstance.sprite.rotation + trackingInstance.rotation
	sprite.material.set("shader_param/selection_color", get_health_color(trackingInstance.health, trackingInstance.max_health))
	animationPlayer.play("Blink")

func _process(delta):
	update_icon()

func update_icon() -> void:
	if not trackingInstance or not "sprite" in trackingInstance:
		return
		
	if not playerController:
		return
	
	if sprite:	
		sprite.texture = trackingInstance.sprite.frames.get_frame(trackingInstance.sprite.animation, trackingInstance.sprite.frame)
		sprite.rotation = trackingInstance.sprite.rotation + trackingInstance.rotation
		sprite.material.set("shader_param/selection_color", get_health_color(trackingInstance.health, trackingInstance.max_health))
	global_position = playerController.transform_world_to_screen_coord(trackingInstance.global_position)

func _on_AnimationPlayer_animation_finished(anim_name):
	animationPlayer.play(anim_name)
