extends Control

export (int) var current_choise : int = 0

onready var cursor : AnimatedSprite = $Pointer

var locked : bool = false

var options : Array = [
	{"animation": "OnePlayerSelected", "cursor_position": Vector2(0, 0), "scene": "res://Scenes/SingleScreen/SingleScreen.tscn"},
	{"animation": "TwoPlayersSelected", "cursor_position": Vector2(0, 16), "scene": "res://Scenes/SplitScreen/SplitScreen.tscn"},
	{"animation": "CreditsSelected", "cursor_position": Vector2(0, 64), "scene": "res://Scenes/Credits/Credits.tscn"}
]

signal select_fraction
signal two_players_selected
signal credits_selected

func _process(delta):
	if locked:
		return
		
	if Input.is_action_just_pressed("player_1_up") or Input.is_action_just_pressed("player_2_up"):
		go_up()
	elif Input.is_action_just_pressed("player_1_down") or Input.is_action_just_pressed("player_2_down") or Input.is_action_just_pressed("player_1_select") or Input.is_action_just_pressed("player_2_select"):
		go_down()
		
	if Input.is_action_just_pressed("player_1_A") or Input.is_action_just_pressed("player_2_A") or Input.is_action_just_pressed("player_1_start") or Input.is_action_just_pressed("player_2_start"):
		accept_selection()
		
func go_up() -> void:
	if locked:
		return
		
	current_choise = (current_choise - 1) % options.size()
	update_menu()
	
func go_down() -> void:
	if locked:
		return
		
	current_choise = (current_choise + 1) % options.size()
	update_menu()

func update_menu() -> void:
	cursor.position = options[current_choise].cursor_position

func accept_selection() -> void:
	if current_choise == 0:
		Global.aiEnabled = true
		emit_signal("select_fraction")
		return
	elif current_choise == 1:
		Global.firstPlayerColor = Global.PlayerColor.BLUE
		Global.firstPlayerFraction = Global.PlayerFraction.AlliedForces
		Global.secondPlayerColor = Global.PlayerColor.RED
		Global.secondPlayerFraction = Global.PlayerFraction.SovietUnion
		Global.aiEnabled = false
		emit_signal("two_players_selected")
	else:
		emit_signal("credits_selected")
	
	locked = true
	$AnimationPlayer.play(options[current_choise].animation)

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene(options[current_choise].scene)
