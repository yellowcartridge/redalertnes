extends Building
class_name ShipYard

func _init():
	buildingName = "ShipYard"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]}
		],
		Global.Orientation.LEFT: [
			{"x": -1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		],
		Global.Orientation.UP: [
			{"x": -1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		],
		Global.Orientation.DOWN: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]}
		],
		Global.Orientation.LEFT: [
			{"x": -1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		],
		Global.Orientation.UP: [
			{"x": -1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		],
		Global.Orientation.DOWN: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_water", "materials": ["water_tile"]}
		]
	}

func calculate_orientation(tilemap : TileMap, tileToPlace : Vector2) -> void:
	var tile_position : Vector2 = tilemap.map_to_world(tileToPlace)
	var main_tile_index : int = tilemap.get_cell(tileToPlace.x, tileToPlace.y)
	var water_tile_index = tilemap.tile_set.find_tile_by_name(waterTileName)
	
	var directionVector : Vector2 = Vector2()
	var directionObtained : bool = false
	
	if main_tile_index == water_tile_index:
		var oppositeOrientation : int = tilemap.direction_of_any_closest_tile_except_name(tileToPlace, waterTileName)
		match oppositeOrientation:
			Global.Orientation.LEFT:
				currentOrientation = Global.Orientation.RIGHT
			Global.Orientation.RIGHT:
				currentOrientation = Global.Orientation.LEFT
			Global.Orientation.UP:
				currentOrientation = Global.Orientation.DOWN
			Global.Orientation.DOWN:
				currentOrientation = Global.Orientation.UP
	else:
		currentOrientation = tilemap.direction_of_closest_tile_by_name(tileToPlace, waterTileName)	

