extends Building
class_name Aviation

func _init():
	buildingName = "Aviation"
	
	blue_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		]
	}
	
	red_tiles = {
		Global.Orientation.RIGHT: [
			{"x": -1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": -1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 0, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": -1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 0, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]},
			{"x": 1, "y": 1, "name": "shipyard_grass", "materials": ["ground_tile"]}
		]
	}
