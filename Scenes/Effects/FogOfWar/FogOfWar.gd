extends TextureRect

export (NodePath) var playerPath : NodePath = NodePath()

export (PackedScene) var fogOfWarTileScene : PackedScene = preload("res://Scenes/Marks/FogOfWarTile/FogOfWarTile.tscn")

onready var player : Node = get_node_or_null(playerPath)

var imageTexture : ImageTexture = null
var dynImage : Image = null
var cell_size : Vector2 = Vector2()
var map_rect : Rect2 = Rect2()
var screen_size_in_tiles : Vector2 = Vector2()
var fog_of_war_tiles_size : Vector2 = Vector2()
var image_center : Vector2 = Vector2()

var fowSpritesArray : Array = []

func _ready():
	if not player:
		set_process(false)
		return
	
	cell_size = player.get_tilemap_cell_size()
	map_rect = player.get_tilemap_rect()
	
#	imageTexture = ImageTexture.new()
#	dynImage = Image.new()
	
	print("RECT SIZE: ", rect_size)
	
	screen_size_in_tiles = Vector2(ceil(rect_size.x / cell_size.x), ceil(rect_size.y / cell_size.y))
	fog_of_war_tiles_size = screen_size_in_tiles + Vector2(2, 2)
	image_center = Vector2(rect_size.x / 2, rect_size.y / 2)
#	dynImage.create(rect_size.x,rect_size.y,false,Image.FORMAT_RGBA8)
#	dynImage.fill(Color(0,0,0,0))
	
#	imageTexture.create_from_image(dynImage)
#	self.texture = imageTexture
	
	player.connect("fog_of_war_changed", self, "fog_of_war_changed")
	create_tiles()
	update_image()

func create_tiles() -> void:
	fowSpritesArray.resize(fog_of_war_tiles_size.x*fog_of_war_tiles_size.y)
	for x in range(fog_of_war_tiles_size.x):
		for y in range(fog_of_war_tiles_size.y):
			var fowSprite = fogOfWarTileScene.instance()
			fowSprite.global_position = Vector2(x * cell_size.x, y * cell_size.y)
			fowSpritesArray[x * fog_of_war_tiles_size.y + y] = fowSprite
			add_child(fowSprite)

func update_image() -> void:
	var matrix_size : Vector2 = player.get_fog_of_war_size()
	var fow_matrix : Array = player.get_fog_of_war_matrix()
	
	var top_left_position : Vector2 = player.global_position - image_center
	var top_left_tile : Vector2 = player.tilemap.world_to_map(top_left_position)
	var top_left_matrix_coordinate : Vector2 = top_left_tile - map_rect.position
	var offset : Vector2 = top_left_position - Vector2(top_left_tile.x * cell_size.x, top_left_tile.y * cell_size.y)

	for i in range(fog_of_war_tiles_size.x):
		for j in range(fog_of_war_tiles_size.y):
			var matrix_index : float = (i+top_left_matrix_coordinate.x)*matrix_size.y + (j+top_left_matrix_coordinate.y)

			if matrix_index < 0 or matrix_index >= fow_matrix.size():
				continue

			var fowArrayIndex : int = i * fog_of_war_tiles_size.y + j
			fowSpritesArray[fowArrayIndex].global_position = Vector2(i*cell_size.x - offset.x, j*cell_size.y - offset.y)
			fowSpritesArray[fowArrayIndex].visible = not fow_matrix[matrix_index]

#func _process(delta):
#	update()
	
#func _draw():
#	var player_position : Vector2 = player.global_position
#
#	var top_left_position : Vector2 = player.global_position - image_center
#	var top_left_tile : Vector2 = player.tilemap.world_to_map(top_left_position)
#	var top_left_matrix_coordinate : Vector2 = top_left_tile - map_rect.position
#	var offset : Vector2 = Vector2(top_left_tile.x * cell_size.x, top_left_tile.y * cell_size.y) - top_left_position
#
#	draw_rect(Rect2(offset, Vector2(fog_of_war_tiles_size.x * cell_size.x, fog_of_war_tiles_size.y * cell_size.y)), Color.red, false, 5)

func fog_of_war_changed(cameraTile : Vector2) -> void:
	update_image()
