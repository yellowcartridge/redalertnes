extends Resource
class_name ProductionRecord

enum ProductionType {
	BUILDING,
	INFANTRY,
	ENGINERY
}

enum MovableMaterial {
	GROUND,
	WATER,
	HYBRID,
	AIR
}

export (String) var productionName : String = ""
export (ProductionType) var type : int = ProductionType.BUILDING
export (MovableMaterial) var movableMaterial : int = MovableMaterial.GROUND
export (int) var cost : int = 0
export (int) var energyConsume : int = 0
export (int) var energyProduce : int = 0
export (int) var productionTime : int = 0
export (Array, String) var dependencies : Array = []
export (PackedScene) var productionScene : PackedScene = null
export (bool) var rotatable : bool = false
