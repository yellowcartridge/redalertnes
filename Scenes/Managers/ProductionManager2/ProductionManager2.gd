extends Node2D
class_name ProductionManager

export (Array, Resource) var productions : Array = []

export (int) var money : int = 0
export (float) var timeToProduceToken : float = 1.0
export (float) var timeToProduceTokenInLEMode : float = 3.0
export (bool) var allowProductionsInLEMode : bool = true

export (Dictionary) var productionsLimits : Dictionary = {}
export (Dictionary) var productionsTypesLimits : Dictionary = {}

export (Dictionary) var productionsFreeQuota : Dictionary = {}

export (Dictionary) var maxProductionsInQuery : Dictionary = {}
export (Dictionary) var maxProductionsTypesInQuery : Dictionary = {}
export (Dictionary) var maxProductionsTypesReady : Dictionary = {}

onready var energyConsumed : int = 0
onready var energyProduced : int = 0

onready var productionsQuery : Array = []

var productionsExist : Dictionary = {}
var productionsTypesExist : Dictionary = {}
var productionsAwaits : Dictionary = {}

var productionsProgresses : Dictionary = {}

var lowEnergyState : bool = false

signal production_ready
signal production_list_changed
signal available_productions_updated
signal productions_progresses_changed

signal money_changed
signal energy_changed

signal placeUnit
signal placeBuilding

func _ready():
	productionsTypesExist.clear()
	
	for type in ProductionRecord.ProductionType.values():
		productionsTypesExist[type] = 0
	
	productionsExist.clear()
	
	for prod in productions:
		productionsExist[prod.productionName] = 0
		productionsAwaits[prod.productionName] = 0
		
	update_available_productions()
	update_money()
	emit_signal("energy_changed", energyConsumed, energyProduced)

func _process(delta):
	productionsProgresses.clear()
	
#	var currentTime: int = OS.get_ticks_msec()
	for prod in productionsQuery:
		if not productionsProgresses.has(prod.productionName):
			productionsProgresses[prod.productionName] = float(prod.tokensReady) /  prod.tokensToFinish
		
		if not prod.tokenIsPaid:
			if money < prod.tokenPrice:
				continue
			else:
				decrease_money(prod.tokenPrice)
				prod.moneySpent += prod.tokenPrice
				prod.tokenIsPaid = true
			
		prod.nextTokenProgress += delta
		
		var tokenTime : int = timeToProduceTokenInLEMode if lowEnergyState else timeToProduceToken
		if prod.nextTokenProgress >= tokenTime:
			var tokensProduced : int = int(prod.nextTokenProgress / tokenTime)
			
			while tokensProduced > 1 and money < prod.tokenPrice * (tokensProduced - 1):
				tokensProduced -= 1
			if tokensProduced > 1:
				var additionalPayment : float = prod.tokenPrice * (tokensProduced - 1)
				decrease_money(additionalPayment)
				prod.moneySpent += additionalPayment
			
			prod.nextTokenProgress -= tokensProduced * tokenTime
			prod.tokensReady += tokensProduced
			prod.tokenIsPaid = false
		
		if prod.tokensToFinish <= prod.tokensReady:
			productionsQuery.erase(prod)
			production_finished(prod)
		
			
	emit_signal("productions_progresses_changed", productionsProgresses)

func increase_money(amount : int) -> void:
	money += amount
	update_money(true)
	
func decrease_money(amount : int) -> void:
	money -= amount
	update_money(true)
	
func update_money(updateAvailableProductions: bool = false) -> void:
	if updateAvailableProductions:
		update_available_productions()
	emit_signal("money_changed", money)

func add_existing_production(productionName : String) -> void:
	var params : Resource = get_production_params(productionName)
	
	if not params:
		return
		
	if productionsExist.has(params.productionName):
		productionsExist[params.productionName] += 1

	if productionsTypesExist.has(params.type):
		productionsTypesExist[params.type] += 1
		
	energyConsumed += params.energyConsume
	energyProduced += params.energyProduce
	lowEnergyState = energyConsumed > energyProduced
	emit_signal("energy_changed", energyConsumed, energyProduced, lowEnergyState)

	update_available_productions()

func remove_existing_production(productionName : String) -> void:
	var params : Resource = get_production_params(productionName)
	
	if not params:
		return
		
	if productionsExist.has(params.productionName):
		if productionsExist[params.productionName] > 0:
			productionsExist[params.productionName] -= 1

	if productionsTypesExist.has(params.type):
		if productionsTypesExist[params.type] > 0:
			productionsTypesExist[params.type] -= 1
			
	energyConsumed -= params.energyConsume
	energyProduced -= params.energyProduce
	lowEnergyState = energyConsumed > energyProduced
	emit_signal("energy_changed", energyConsumed, energyProduced, lowEnergyState)
	
	update_available_productions()

			
func get_count_of_existing_productions(productionName : String) -> int:
	if productionsExist.has(productionName):
		return productionsExist[productionName]

	return 0
	
func get_count_of_existing_productions_types(type : int) -> int:
	if productionsTypesExist.has(type):
		return productionsTypesExist[type]
	return 0
			
func is_depencies_satisfied(production : Resource) -> bool:
	for depName in production.dependencies:
		if get_count_of_existing_productions(depName) == 0:
			return false
	return true

func updateProductions() -> void:
	pass

func get_production_params(productionName : String) -> Resource:
	for prod in productions:
		if prod.productionName == productionName:
			return prod
	return null

func produce(productionName : String) -> bool:
	var params : Resource = get_production_params(productionName)
	
	if not is_production_available(params) or not is_depencies_satisfied(params):
		return false
	
#	var time = OS.get_ticks_msec()
	productionsQuery.push_back({"productionName": params.productionName, "productionType": params.type, "tokensToFinish": params.productionTime / 1000, "tokensReady": 0, "nextTokenProgress": 0.0, "tokenIsPaid": false, "moneySpent": 0.0, "tokenPrice": float(params.cost) / (float(params.productionTime) / 1000)})
	
	update_available_productions()
	
	return true
	
func is_production_exists(productionName : String) -> bool:
	return productionsExist.has(productionName) and productionsExist[productionName] > 0
	
func is_production_available(production : Resource) -> bool:
	if not production:
		return false

	if not allowProductionsInLEMode and energyProduced - energyConsumed < production.energyConsume:
		return false
	
	if is_production_limit_reached(production):
		return false
			
	if maxProductionsInQuery.has(production.productionName):
		if productions_in_query_count(production.productionName) >= maxProductionsInQuery[production.productionName]:
			return false
	
	if maxProductionsTypesInQuery.has(production.type):
		if productions_types_in_query_count(production.type) >= maxProductionsTypesInQuery[production.type]:
			return false
			
	if maxProductionsTypesReady.has(production.type):
		if productions_types_ready_count(production.type) >= maxProductionsTypesReady[production.type]:
			return false
		
	return true
	
func is_production_limit_reached(production : Resource) -> bool:
	if productionsLimits.has(production.productionName) and productionsExist.has(production.productionName):
		if productionsExist[production.productionName] >= productionsLimits[production.productionName]:
				return true
			
	if productionsTypesLimits.has(production.type) and productionsTypesExist.has(production.type):
		if productionsTypesExist[production.type] - get_quoted_count_of_type(production.type) >= productionsTypesLimits[production.type]:
			if not (productionsFreeQuota.has(production.productionName) and productionsExist[production.productionName] < productionsFreeQuota[production.productionName]):
				return true
				
	return false
	
func is_production_awaits(productionName : String) -> bool:
	return productionsAwaits.has(productionName) and productionsAwaits[productionName] > 0

func productions_in_query_count(productionName : String):
	var count : int = 0
	for prod in productionsQuery:
		if prod.productionName == productionName:
			count += 1
			
	return count
	
func productions_types_in_query_count(productionType : int):
	var count : int = 0
	for prod in productionsQuery:
		if prod.productionType == productionType:
			count += 1
			
	return count

func productions_types_ready_count(productionType : int):
	var count : int = 0
	for prodName in productionsAwaits.keys():
		var prod : Resource = get_production_params(prodName)
		if prod.type == productionType:
			count += productionsAwaits[prodName]
			
	return count
	
func get_quoted_count_of_type(productionType : int) -> int:
	var count : int = 0
	for prod in productionsFreeQuota.keys():
		var production : Resource = get_production_params(prod)
		if not production or not productionsExist.has(prod):
			continue
		if production.type == productionType:
			count += min(productionsExist[prod], productionsFreeQuota[prod])
	return count

func production_finished(production : Dictionary) -> void:
	productionsAwaits[production.productionName] += 1
	update_available_productions()
	emit_signal("production_ready", production.productionName)
	
func take_production(productionName : String) -> Node:
	return get_production_instance(productionName)
	
func production_is_taken(productionName : String) -> void:
	if productionsAwaits.has(productionName) and productionsAwaits[productionName] > 0:
		productionsAwaits[productionName] -= 1
	
func get_production_instance(productionName : String) -> Node:
	var params : Resource = get_production_params(productionName)
	
	if not params:
		return null
	
	var instance : Node = params.productionScene.instance()
	if "movableMaterial" in instance:
		instance.movableMaterial = params.movableMaterial
		if "movableMaterials" in instance:
			instance.movableMaterials = Global.movableMaterials[params.movableMaterial]
	if "type" in instance:
		instance.type = params.type
	if "rotatable" in instance:
		instance.rotatable = params.rotatable
	
	return instance

func update_available_productions() -> void:
	var available_productions : Array = []
	for production in productions:
		if is_depencies_satisfied(production):
			available_productions.append({
				"name": production.productionName,
				"type": "building" if production.type == ProductionRecord.ProductionType.BUILDING else "unit",
				"available": is_production_available(production),
				"awaits": is_production_awaits(production.productionName)
			})
	emit_signal("available_productions_updated", available_productions)
	
func button_accept_action(production : String) -> void:
	print("accept: ", production)
	if is_production_awaits(production):
		#take_production(production)
		#production_is_taken(production)
		startProductionPlacing(production)
	else:
		produce(production)
		
	
func button_decline_action(production : String) -> void:
	var params : Resource = get_production_params(production)
	if not params:
		return
	
	if productionsAwaits.has(production) and productionsAwaits[production] > 0:
		productionsAwaits[production] -= 1
		increase_money(params.cost)
		update_available_productions()
	else:
		var productionQueryRecord : Dictionary = get_production_from_query_if_exists(production)
		if not productionQueryRecord.empty():
			remove_production_from_query(productionQueryRecord)
			increase_money(productionQueryRecord.moneySpent)
			update_available_productions()

func remove_production_from_query(productionQueryRecord : Dictionary) -> void:
	productionsQuery.erase(productionQueryRecord)
	
func get_production_from_query_if_exists(production : String) -> Dictionary:
	for prod in productionsQuery:
		if prod.productionName == production:
			return prod
	return {}

func startProductionPlacing(productionName : String) -> void:
	var params : Resource = get_production_params(productionName)
	
	if not params:
		return
		
	if params.type == ProductionRecord.ProductionType.BUILDING:
		emit_signal("placeBuilding", productionName)
	else:
		emit_signal("placeUnit", productionName)
	
func cancelProductionPlacing(_productionName : String) -> void:
	pass
	
func productionPlaced(productionName : String) -> void:
	production_is_taken(productionName)
	add_existing_production(productionName)
	update_available_productions()

func productionDestroyed(productionName : String) -> void:
	remove_existing_production(productionName)
