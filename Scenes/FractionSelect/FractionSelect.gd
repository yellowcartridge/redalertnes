extends Control

export (int) var current_choise : int = 0

onready var alliesFrame : Control = $AlliesFrame
onready var sovietFrame : Control = $SovietFrame

var locked : bool = false
var fractionsCount : int = 2

var alliesFraction : int = 0
var sovietFraction : int = 1

signal go_back
signal fraction_selected

func _ready():
	update_menu()

func _process(delta):
	if locked:
		return
		
	if Input.is_action_just_pressed("player_1_left") or Input.is_action_just_pressed("player_2_left"):
		go_left()
	elif Input.is_action_just_pressed("player_1_right") or Input.is_action_just_pressed("player_2_right") or Input.is_action_just_pressed("player_1_select") or Input.is_action_just_pressed("player_2_select"):
		go_right()
		
	if Input.is_action_just_pressed("player_1_A") or Input.is_action_just_pressed("player_2_A") or Input.is_action_just_pressed("player_1_start") or Input.is_action_just_pressed("player_2_start"):
		accept_selection()
		
	if Input.is_action_just_pressed("player_1_B") or Input.is_action_just_pressed("player_2_B"):
		go_back()

func go_right() -> void:
	if locked:
		return
		
	current_choise = (current_choise + 1) % fractionsCount
	update_menu()
	
func go_left() -> void:
	if locked:
		return
		
	current_choise = (current_choise - 1) % fractionsCount
	update_menu()

func update_menu() -> void:
	if current_choise == alliesFraction:
		alliesFrame.visible = true
		sovietFrame.visible = false
		
		Global.firstPlayerColor = Global.PlayerColor.BLUE
		Global.firstPlayerFraction = Global.PlayerFraction.AlliedForces
		Global.secondPlayerColor = Global.PlayerColor.RED
		Global.secondPlayerFraction = Global.PlayerFraction.SovietUnion
	else:
		alliesFrame.visible = false
		sovietFrame.visible = true
		
		Global.firstPlayerColor = Global.PlayerColor.RED
		Global.firstPlayerFraction = Global.PlayerFraction.SovietUnion
		Global.secondPlayerColor = Global.PlayerColor.BLUE
		Global.secondPlayerFraction = Global.PlayerFraction.AlliedForces

func accept_selection() -> void:
	if current_choise == alliesFraction:
		$AnimationPlayer.play("AlliedSelected")
	else:
		$AnimationPlayer.play("SovietSelected")
		
	locked = true
	emit_signal("fraction_selected")
	
func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://Scenes/SingleScreen/SingleScreen.tscn")

func go_back() -> void:
	emit_signal("go_back")
