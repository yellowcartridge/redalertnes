extends Area2D

var collisionShapes : Array = []
var tracking_bodies : Array = []

signal area_cleared

func Init(tiles : Array, tile_size : Vector2):
	for tile in tiles:
		var collisionTile : CollisionShape2D = CollisionShape2D.new()
		collisionShapes.push_back(collisionTile)
		collisionTile.shape = RectangleShape2D.new()
		collisionTile.shape.set_extents(tile_size / 2)
		add_child(collisionTile)
		collisionTile.set_global_position(Vector2(tile.x * tile_size.x + tile_size.x / 2, tile.y * tile_size.y + tile_size.y / 2))
		
func _ready():
	print("ready: ", get_overlapping_bodies().size())
	

func _on_EscapeArea_body_entered(body):
	if not body in tracking_bodies and body is Unit:
		tracking_bodies.push_back(body)
		body.escape_from_area(global_position)


func _on_EscapeArea_body_exited(body):
	if body in tracking_bodies:
		tracking_bodies.erase(body)
		body.escape_finished()
		
	if tracking_bodies.empty():
		emit_signal("area_cleared")
		queue_free()
