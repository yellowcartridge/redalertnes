extends TileMap

export (NodePath) var drawLayerPath : NodePath = NodePath()
onready var drawLayer : Node2D = get_node_or_null(drawLayerPath)

onready var traversableTiles : Array = [tile_set.find_tile_by_name("ground_tile"), tile_set.find_tile_by_name("ore_tile")]

#var dGrid : Array = []
#var fGrid : Array = []
#
#var losGrid : Array = []

var gridX : int = 0
var gridY : int = 0
var gridWidth : int = 0
var gridHeight : int = 0

var gridCells : Array = []

const INVALID_ID : int = -1
enum CellOccupationType {
	STAY = 0,
	TRANSIT
}
class QueryRecord:
	var entityId : int = INVALID_ID
	var occupationType : int = CellOccupationType.STAY
	
class CellInfo:
	var entityId : int = INVALID_ID
	var traversable : bool = false
	var busy : bool = false
	var query : Array = []

## Defines an agent that moves
#class Agent:
#
#	const maxForce : float = 20.0 #rate of acceleration
#	const maxSpeed : float = 4.0 # grid squares / second
#
#	const radius = 0.23
#	const minSeparation = 0.8 # We'll move away from anyone nearer than this
#
#	const maxCohesion = 2 # We'll move closer to anyone within this bound
#
#	const maxForceSquared : float = maxForce * maxForce
#	const maxSpeedSquared : float = maxSpeed * maxSpeed
#
#	var group : int = 0
#	var rotation : float = 0.0
#	var position : Vector2 = Vector2()
#	var velocity : Vector2 = Vector2()
#
#	var ff : Vector2 = Vector2()
#	var forceToApply : Vector2 = Vector2()
#
#	func _init(pos : Vector2, grp : int):
#		group = grp
#		rotation = 0.0
#
##		//Create a physics body for the agent
##		var fixDef = new B2FixtureDef();
##		var bodyDef = new B2BodyDef();
##
##		fixDef.density = 20.0;
##		fixDef.friction = 0.0;
##		fixDef.restitution = 0.0;
##		fixDef.shape = new B2CircleShape(this.radius);
##
##		bodyDef.type = B2Body.b2_dynamicBody;
##		//bodyDef.linearDamping = 0.1;
##		bodyDef.position.SetV(pos);
##
##		this.body = world.CreateBody(bodyDef);
##		this.fixture = this.body.CreateFixture(fixDef);
#
#
#
#
#
## Called to start the game
#func startGame():
#	for yPos in range(1, gridHeight - 1):
#		agents.push_back(Agent.new(Vector2(0, yPos), 0))
#		agents.push_back(Agent.new(Vector2(1, yPos), 0))
#		agents.push_back(Agent.new(Vector2(2, yPos), 0))
#
#	for yPos in range (1, gridHeight - 1):
#		agents.push_back(Agent.new(Vector2(gridWidth - 1, yPos), 1))
#		agents.push_back(Agent.new(Vector2(gridWidth - 2, yPos), 1))
#		agents.push_back(Agent.new(Vector2(gridWidth - 3, yPos), 1))
#
#
#	ccCreateBuffers()
##	if (document.URL.indexOf('noobstacles') == -1) {
##		for (var i = 0; i < gridHeight; i++) {
##			if (i >= gridHeight / 2 - 2 && i < gridHeight / 2 + 2) {
##				continue;
##			}
##			for (var y = 6; y < gridWidth - 6; y++) {
##				obstacles.push(new B2Vec2(y, i));
##			}
##		}
##	}
#
#
##	//for (var i = 0; i < 30; i++) {
##	//	var x = 1 + Math.floor(Math.random() * (gridWidth - 3));
##	//	var y = Math.floor(Math.random() * (gridHeight - 2));
##	//	obstacles.push(new B2Vec2(x, y));
##	//}
#
##	for (var i = 0; i < obstacles.length; i++) {
##		var pos = obstacles[i];
##
##		//Create a physics body for the agent
##		var fixDef = new B2FixtureDef();
##		var bodyDef = new B2BodyDef();
##
##		fixDef.density = 1.0;
##		fixDef.friction = 0.5;
##		fixDef.restitution = 0.2;
##		fixDef.shape = new B2PolygonShape();
##		fixDef.shape.SetAsBox(0.5, 0.5);
##
##		bodyDef.type = B2Body.b2_staticBody;
##		bodyDef.position.SetV(pos);
##
##		world.CreateBody(bodyDef).CreateFixture(fixDef);
##	}
#
##	stage.addEventListener('stagemouseup', function (ev) {
##		destinations[0].x = Math.floor(ev.stageX / gridPx);
##		destinations[0].y = Math.floor(ev.stageY / gridPx);
##
##		destinations[1].x = gridWidth - destinations[0].x - 1;
##		destinations[1].y = gridHeight - destinations[0].y - 1;
##	});
#
#
## called periodically to update the game
## dt is the change of time since the last update (in seconds)
#func gameTick(dt):
#	#var i, agent;
#
#	updateContinuumCrowdsData(Agent.maxSpeed)
#
#	# Calculate steering and flocking forces for all agents
#	for agent in agents:
#		# Work out our behaviours
#		var ff = agent.ff #steeringBehaviourSeek(agent, destinations[agent.group]);
#		var sep = steeringBehaviourSeparation(agent)
#		var alg = steeringBehaviourAlignment(agent)
#		var coh = steeringBehaviourCohesion(agent)
#
#		# For visually debugging forces agent.forces = [ff.Copy(), sep.Copy(), alg.Copy(), coh.Copy()];
#
#		agent.forceToApply = ff + (sep * (1.2)) + (alg * (0.3)) + (coh * (0.05))
#
#		var lengthSquared = agent.forceToApply.length_squared()
#		if lengthSquared > agent.maxForceSquared:
#			agent.forceToApply *= (agent.maxForce / sqrt(lengthSquared))
#
#	# Move agents based on forces being applied (aka physics)
#	for agent in agents:
#
#		# Apply the force
#		# console.log(i + ': ' + agent.forceToApply.x + ', ' + agent.forceToApply.y);
#		#agent.body.ApplyImpulse(agent.forceToApply.Multiply(dt), agent.position());
#		agent.velocity += agent.forceToApply * dt
#		agent.position += agent.velocity * dt
#
#		# Calculate our new movement angle TODO: Should probably be done after running step
#		agent.rotation = agent.velocity.angle()


func getIndex(row : int, col : int) -> int:
	return row * gridWidth + col
	
func getRow(index : int) -> int:
	return int(index / gridWidth)
	
func getCol(index : int) -> int:
	return index % gridWidth

func standAtCell(_row : int, _col : int) -> void:
	#gridCells[getIndex(row, col)]
	pass
	
func init_cell(row : int, col : int) -> void:
	var cellInfo : CellInfo = CellInfo.new()
	var cell : int = get_cell(gridX + row, gridY + col)
	cellInfo.busy = false
	cellInfo.traversable = cell in traversableTiles
	gridCells[getIndex(row, col)] = cellInfo

func get_cell_info(row : int, col : int) -> CellInfo:
	return gridCells[getIndex(row, col)]

func map_to_grid(tilemapCellCoord : Vector2) -> Vector2:
	return Vector2(tilemapCellCoord.x - gridX, tilemapCellCoord.y - gridY)
	
func get_cell_info_tilemap(tilemapCellCoord : Vector2) -> CellInfo:
	var cellCoord : Vector2 = map_to_grid(tilemapCellCoord)
	return get_cell_info(cellCoord.x, cellCoord.y)
	
func take_cell_tilemap(entityId : int, tilemapCellCoord : Vector2) -> void:
	var cellCoord : Vector2 = map_to_grid(tilemapCellCoord)
	take_cell(entityId, cellCoord.x, cellCoord.y)

func release_cell_tilemap(entityId : int, tilemapCellCoord : Vector2) -> void:
	var cellCoord : Vector2 = map_to_grid(tilemapCellCoord)
	release_cell(entityId, cellCoord.x, cellCoord.y)

func stay_at_cell(entityId : int, row : int, col : int) -> void:
	var cellInfo : CellInfo = get_cell_info(row, col)
	cellInfo.busy = true 
	cellInfo.entityId = entityId
	cellInfo.query.clear()
	
func take_cell(entityId : int, row : int, col : int) -> void:
	var cellInfo : CellInfo = get_cell_info(row, col)
	cellInfo.busy = true 
	cellInfo.entityId = entityId
	
func release_cell(entityId : int, row : int, col : int) -> void:
	var cellInfo : CellInfo = get_cell_info(row, col)
	cellInfo.busy = false 
	cellInfo.entityId = INVALID_ID
	for record in cellInfo.query:
		if record.entityId == entityId:
			cellInfo.query.erase(record)
			record.free()
	
func transit_through_cell(_entityId : int, _row : int, _col : int) -> void:
	pass
	
func cell_query_size(row : int, col : int) -> int:
	var cellInfo : CellInfo = get_cell_info(row, col)
	return cellInfo.query.size()

func _ready():
	var tilemap_rect : Rect2 = get_used_rect()
	gridX = tilemap_rect.position.x
	gridY = tilemap_rect.position.y
	gridWidth = tilemap_rect.size.x
	gridHeight = tilemap_rect.size.y

	gridCells.resize(gridWidth * gridHeight)
	
	for row in range(0, gridWidth):
		for col in range(0, gridHeight):
			init_cell(row, col)
	pass
#	#dGrid = generateDijkstraGrid(Vector2(-1, -1), [tile_set.find_tile_by_name("ground_tile")], tilemap_rect.size.x, tilemap_rect.size.y)
#	#fGrid = generateFlowField(Vector2(-1, -1) - tilemap_rect.position, dGrid, tilemap_rect.size.x, tilemap_rect.size.y)
#
#	startGame()
#
#	drawLayer.agents = agents
#	drawLayer.dijkstraGrid = ccPotentialField
#	drawLayer.losGrid = ccEikonalVisited
#	drawLayer.flowField = ccFlowField
#	drawLayer.cell_size = cell_size
#	drawLayer.width = tilemap_rect.size.x
#	drawLayer.height = tilemap_rect.size.y
#	drawLayer.visible = false
	#drawLayer.update()
	
func find_closest_tile_by_name(position: Vector2, tileName : String):
	var tile_index = tile_set.find_tile_by_name(tileName)
	var tiles = get_used_cells_by_id(tile_index)
	
	var closestDistSqr : float = INF
	var closestTile: Vector2 = Vector2()
	var closestTilePos: Vector2 = Vector2()
	var tileFound: bool = false
	for tile in tiles:
		var tilePos = map_to_world(tile) + cell_size / 2
		
		var distToTile = position.distance_squared_to(tilePos)
		if distToTile < closestDistSqr:
			closestDistSqr = distToTile
			closestTilePos = tilePos
			closestTile = tile
			tileFound = true
			
	return {"success": tileFound, "position": closestTilePos, "tile": closestTile}
	
func find_any_closest_tile_except_name(position: Vector2, tileName : String):
	var tile_index = tile_set.find_tile_by_name(tileName)
	var tiles = get_used_cells()
	
	var closestDistSqr : float = INF
	var closestTile: Vector2 = Vector2()
	var closestTilePos: Vector2 = Vector2()
	var tileFound: bool = false
	for tile in tiles:
		var tilePos = map_to_world(tile) + cell_size / 2
		var cell_index : int = get_cell(tile.x, tile.y)
		
		if cell_index == tile_index:
			continue
		
		var distToTile = position.distance_squared_to(tilePos)
		if distToTile < closestDistSqr:
			closestDistSqr = distToTile
			closestTilePos = tilePos
			closestTile = tile
			tileFound = true
			
	return {"success": tileFound, "position": closestTilePos, "tile": closestTile}

func direction_of_closest_tile_by_name(tileCoordinates: Vector2, tileName : String) -> int:
	var tile_index = tile_set.find_tile_by_name(tileName)
#	var tiles = get_used_cells_by_id(tile_index)
	var rect : Rect2 = get_used_rect()
	
	# check right direction
	var tileExists : bool = false
	var rightCount : float = 0
	for x in range(tileCoordinates.x, rect.position.x + rect.size.x):
		var cell : int = get_cell(x, tileCoordinates.y)
		
		if cell == tile_index:
			tileExists = true
			break
			
		rightCount += 1
		
	if not tileExists:
		rightCount = INF
		
	# check left direction
	tileExists = false
	var leftCount : float = 0
	for x in range(tileCoordinates.x, rect.position.x, -1):
		var cell : int = get_cell(x, tileCoordinates.y)
		
		if cell == tile_index:
			tileExists = true
			break
			
		leftCount += 1
		
	if not tileExists:
		leftCount = INF
		
	# check down direction
	tileExists = false
	var downCount : float = 0
	for y in range(tileCoordinates.y, rect.position.y + rect.size.y):
		var cell : int = get_cell(tileCoordinates.x, y)
		
		if cell == tile_index:
			tileExists = true
			break
			
		downCount += 1
		
	if not tileExists:
		downCount = INF
		
	# check up direction
	tileExists = false
	var upCount : float = 0
	for y in range(tileCoordinates.y, rect.position.y, -1):
		var cell : int = get_cell(tileCoordinates.x, y)
		
		if cell == tile_index:
			tileExists = true
			break
			
		upCount += 1
		
	if not tileExists:
		upCount = INF
		
	var directionsArray : Dictionary = {
		Global.Orientation.UP: upCount,
		Global.Orientation.DOWN: downCount,
		Global.Orientation.LEFT: leftCount,
		Global.Orientation.RIGHT: rightCount
	}
	
	var minDir : int = -1
	var minCount : float = INF
	for key in directionsArray.keys():
		if directionsArray[key] < minCount:
			minCount = directionsArray[key]
			minDir = key
			
	return minDir
	
func direction_of_any_closest_tile_except_name(tileCoordinates: Vector2, tileName : String) -> int:
	var tile_index = tile_set.find_tile_by_name(tileName)
#	var tiles = get_used_cells_by_id(tile_index)
	var rect : Rect2 = get_used_rect()
	
	# check right direction
	var tileExists : bool = false
	var rightCount : float = 0
	for x in range(tileCoordinates.x, rect.position.x + rect.size.x):
		var cell : int = get_cell(x, tileCoordinates.y)
		
		if cell != tile_index:
			tileExists = true
			break
			
		rightCount += 1
		
	if not tileExists:
		rightCount = INF
		
	# check left direction
	tileExists = false
	var leftCount : float = 0
	for x in range(tileCoordinates.x, rect.position.x, -1):
		var cell : int = get_cell(x, tileCoordinates.y)
		
		if cell != tile_index:
			tileExists = true
			break
			
		leftCount += 1
		
	if not tileExists:
		leftCount = INF
		
	# check down direction
	tileExists = false
	var downCount : float = 0
	for y in range(tileCoordinates.y, rect.position.y + rect.size.y):
		var cell : int = get_cell(tileCoordinates.x, y)
		
		if cell != tile_index:
			tileExists = true
			break
			
		downCount += 1
		
	if not tileExists:
		downCount = INF
		
	# check up direction
	tileExists = false
	var upCount : float = 0
	for y in range(tileCoordinates.y, rect.position.y, -1):
		var cell : int = get_cell(tileCoordinates.x, y)
		
		if cell != tile_index:
			tileExists = true
			break
			
		upCount += 1
		
	if not tileExists:
		upCount = INF
		
	var directionsArray : Dictionary = {
		Global.Orientation.UP: upCount,
		Global.Orientation.DOWN: downCount,
		Global.Orientation.LEFT: leftCount,
		Global.Orientation.RIGHT: rightCount
	}
	
	var minDir : int = -1
	var minCount : int = INF
	for key in directionsArray.keys():
		if directionsArray[key] < minCount:
			minCount = directionsArray[key]
			minDir = key
			
	return minDir
	
#
#func _physics_process(delta):
#	gameTick(delta)
#
#func neighboursOf(v : Vector2, gridWidth : int, gridHeight : int) -> Array:
#	var res : Array = []
#	if v.x > 0:
#		res.push_back(Vector2(v.x - 1, v.y))
#
#	if v.y > 0:
#		res.push_back(Vector2(v.x, v.y - 1))
#
#	if v.x < gridWidth - 1:
#		res.push_back(Vector2(v.x + 1, v.y))
#
#	if v.y < gridHeight - 1:
#		res.push_back(Vector2(v.x, v.y + 1))
#
#	return res
#
#func allNeighboursOfWO(v : Vector2, gridWidth : int, gridHeight : int) -> Array:
#	var res : Array = []
#
#	for dx in range(-1, 2):
#		for dy in range(-1, 2):
#			var x = v.x + dx
#			var y = v.y + dy
#
#			# All neighbours on the grid that aren't ourself
#			if x >= 0 and y >= 0 and x < gridWidth and y < gridHeight and not (dx == 0 and dy == 0):
#				res.push_back(Vector2(x, y))
#
#	return res
#
## Helper method. Returns true if this grid location is on the grid and not impassable
#func isValid_(x, y, dijkstraGrid : Array, gridWidth : int, gridHeight : int) -> bool:
#	return x >= 0 and y >= 0 and x < gridWidth and y < gridHeight and dijkstraGrid[x][y] != SteeringBehavior.MaxFloat and dijkstraGrid[x][y] != -1
#
## Returns the non-obstructed neighbours of the given grid location.
## Diagonals are only included if their neighbours are also not obstructed
#func allNeighboursOf(v : Vector2, dijkstraGrid : Array, gridWidth : int, gridHeight : int) -> Array:
#	var res : Array = []
#	var x = v.x
#	var y = v.y
#
#	var up = isValid_(x, y - 1, dijkstraGrid, gridWidth, gridHeight)
#	var down = isValid_(x, y + 1, dijkstraGrid, gridWidth, gridHeight)
#	var left = isValid_(x - 1, y, dijkstraGrid, gridWidth, gridHeight)
#	var right = isValid_(x + 1, y, dijkstraGrid, gridWidth, gridHeight)
#
#	# We test each straight direction, then subtest the next one clockwise
#
#	if left:
#		res.push_back(Vector2(x - 1, y))
#
#		# left up
#		if up and isValid_(x - 1, y - 1, dijkstraGrid, gridWidth, gridHeight):
#			res.push_back(Vector2(x - 1, y - 1))
#
#	if up:
#		res.push_back(Vector2(x, y - 1))
#
#		# up right
#		if right and isValid_(x + 1, y - 1, dijkstraGrid, gridWidth, gridHeight):
#			res.push_back(Vector2(x + 1, y - 1))
#
#	if right:
#		res.push_back(Vector2(x + 1, y))
#
#		# right down
#		if down and isValid_(x + 1, y + 1, dijkstraGrid, gridWidth, gridHeight):
#			res.push_back(Vector2(x + 1, y + 1))
#
#	if down:
#		res.push_back(Vector2(x, y + 1))
#
#		# down left
#		if left and isValid_(x - 1, y + 1, dijkstraGrid, gridWidth, gridHeight):
#			res.push_back(Vector2(x - 1, y + 1))
#
#	return res
#
#func calculateLos(at : Vector2, pathEnd : Vector2, dijkstraGrid : Array, losGrid : Array):
#	var xDif = pathEnd.x - at.x
#	var yDif = pathEnd.y - at.y
#
#	var xDifAbs = abs(xDif)
#	var yDifAbs = abs(yDif)
#
#	var hasLos = false
#
#	var xDifOne = sign(xDif)
#	var yDifOne = sign(yDif)
#
#	# Check the direction we are furtherest from the destination on (or both if equal)
#	# If it has LOS then we might
#
#	# Check in the x direction
#	if xDifAbs >= yDifAbs:
#
#		if losGrid[at.x + xDifOne][at.y]:
#			hasLos = true
#
#	# Check in the y direction
#	if yDifAbs >= xDifAbs:
#
#		if losGrid[at.x][at.y + yDifOne]:
#			hasLos = true
#
#	# If we are not a straight line vertically/horizontally to the exit
#	if yDifAbs > 0 and xDifAbs > 0:
#		# If the diagonal doesn't have LOS, we don't
#		if !losGrid[at.x + xDifOne][at.y + yDifOne]:
#			hasLos = false
#		elif yDifAbs == xDifAbs:
#			# If we are an exact diagonal and either straight direction is a wall, we don't have LOS
#			if dijkstraGrid[at.x + xDifOne][at.y] == SteeringBehavior.MaxFloat or dijkstraGrid[at.x][at.y + yDifOne] == SteeringBehavior.MaxFloat:
#				hasLos = false
#
#	#It's a definite now
#	losGrid[at.x][at.y] = hasLos
#
#	# TODO: Could replace our distance with a direct distance?
#	# Might not be worth it, would need to use a priority queue for the open list.
#
#func generateDijkstraGrid(pathEnd : Vector2, traversableTiles : Array, gridWidth : int, gridHeight : int) -> Array:
#	var tilemap_rect : Rect2 = get_used_rect()
#
#	# Generate an empty grid, set all places as weight null, which will stand for unvisited
#	var dijkstraGrid : Array = []
#	losGrid = []
#	dijkstraGrid.resize(gridWidth)
#	losGrid.resize(gridWidth)
#	for x in range (0, gridWidth):
#		var arr : Array = []
#		var arr2 : Array = []
#		arr.resize(gridHeight)
#		arr2.resize(gridHeight)
#		for y in range (0, gridHeight):
#			arr[y] = -1
#			arr2[y] = false
#		dijkstraGrid[x] = arr
#		losGrid[x] = arr2
#
#	# Set all places where towers are as being weight MAXINT, which will stand for not able to go here
##	for (var i = 0; i < towers.length; i++) {
##		var t = towers[i];
##
##		dijkstraGrid[t.x][t.y] = Number.MAX_VALUE;
##	}
#
#	# flood fill out from the end point
#	#pathEnd.distance = 0
#	var pathEndInGridCoord : Vector2 = pathEnd - tilemap_rect.position
#	dijkstraGrid[pathEndInGridCoord.x][pathEndInGridCoord.y] = 0
#	losGrid[pathEndInGridCoord.x][pathEndInGridCoord.y] = true
#
#	var toVisit = [pathEnd]
#
#	# for each node we need to visit, starting with the pathEnd
#	for cellToVisit in toVisit:
#		var cellInGridCoerd : Vector2 = cellToVisit - tilemap_rect.position
#		if not get_cell(cellToVisit.x, cellToVisit.y) in traversableTiles:
#			dijkstraGrid[cellInGridCoerd.x][cellInGridCoerd.y] = SteeringBehavior.MaxFloat
#			continue
#
#		# Calculate if we have LOS
#		# Only need to see if don't have LOS if we aren't the end
#		if cellToVisit != pathEnd:
#			calculateLos(cellInGridCoerd, pathEndInGridCoord, dijkstraGrid, losGrid)
#
#		var neighbours = neighboursOf(cellInGridCoerd, gridWidth, gridHeight)
#
#		# for each neighbour of this node (only straight line neighbours, not diagonals)
#		for n in neighbours:
#			var cellInTileMapCoord : Vector2 = n + tilemap_rect.position
#			# We will only ever visit every node once as we are always visiting nodes in the most efficient order
#			if dijkstraGrid[n.x][n.y] == -1:
#				if get_cell(cellInTileMapCoord.x, cellInTileMapCoord.y) in traversableTiles:
#					#n.distance = toVisit[i].distance + 1
#					#distGrid[n.x][n.y] = distGrid[cellInGridCoerd.x][cellInGridCoerd.y] + 1
#					#dijkstraGrid[n.x][n.y] = distGrid[n.x][n.y]#dijkstraGrid[cellInGridCoerd.x][cellInGridCoerd.y] + 1#cellInTileMapCoord.distance_to(pathEnd)
#					dijkstraGrid[n.x][n.y] = dijkstraGrid[cellInGridCoerd.x][cellInGridCoerd.y] + 1#cellInTileMapCoord.distance_to(pathEnd)
#					toVisit.push_back(cellInTileMapCoord)
#				else:
#					dijkstraGrid[n.x][n.y] = SteeringBehavior.MaxFloat
#
#	return dijkstraGrid
#
#func generateFlowField(destination : Vector2, dijkstraGrid : Array, gridWidth : int, gridHeight : int) -> Array:
#	# Generate an empty grid, set all places as Vector2.zero, which will stand for no good direction
#	var flowField : Array = []
#	flowField.resize(gridWidth)
#	for x in range (0, gridWidth):
#		var arr : Array = []
#		arr.resize(gridHeight)
#		for y in range (0, gridHeight):
#			arr[y] = Vector2()
#		flowField[x] = arr
#
#	# for each grid square
#	for x in range (0, gridWidth):
#		for y in range (0, gridHeight):
#
#			# Obstacles have no flow value
#			if dijkstraGrid[x][y] == SteeringBehavior.MaxFloat or dijkstraGrid[x][y] == -1:
#				continue
#
#			if losGrid[x][y]:
#				# Just point straight at the destination
#				flowField[x][y] = (destination - Vector2(x, y)).normalized()
#				continue
#
#			var pos : Vector2 = Vector2(x, y)
#			var neighbours = allNeighboursOf(pos, dijkstraGrid, gridWidth, gridHeight)
#
#			# Go through all neighbours and find the one with the lowest distance
#			var minNeighbour = null;
#			var minDist = 0;
#			for n in neighbours:
#				if dijkstraGrid[n.x][n.y] == -1 || dijkstraGrid[x][y] == SteeringBehavior.MaxFloat:
#					continue
#
#				var dist = dijkstraGrid[n.x][n.y] - dijkstraGrid[pos.x][pos.y]
#
#				if dist < minDist:
#					minNeighbour = n
#					minDist = dist
#
#			# If we found a valid neighbour, point in its direction
#			if minNeighbour:
#				flowField[x][y] = (minNeighbour - pos).normalized()
#
#	return flowField
#
#
#func steeringBehaviourFlowField(agent) -> Vector2:
#	# Work out the force to apply to us based on the flow field grid squares we are on.
#	# we apply bilinear interpolation on the 4 grid squares nearest to us to work out our force.
#	# http://en.wikipedia.org/wiki/Bilinear_interpolation#Nonlinear
#
#	# Top left Coordinate of the 4
#	var agentTile : Vector2 = world_to_map(agent.position)
#	var agentTileInGridCoord : Vector2 = agentTile - get_used_rect().position
#
#	# The 4 weights we'll interpolate, see http://en.wikipedia.org/wiki/File:Bilininterp.png for the coordinates
#	var f00 : Vector2 = (ccFlowField[agentTileInGridCoord.x][agentTileInGridCoord.y] if isValid(agentTileInGridCoord.x, agentTileInGridCoord.y) else Vector2())
#	var f01 : Vector2 = (ccFlowField[agentTileInGridCoord.x][agentTileInGridCoord.y + 1] if isValid(agentTileInGridCoord.x, agentTileInGridCoord.y + 1) else Vector2())
#	var f10 : Vector2 = (ccFlowField[agentTileInGridCoord.x + 1][agentTileInGridCoord.y] if isValid(agentTileInGridCoord.x + 1, agentTileInGridCoord.y) else Vector2())
#	var f11 : Vector2 = (ccFlowField[agentTileInGridCoord.x + 1][agentTileInGridCoord.y + 1] if isValid(agentTileInGridCoord.x + 1, agentTileInGridCoord.y + 1) else Vector2())
#
#	# Do the x interpolations
#	var xWeight : float = agent.position.x - agentTile.x;
#
#	var top : Vector2 = f00 * (1 - xWeight) + (f10 * (xWeight))
#	var bottom : Vector2 = f01 * (1 - xWeight) + (f11 * (xWeight))
#
#	# Do the y interpolation
#	var yWeight : float = agent.position.y - agentTile.y
#
#	# This is now the direction we want to be travelling in (needs to be normalized)
#	var desiredDirection : Vector2 = top * (1 - yWeight) + (bottom * (yWeight))
#
#	if desiredDirection.length() > SteeringBehavior.Epsilon:
#		desiredDirection = desiredDirection.normalized()
#		return steerTowards(agent, desiredDirection)
#
#	return Vector2()
#
#func steeringBehaviourSeek(agent : Agent, dest : Vector2):
#
#	if dest.x == agent.position.x and dest.y == agent.position.y:
#		return Vector2()
#
#	# Desired change of location
#	var desired : Vector2 = dest - agent.position
#	# Desired velocity (move there at maximum speed)
#	desired *= (agent.maxSpeed / desired.length())
#	# The velocity change we want
#	var velocityChange : Vector2 = desired - agent.velocity
#	# Convert to a force
#	return velocityChange * (agent.maxForce / agent.maxSpeed)
#
#func steeringBehaviourSeparation(agent : Agent):
#	var totalForce : Vector2 = Vector2()
#	var neighboursCount : int = 0
#
#	for a in agents:
#		if a != agent:
#			var distance : float = agent.position.distance_to(a.position)
#			if distance < agent.minSeparation and distance > 0:
#				# Vector to other agent
#				var pushForce : Vector2 = agent.position - a.position
#				var length : float = pushForce.length()
#				pushForce = pushForce.normalized()
#				var r : float = (agent.radius + a.radius)
#
#				totalForce += pushForce * (1 - ((length - r) / (agent.minSeparation - r))) #agent.minSeparation)))
#				# totalForce.Add(pushForce.Multiply(1 - (length / agent.minSeparation)))
#				# totalForce.Add(pushForce.Divide(agent.radius))
#
#				neighboursCount += 1
#
#	if neighboursCount == 0:
#		return totalForce # Zero
#
#	return totalForce * (agent.maxForce / neighboursCount)
#
#func steeringBehaviourCohesion(agent : Agent):
#	# Start with just our position
#	var centerOfMass = Vector2() #agent.position
#	var neighboursCount : int = 0
#
#	for a in agents:
#		if a != agent and a.group == agent.group:
#			var distance : float = agent.position.distance_to(a.position)
#			if distance < agent.maxCohesion:
#				# sum up the position of our neighbours
#				centerOfMass += a.position
#				neighboursCount += 1
#
#	if neighboursCount == 0:
#		return Vector2()
#
#	# Get the average position of ourself and our neighbours
#	centerOfMass /= neighboursCount
#
#	# seek that position
#	return steeringBehaviourSeek(agent, centerOfMass)
#
#func steeringBehaviourAlignment(agent):
#	var averageHeading : Vector2 = Vector2()
#	var neighboursCount : int = 0
#
#	# for each of our neighbours (including ourself)
#	for a in agents:
#		var distance : float = agent.position.distance_to(a.position)
#		# That are within the max distance and are moving
#		if distance < agent.maxCohesion and a.velocity.length() > 0 and a.group == agent.group:
#			# Sum up our headings
#			var head : Vector2 = a.velocity.normalized()
#			averageHeading += head
#			neighboursCount += 1
#
#	if neighboursCount == 0:
#		return averageHeading # Zero
#
#	# Divide to get the average heading
#	averageHeading /= neighboursCount
#
#	# Steer towards that heading
#	return steerTowards(agent, averageHeading)
#
#func steerTowards(agent, desiredDirection) -> Vector2:
#	# Multiply our direction by speed for our desired speed
#	var desiredVelocity : Vector2 = desiredDirection * agent.maxSpeed
#
#	# The velocity change we want
#	var velocityChange : Vector2 = desiredVelocity - agent.velocity
#	# Convert to a force
#	return velocityChange * (agent.maxForce / agent.maxSpeed)
#
#
#var destinations : Array = [
#	Vector2(gridWidth - 2, gridHeight / 2), # middle right
#	Vector2(1, gridHeight / 2) # middle left
#]
#
#var ccDensity : Array = []
#
#
#var ccAvgVelocity : Array = []
#
#
#var ccDiscomfort : Array = []
#
#
## The directional array is always [North, East, South, West]
#var ccSpeedField : Array = []
#
#
#var ccCostField : Array = []
#
#
#var directionVectors : Array = [
#	Vector2(0, -1),
#	Vector2(1, 0),
#	Vector2(0, 1),
#	Vector2(-1, 0)
#]
#
#var allDirectionVectors : Array = [
#	Vector2(0, -1),
#	Vector2(1, 0),
#	Vector2(0, 1),
#	Vector2(-1, 0),
#
#	Vector2(1, 1),
#	Vector2(1, -1),
#	Vector2(-1, -1),
#	Vector2(-1, 1)
#]
#
#var ccPotentialField : Array = []
#
#
#var ccFlowField : Array = []
#
#var agents : Array = []
#
#func updateContinuumCrowdsData(agentMaxSpeed : float) -> void:
#	# First, clear all buffers
#	ccClearBuffers()
#
#	# Update density field and average speed map (4.1)
#	ccCalculateDensityAndAverageSpeed()
#
#	# CC Paper says this is group dependant, but I'm not sure how...
#	ccCalculateUnitCostField(agentMaxSpeed)
#
#	for group in range(0, 2): # foreach group
#		ccClearPotentialField()
#
#		# Construct the potential
#		ccPotentialFieldEikonalFill(destinations[group])
#		# Compute the gradient
#		# ccCalculatePotentialFieldGradient()
#		ccGenerateFlowField() # TODO: This does not use the way of calculating described in the paper (I think)
#
#		# (use these for steering later)
#		for agent in agents:
#			if agent.group == group:
#				agent.ff = steeringBehaviourFlowField(agent)
#
#func ccCreateBuffers() -> void:
#	ccDensity.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccDensity[x] = []
#		ccDensity[x].resize(gridHeight)
#
#	ccAvgVelocity.resize(gridWidth)
#	for x in range (0, gridWidth):
#		ccAvgVelocity[x] = []
#		ccAvgVelocity[x].resize(gridHeight)
#		for y in range(0, gridHeight):
#			ccAvgVelocity[x][y] = Vector2()
#
#	ccDiscomfort.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccDiscomfort[x] = [];
#		ccDiscomfort[x].resize(gridHeight)
#
#	ccSpeedField.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccSpeedField[x] = []
#		ccSpeedField[x].resize(gridHeight)
#		for y in range(0, gridHeight):
#			ccSpeedField[x][y] = [0, 0, 0, 0];
#
#	ccCostField.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccCostField[x] = []
#		ccCostField[x].resize(gridHeight)
#		for y in range(0, gridHeight):
#			ccCostField[x][y] = [0, 0, 0, 0];
#
#	ccPotentialField.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccPotentialField[x] = []
#		ccPotentialField[x].resize(gridHeight)
#		for y in range(0, gridHeight):
#			ccPotentialField[x][y] = SteeringBehavior.MaxFloat
#
#	ccFlowField.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccFlowField[x] = []
#		ccFlowField[x].resize(gridHeight)
#		for y in range(0, gridHeight):
#			ccFlowField[x][y] = Vector2()
#
#	ccEikonalVisited.resize(gridWidth)
#	for x in range(0, gridWidth):
#		ccEikonalVisited[x] = []
#		ccEikonalVisited[x].resize(gridHeight)
#
#func ccClearBuffers() -> void:
#	# Clear the density field
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccDensity[x][y] = 0
#
#	# Clear the average velocity field
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccAvgVelocity[x][y] = Vector2()
#
#	# Clear the discomfort field and repopulate from obstacles list
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccDiscomfort[x][y] = 0
#
##	for (x = obstacles.length - 1; x >= 0; x--) {
##		var o = obstacles[x];
##		ccDiscomfort[o.x][o.y] = SteeringBehavior.MaxFloat
##	}
#	for cell in get_used_cells():
#		if not get_cell(cell.x, cell.y) in traversableTiles:
#			ccDiscomfort[cell.x][cell.y] = SteeringBehavior.MaxFloat
#
#func ccCalculateDensityAndAverageSpeed():
#	var perAgentDensity = 1
#
#	for agent in agents:
#		# Add their density onto the density field like bilinear filtering
#		# TODO: This isn't the same as the paper formula, will need investigating
#
#		var posGridCell : Vector2 = Vector2(int(agent.position.x / cell_size.x), int(agent.position.y / cell_size.y))#world_to_map(agent.position) - get_used_rect().position
#
#		var xWeight = agent.position.x - posGridCell.x;
#		var yWeight = agent.position.y - posGridCell.y;
#
#		# top left
#		if isValid(posGridCell.x, posGridCell.y):
#			ccAddDensity(posGridCell.x, posGridCell.y, agent.velocity, perAgentDensity * (1 - xWeight) * (1 - yWeight));
#		# top right
#		if isValid(posGridCell.x + 1, posGridCell.y):
#			ccAddDensity(posGridCell.x + 1, posGridCell.y, agent.velocity, perAgentDensity * (xWeight) * (1 - yWeight));
#		# bottom left
#		if isValid(posGridCell.x, posGridCell.y + 1):
#			ccAddDensity(posGridCell.x, posGridCell.y + 1, agent.velocity, perAgentDensity * (1 - xWeight) * (yWeight));
#		# bottom right
#		if isValid(posGridCell.x + 1, posGridCell.y + 1):
#			ccAddDensity(posGridCell.x + 1, posGridCell.y + 1, agent.velocity, perAgentDensity * (xWeight) * (yWeight));
#
#	# Fix up the average velocity to be average rather than weighted sum
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			if ccDensity[x][y] > 0:
#				ccAvgVelocity[x][y] /= (ccDensity[x][y])
#
#func ccAddDensity(x : int, y : int, velocity : Vector2, weight : float) -> void:
#	ccDensity[x][y] += weight
#
#	ccAvgVelocity[x][y].x += velocity.x * weight
#	ccAvgVelocity[x][y].y += velocity.y * weight
#
#func ccCalculateUnitCostField(agentMaxSpeed : float) -> void:
#	# Tuning variables for how much the density of a grid cell influences the speed you can travel in it
#	# density >= max mean your speed is decided by the average speed in the cell
#	# density <= min mean your speed is the maximum speed
#	# in between we interpolate between the 2
#	var densityMin : float = 0.5
#	var densityMax : float = 0.8
#
#	# Weights for formula (4) on page 4
#	var lengthWeight = 1;
#	var timeWeight = 1;
#	var discomfortWeight = 1;
#
#	# foreach grid cell
#	for x in range(0, gridWidth):
#		var speedArr = ccSpeedField[x];
#		var costsArr = ccCostField[x];
#		for y in range(0, gridHeight):
#			var speeds = speedArr[y];
#			var costs = costsArr[y];
#
#			# foreach direction we can leave that cell
#			for i in range(0, 4):
#				var dir = directionVectors[i];
#				var targetX = x + dir.x;
#				var targetY = y + dir.y;
#
#				# If I was to move from this grid cell to the one in the given direction
#				# how bad would that be
#
#				# If we'd move off the grid, don't go that way (also if we'd walk in to a wall would go here)
#				if !isValid(targetX, targetY):
#					speeds[i] = SteeringBehavior.MaxFloat
#					continue
#
#				# Calculate by looking at the average velocity of the destination
#				# in the direction that we'd be travelling
#
#				var speedVecX : float = dir.x * ccAvgVelocity[targetX][targetY].x;
#				var speedVecY : float = dir.y * ccAvgVelocity[targetX][targetY].y;
#
#				# Get the only speed value as one will be zero
#				# this is like speedVecX != 0 ? : speedVecX : speedVecY
#				var flowSpeed : float = speedVecX if speedVecX != 0 else speedVecY
#
#				var targetDensity = ccDensity[targetX][targetY];
#				var targetDiscomfort = ccDiscomfort[targetX][targetY];
#
#				if targetDensity >= densityMax:
#					speeds[i] = flowSpeed
#				elif targetDensity <= densityMin:
#					speeds[i] = agentMaxSpeed
#				else:
#					# medium speed
#					speeds[i] = agentMaxSpeed - (targetDensity - densityMin) / (densityMax - densityMin) * (agentMaxSpeed - flowSpeed)
#
#				# we're going to divide by speed later, so make sure it's not zero
#				speeds[i] = max(0.001, speeds[i]);
#
#
#				# Work out the cost to move in to the destination cell
#				costs[i] = (speeds[i] * lengthWeight + timeWeight + discomfortWeight * targetDiscomfort) / speeds[i];
#
#
#func ccClearPotentialField() -> void:
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccPotentialField[x][y] = SteeringBehavior.MaxFloat
#
#var ccEikonalVisited : Array = []
#
#
#func costCompare(a, b):
#	return a.ccCost - b.ccCost
#
#func ccPotentialFieldEikonalFill(dest : Vector2) -> void:
#	# Do a dijkstra style fill out from the destination.
#	# We need to do this properly with priority lists and stuff as we will be using the cost field to go between cells
#
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccEikonalVisited[x][y] = false;
#
#	# TODO: The PriorityQueue we are using is probably slow. Check the jsperf:
#	# http://jsperf.com/js-priority-queue-queue-dequeue
#	# https://github.com/adamhooper/js-priority-queue
#	# I have no idea how array implementation could be faster than the other 2 implementations
#
##	var candidates = new PriorityQueue({
##		comparator: function (a, b) { return a.ccCost - b.ccCost; }
##	});
#	var candidates = PriorityQueue.new()
#	candidates.comparator = funcref(self, "costCompare")
#
#	var destination : Dictionary = {"position": dest, "ccCost": 0}
#	candidates.insert(destination)
#
#	var candidatesCount = 0
#	var failedCount = 0
#
#	while candidates.currentSize > 0:
#		candidatesCount += 1
#		var candidate : Dictionary = candidates.delMin()
#		var at : Vector2 = candidate.position
#		var ccCost : float = candidate.ccCost
#		# print('at ' + at.x + ', ' + at.y + ' @ ' + at.ccCost);
#
#		# We've got a better path
#		if (ccPotentialField[at.x][at.y] >= ccCost and !ccEikonalVisited[at.x][at.y]):
#			# print(ccPotentialField[at.x][at.y], ' >= ', ccCost);
#
#			ccPotentialField[at.x][at.y] = ccCost
#			ccEikonalVisited[at.x][at.y] = true
#
#			for i in range(0, directionVectors.size()):
#				var dir : Vector2 = directionVectors[i]
#
#				var toX : float = at.x + dir.x
#				var toY : float = at.y + dir.y
#				if isValid(toX, toY):
#
#					# Cost to go from our target cell to the start
#					# Our cost + cost of moving from the target to us
#					var toCost : float = ccCost + ccCostField[toX][toY][(i + 2) % directionVectors.size()]
#
#					# If we present a better path, overwrite the cost and queue up going to that cell
#					if toCost < ccPotentialField[toX][toY]:
#						#print('Queueing ' + toX + ', ' + toY + ' @ ' + toCost);
#						ccPotentialField[toX][toY] = toCost
#						ccEikonalVisited[at.x][at.y] = false
#
#						var toVec : Vector2 = at + dir
#						candidates.insert({"position": toVec, "ccCost": toCost})
#		else:
#			failedCount += 1
#
#	# print(candidatesCount + ' - ' + failedCount + ' ' + (gridWidth * gridHeight));
#
#func ccGenerateFlowField() -> void:
#	# Generate an empty grid, set all places as Vector2.zero, which will stand for no good direction
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#			ccFlowField[x][y] = Vector2()
#
#	# for each grid square
#	for x in range(0, gridWidth):
#		for y in range(0, gridHeight):
#
#			# Obstacles have no flow value
#			if ccPotentialField[x][y] == SteeringBehavior.MaxFloat:
#			#var cellInMapCoord : Vector2 = Vector2(x, y) + get_used_rect().position
#			#if not get_cell(cellInMapCoord.x, cellInMapCoord.y) in traversableTiles:
#			#	ccPotentialField[x][y] = SteeringBehavior.MaxFloat
#				continue
#
#			# Go through all neighbours and find the one with the lowest distance
#			var minDir = null;
#			var minDist = SteeringBehavior.MaxFloat
#
#			for dir in allDirectionVectors:
#				if isValid(x + dir.x, y + dir.y):
#					var dist = ccPotentialField[x + dir.x][y + dir.y]
#
#					if dist < minDist:
#						minDir = dir
#						minDist = dist
#
#
#			# If we found a valid neighbour, point in its direction
#			if minDir:
#				ccFlowField[x][y] = minDir.normalized()
#
## Helper method. Returns true if this grid location is on the grid
#func isValid(x, y) -> bool:
#	return x >= 0 and y >= 0 and x < gridWidth and y < gridHeight
