extends Node2D

#export (Array) var dijkstraGrid : Array = []
#export (Array) var losGrid : Array = []
#export (Array) var flowField : Array = []
#export (Array) var agents : Array = []
export (Vector2) var cell_size
export (int) var width = 0
export (int) var height = 0

export (NodePath) var tilemapPath : NodePath = NodePath()
onready var tilemap : TileMap = get_node_or_null(tilemapPath)

onready var default_font = Control.new().get_font("font")

func _ready():
	update()

func _process(delta):
	update()

func _draw():
	if not tilemap:
		return
	
	var tilemap_rect : Rect2 = tilemap.get_used_rect()
#	for x in range (tilemap_rect.position.x, tilemap_rect.position.x + tilemap_rect.size.x):
#		for y in range (tilemap_rect.position.y, tilemap_rect.position.y + tilemap_rect.size.y):
#
#			var pos : Vector2 = Vector2(cell_size.x * x, cell_size.y * y) - Vector2(tilemap_rect.position.x * cell_size.x, tilemap_rect.position.y * cell_size.y)
	for x in range (0, width):
		for y in range (0, height):
			var pos : Vector2 = Vector2(cell_size.x * x, cell_size.y * y)
			var posCentered : Vector2 = pos + cell_size / 2
			var cellInfo = tilemap.get_cell_info(x, y)
			if cellInfo.busy:
				draw_circle(posCentered, 8.0, Color(1, 0, 0))
			else:
				draw_circle(posCentered, 8.0, Color(0, 0, 1))
#	var font : Font = Label.new().get_font("")
#
#	for x in range (0, width):
#		for y in range (0, height):
#			var pos : Vector2 = Vector2(cell_size.x * x, cell_size.y * y)
#			var posCentered : Vector2 = pos + cell_size / 2
#			if dijkstraGrid[x][y] != -1 and dijkstraGrid[x][y] != SteeringBehavior.MaxFloat:
#				var string : String = String(int(dijkstraGrid[x][y]))
#				var string_size : Vector2 = default_font.get_string_size(string)
#				if losGrid[x][y]:
#					draw_rect(Rect2(pos, cell_size), Color(0, 1, 0), false)
#				else:
#					draw_rect(Rect2(pos, cell_size), Color(1, 0, 0), false)
#				draw_string(default_font, Vector2(posCentered.x - string_size.x / 2, posCentered.y + string_size.y / 2), string)
#				draw_line(posCentered, posCentered + flowField[x][y] * 8.0, Color(0, 0, 1))
#			else:
#				if dijkstraGrid[x][y] != -1:
#					draw_circle(posCentered, 8.0, Color(1, 0, 0))
#				else:
#					draw_circle(posCentered, 8.0, Color(0, 0, 1))
#
#	for agent in agents:
#		draw_circle(agent.position, 4.0, Color(0, 0, 1))
