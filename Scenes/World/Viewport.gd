extends Viewport

export (World2D) var root_world : World2D = null

func _ready():
	if root_world:
		set_world_2d(root_world)
	
func register_camera(camera: Camera2D) -> void:
	add_child(camera)
